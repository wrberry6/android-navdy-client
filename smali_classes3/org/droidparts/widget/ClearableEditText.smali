.class public Lorg/droidparts/widget/ClearableEditText;
.super Landroid/widget/EditText;
.source "ClearableEditText.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lorg/droidparts/adapter/widget/TextWatcherAdapter$TextWatcherListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/widget/ClearableEditText$Listener;
    }
.end annotation


# instance fields
.field private f:Landroid/view/View$OnFocusChangeListener;

.field private l:Landroid/view/View$OnTouchListener;

.field private listener:Lorg/droidparts/widget/ClearableEditText$Listener;

.field private xD:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-direct {p0}, Lorg/droidparts/widget/ClearableEditText;->init()V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    invoke-direct {p0}, Lorg/droidparts/widget/ClearableEditText;->init()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    invoke-direct {p0}, Lorg/droidparts/widget/ClearableEditText;->init()V

    .line 66
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 123
    iget-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x108006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 128
    invoke-virtual {p0, v3}, Lorg/droidparts/widget/ClearableEditText;->setClearIconVisible(Z)V

    .line 129
    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 131
    new-instance v0, Lorg/droidparts/adapter/widget/TextWatcherAdapter;

    invoke-direct {v0, p0, p0}, Lorg/droidparts/adapter/widget/TextWatcherAdapter;-><init>(Landroid/widget/EditText;Lorg/droidparts/adapter/widget/TextWatcherAdapter$TextWatcherListener;)V

    invoke-virtual {p0, v0}, Lorg/droidparts/widget/ClearableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 132
    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 104
    if-eqz p2, :cond_1

    .line 105
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/droidparts/widget/ClearableEditText;->setClearIconVisible(Z)V

    .line 109
    :goto_0
    iget-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 112
    :cond_0
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/droidparts/widget/ClearableEditText;->setClearIconVisible(Z)V

    goto :goto_0
.end method

.method public onTextChanged(Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/EditText;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-static {p2}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/droidparts/widget/ClearableEditText;->setClearIconVisible(Z)V

    .line 119
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 83
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v3, v3, v4

    if-eqz v3, :cond_2

    .line 84
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    move v0, v1

    .line 86
    .local v0, "tappedX":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 88
    const-string v2, ""

    invoke-virtual {p0, v2}, Lorg/droidparts/widget/ClearableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v2, p0, Lorg/droidparts/widget/ClearableEditText;->listener:Lorg/droidparts/widget/ClearableEditText$Listener;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lorg/droidparts/widget/ClearableEditText;->listener:Lorg/droidparts/widget/ClearableEditText$Listener;

    invoke-interface {v2}, Lorg/droidparts/widget/ClearableEditText$Listener;->didClearText()V

    .line 99
    .end local v0    # "tappedX":Z
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 84
    goto :goto_0

    .line 96
    :cond_2
    iget-object v1, p0, Lorg/droidparts/widget/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_3

    .line 97
    iget-object v1, p0, Lorg/droidparts/widget/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 99
    goto :goto_1
.end method

.method protected setClearIconVisible(Z)V
    .locals 5
    .param p1, "visible"    # Z

    .prologue
    .line 135
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/droidparts/widget/ClearableEditText;->xD:Landroid/graphics/drawable/Drawable;

    .line 136
    .local v0, "x":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p0}, Lorg/droidparts/widget/ClearableEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {p0, v1, v2, v0, v3}, Lorg/droidparts/widget/ClearableEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 138
    return-void

    .line 135
    .end local v0    # "x":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lorg/droidparts/widget/ClearableEditText$Listener;)V
    .locals 0
    .param p1, "listener"    # Lorg/droidparts/widget/ClearableEditText$Listener;

    .prologue
    .line 47
    iput-object p1, p0, Lorg/droidparts/widget/ClearableEditText;->listener:Lorg/droidparts/widget/ClearableEditText$Listener;

    .line 48
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0
    .param p1, "f"    # Landroid/view/View$OnFocusChangeListener;

    .prologue
    .line 75
    iput-object p1, p0, Lorg/droidparts/widget/ClearableEditText;->f:Landroid/view/View$OnFocusChangeListener;

    .line 76
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 70
    iput-object p1, p0, Lorg/droidparts/widget/ClearableEditText;->l:Landroid/view/View$OnTouchListener;

    .line 71
    return-void
.end method
