.class final Lorg/droidparts/inner/PersistUtils$1;
.super Ljava/lang/Object;
.source "PersistUtils.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/droidparts/inner/PersistUtils;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$db:Landroid/database/sqlite/SQLiteDatabase;

.field final synthetic val$statements:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lorg/droidparts/inner/PersistUtils$1;->val$statements:Ljava/util/ArrayList;

    iput-object p2, p0, Lorg/droidparts/inner/PersistUtils$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 175
    iget-object v2, p0, Lorg/droidparts/inner/PersistUtils$1;->val$statements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 176
    .local v1, "statement":Ljava/lang/String;
    invoke-static {v1}, Lorg/droidparts/util/L;->i(Ljava/lang/Object;)V

    .line 177
    iget-object v2, p0, Lorg/droidparts/inner/PersistUtils$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    .end local v1    # "statement":Ljava/lang/String;
    :cond_0
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v2
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p0}, Lorg/droidparts/inner/PersistUtils$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
