.class public final Lorg/droidparts/inner/AnnBuilder;
.super Ljava/lang/Object;
.source "AnnBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getColumnAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/sql/ColumnAnn;
    .locals 6
    .param p0, "f"    # Ljava/lang/reflect/Field;

    .prologue
    .line 103
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 104
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 105
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/sql/Column;

    if-ne v5, v2, :cond_0

    .line 106
    new-instance v5, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    check-cast v0, Lorg/droidparts/annotation/sql/Column;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/sql/ColumnAnn;-><init>(Lorg/droidparts/annotation/sql/Column;)V

    .line 109
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 103
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method static getInjectAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/inject/InjectAnn;
    .locals 6
    .param p0, "f"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            ")",
            "Lorg/droidparts/inner/ann/inject/InjectAnn",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_7

    aget-object v0, v1, v3

    .line 62
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 63
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/inject/InjectView;

    if-ne v5, v2, :cond_0

    .line 64
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectViewAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectView;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectViewAnn;-><init>(Lorg/droidparts/annotation/inject/InjectView;)V

    .line 79
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 65
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    const-class v5, Lorg/droidparts/annotation/inject/InjectFragment;

    if-ne v5, v2, :cond_1

    .line 66
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectFragment;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;-><init>(Lorg/droidparts/annotation/inject/InjectFragment;)V

    goto :goto_1

    .line 67
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_1
    const-class v5, Lorg/droidparts/annotation/inject/InjectDependency;

    if-ne v5, v2, :cond_2

    .line 68
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectDependencyAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectDependency;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectDependencyAnn;-><init>(Lorg/droidparts/annotation/inject/InjectDependency;)V

    goto :goto_1

    .line 69
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_2
    const-class v5, Lorg/droidparts/annotation/inject/InjectBundleExtra;

    if-ne v5, v2, :cond_3

    .line 70
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectBundleExtra;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;-><init>(Lorg/droidparts/annotation/inject/InjectBundleExtra;)V

    goto :goto_1

    .line 71
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_3
    const-class v5, Lorg/droidparts/annotation/inject/InjectParentActivity;

    if-ne v5, v2, :cond_4

    .line 72
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectParentActivityAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectParentActivity;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectParentActivityAnn;-><init>(Lorg/droidparts/annotation/inject/InjectParentActivity;)V

    goto :goto_1

    .line 73
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_4
    const-class v5, Lorg/droidparts/annotation/inject/InjectResource;

    if-ne v5, v2, :cond_5

    .line 74
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectResourceAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectResource;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectResourceAnn;-><init>(Lorg/droidparts/annotation/inject/InjectResource;)V

    goto :goto_1

    .line 75
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_5
    const-class v5, Lorg/droidparts/annotation/inject/InjectSystemService;

    if-ne v5, v2, :cond_6

    .line 76
    new-instance v5, Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;

    check-cast v0, Lorg/droidparts/annotation/inject/InjectSystemService;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;-><init>(Lorg/droidparts/annotation/inject/InjectSystemService;)V

    goto :goto_1

    .line 61
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_7
    const/4 v5, 0x0

    goto :goto_1
.end method

.method static getJSONAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/serialize/JSONAnn;
    .locals 6
    .param p0, "f"    # Ljava/lang/reflect/Field;

    .prologue
    .line 83
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 84
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 85
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/serialize/JSON;

    if-ne v5, v2, :cond_0

    .line 86
    new-instance v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    check-cast v0, Lorg/droidparts/annotation/serialize/JSON;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/serialize/JSONAnn;-><init>(Lorg/droidparts/annotation/serialize/JSON;)V

    .line 89
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 83
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method static getReceiveEventsAnn(Ljava/lang/reflect/Method;)Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;
    .locals 6
    .param p0, "m"    # Ljava/lang/reflect/Method;

    .prologue
    .line 113
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 114
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 115
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/bus/ReceiveEvents;

    if-ne v5, v2, :cond_0

    .line 116
    new-instance v5, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;

    check-cast v0, Lorg/droidparts/annotation/bus/ReceiveEvents;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;-><init>(Lorg/droidparts/annotation/bus/ReceiveEvents;)V

    .line 119
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 113
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 119
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method static getTableAnn(Ljava/lang/Class;)Lorg/droidparts/inner/ann/sql/TableAnn;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/droidparts/inner/ann/sql/TableAnn;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 52
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 53
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/sql/Table;

    if-ne v5, v2, :cond_0

    .line 54
    new-instance v5, Lorg/droidparts/inner/ann/sql/TableAnn;

    check-cast v0, Lorg/droidparts/annotation/sql/Table;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/sql/TableAnn;-><init>(Lorg/droidparts/annotation/sql/Table;)V

    .line 57
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 51
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method static getXMLAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/serialize/XMLAnn;
    .locals 6
    .param p0, "f"    # Ljava/lang/reflect/Field;

    .prologue
    .line 93
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 94
    .local v0, "a":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 95
    .local v2, "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Lorg/droidparts/annotation/serialize/XML;

    if-ne v5, v2, :cond_0

    .line 96
    new-instance v5, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    check-cast v0, Lorg/droidparts/annotation/serialize/XML;

    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    invoke-direct {v5, v0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;-><init>(Lorg/droidparts/annotation/serialize/XML;)V

    .line 99
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return-object v5

    .line 93
    .restart local v0    # "a":Ljava/lang/annotation/Annotation;
    .restart local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "a":Ljava/lang/annotation/Annotation;
    .end local v2    # "at":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method
