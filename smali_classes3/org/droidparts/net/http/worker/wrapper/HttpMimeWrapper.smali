.class public Lorg/droidparts/net/http/worker/wrapper/HttpMimeWrapper;
.super Ljava/lang/Object;
.source "HttpMimeWrapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildMultipartEntity(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/apache/http/HttpEntity;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "contentType"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 31
    new-instance v1, Lorg/apache/http/entity/mime/MultipartEntity;

    invoke-direct {v1}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 33
    .local v1, "entity":Lorg/apache/http/entity/mime/MultipartEntity;
    if-eqz p1, :cond_0

    .line 34
    new-instance v0, Lorg/apache/http/entity/mime/content/FileBody;

    const-string v2, "utf-8"

    invoke-direct {v0, p2, p1, v2}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .local v0, "contentBody":Lorg/apache/http/entity/mime/content/ContentBody;
    :goto_0
    invoke-virtual {v1, p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 39
    return-object v1

    .line 36
    .end local v0    # "contentBody":Lorg/apache/http/entity/mime/content/ContentBody;
    :cond_0
    new-instance v0, Lorg/apache/http/entity/mime/content/FileBody;

    invoke-direct {v0, p2}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;)V

    .restart local v0    # "contentBody":Lorg/apache/http/entity/mime/content/ContentBody;
    goto :goto_0
.end method
