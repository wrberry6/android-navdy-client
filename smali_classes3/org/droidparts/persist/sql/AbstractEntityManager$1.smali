.class Lorg/droidparts/persist/sql/AbstractEntityManager$1;
.super Ljava/lang/Object;
.source "AbstractEntityManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/droidparts/persist/sql/AbstractEntityManager;->cud(Ljava/util/Collection;I)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/droidparts/persist/sql/AbstractEntityManager;

.field final synthetic val$items:Ljava/util/Collection;

.field final synthetic val$operation:I


# direct methods
.method constructor <init>(Lorg/droidparts/persist/sql/AbstractEntityManager;Ljava/util/Collection;I)V
    .locals 0

    .prologue
    .line 102
    .local p0, "this":Lorg/droidparts/persist/sql/AbstractEntityManager$1;, "Lorg/droidparts/persist/sql/AbstractEntityManager.1;"
    iput-object p1, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->this$0:Lorg/droidparts/persist/sql/AbstractEntityManager;

    iput-object p2, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->val$items:Ljava/util/Collection;

    iput p3, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->val$operation:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lorg/droidparts/persist/sql/AbstractEntityManager$1;, "Lorg/droidparts/persist/sql/AbstractEntityManager.1;"
    const/4 v0, 0x0

    .line 107
    .local v0, "count":I
    iget-object v4, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->val$items:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/droidparts/model/Entity;

    .line 108
    .local v2, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    const/4 v3, 0x0

    .line 109
    .local v3, "success":Z
    iget v4, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->val$operation:I

    packed-switch v4, :pswitch_data_0

    .line 120
    :goto_1
    if-eqz v3, :cond_0

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :pswitch_0
    iget-object v4, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->this$0:Lorg/droidparts/persist/sql/AbstractEntityManager;

    invoke-virtual {v4, v2}, Lorg/droidparts/persist/sql/AbstractEntityManager;->create(Lorg/droidparts/model/Entity;)Z

    move-result v3

    .line 112
    goto :goto_1

    .line 114
    :pswitch_1
    iget-object v4, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->this$0:Lorg/droidparts/persist/sql/AbstractEntityManager;

    invoke-virtual {v4, v2}, Lorg/droidparts/persist/sql/AbstractEntityManager;->update(Lorg/droidparts/model/Entity;)Z

    move-result v3

    .line 115
    goto :goto_1

    .line 117
    :pswitch_2
    iget-object v4, p0, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->this$0:Lorg/droidparts/persist/sql/AbstractEntityManager;

    iget-wide v6, v2, Lorg/droidparts/model/Entity;->id:J

    invoke-virtual {v4, v6, v7}, Lorg/droidparts/persist/sql/AbstractEntityManager;->delete(J)Z

    move-result v3

    goto :goto_1

    .line 124
    .end local v2    # "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    .end local v3    # "success":Z
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    return-object v4

    .line 109
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lorg/droidparts/persist/sql/AbstractEntityManager$1;, "Lorg/droidparts/persist/sql/AbstractEntityManager.1;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/AbstractEntityManager$1;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
