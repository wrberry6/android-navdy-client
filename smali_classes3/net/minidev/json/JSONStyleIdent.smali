.class public Lnet/minidev/json/JSONStyleIdent;
.super Lnet/minidev/json/JSONStyle;
.source "JSONStyleIdent.java"


# instance fields
.field deep:I

.field identChar:C

.field newline:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lnet/minidev/json/JSONStyle;-><init>()V

    .line 26
    const/16 v0, 0x20

    iput-char v0, p0, Lnet/minidev/json/JSONStyleIdent;->identChar:C

    .line 27
    const-string v0, "\n"

    iput-object v0, p0, Lnet/minidev/json/JSONStyleIdent;->newline:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 36
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "FLAG"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyle;-><init>(I)V

    .line 26
    const/16 v0, 0x20

    iput-char v0, p0, Lnet/minidev/json/JSONStyleIdent;->identChar:C

    .line 27
    const-string v0, "\n"

    iput-object v0, p0, Lnet/minidev/json/JSONStyleIdent;->newline:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 32
    return-void
.end method

.method private ident(Ljava/lang/Appendable;)V
    .locals 2
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v1, p0, Lnet/minidev/json/JSONStyleIdent;->newline:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    if-ge v0, v1, :cond_0

    .line 41
    iget-char v1, p0, Lnet/minidev/json/JSONStyleIdent;->identChar:C

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public arrayNextElm(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 118
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 119
    return-void
.end method

.method public arrayObjectEnd(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    return-void
.end method

.method public arrayStart(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/16 v0, 0x5b

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 94
    iget v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 95
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 96
    return-void
.end method

.method public arrayStop(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 103
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 104
    const/16 v0, 0x5d

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 105
    return-void
.end method

.method public arrayfirstObject(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    return-void
.end method

.method public objectElmStop(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    return-void
.end method

.method public objectEndOfKey(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    const/16 v0, 0x3a

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 87
    return-void
.end method

.method public objectFirstStart(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public objectNext(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 73
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 74
    return-void
.end method

.method public objectStart(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const/16 v0, 0x7b

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 49
    iget v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 50
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 51
    return-void
.end method

.method public objectStop(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lnet/minidev/json/JSONStyleIdent;->deep:I

    .line 58
    invoke-direct {p0, p1}, Lnet/minidev/json/JSONStyleIdent;->ident(Ljava/lang/Appendable;)V

    .line 59
    const/16 v0, 0x7d

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 60
    return-void
.end method
