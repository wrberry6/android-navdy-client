.class Lcom/nimbusds/jose/crypto/ConstantTimeUtils;
.super Ljava/lang/Object;
.source "ConstantTimeUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static areEqual([B[B)Z
    .locals 5
    .param p0, "a"    # [B
    .param p1, "b"    # [B

    .prologue
    const/4 v2, 0x0

    .line 27
    array-length v3, p0

    array-length v4, p1

    if-eq v3, v4, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v2

    .line 31
    :cond_1
    const/4 v1, 0x0

    .line 32
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p0

    if-lt v0, v3, :cond_2

    .line 36
    if-nez v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 33
    :cond_2
    aget-byte v3, p0, v0

    aget-byte v4, p1, v0

    xor-int/2addr v3, v4

    or-int/2addr v1, v3

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
