.class Lcom/localytics/android/PushManager$4;
.super Landroid/os/AsyncTask;
.source "PushManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/PushManager;->pushIntegrationUpload(Ljava/lang/String;ZLjava/lang/String;Lcom/localytics/android/PushManager$POSTBodyBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/PushManager;

.field final synthetic val$builder:Lcom/localytics/android/PushManager$POSTBodyBuilder;

.field final synthetic val$canPresentToast:Z

.field final synthetic val$successMessage:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/localytics/android/PushManager;Lcom/localytics/android/PushManager$POSTBodyBuilder;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/localytics/android/PushManager$4;->this$0:Lcom/localytics/android/PushManager;

    iput-object p2, p0, Lcom/localytics/android/PushManager$4;->val$builder:Lcom/localytics/android/PushManager$POSTBodyBuilder;

    iput-object p3, p0, Lcom/localytics/android/PushManager$4;->val$url:Ljava/lang/String;

    iput-object p4, p0, Lcom/localytics/android/PushManager$4;->val$successMessage:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/localytics/android/PushManager$4;->val$canPresentToast:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 449
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/PushManager$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 453
    const-string v4, "There was an unexpected network error while connecting to the Dashboard. Please try again."

    .line 456
    .local v4, "errorMessageForUser":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/localytics/android/PushManager$4;->val$builder:Lcom/localytics/android/PushManager$POSTBodyBuilder;

    invoke-interface {v6}, Lcom/localytics/android/PushManager$POSTBodyBuilder;->getBody()Ljava/lang/String;

    move-result-object v1

    .line 457
    .local v1, "body":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 459
    const-string v6, "POST body for push integration is empty"

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 499
    .end local v1    # "body":Ljava/lang/String;
    .end local v4    # "errorMessageForUser":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 463
    .restart local v1    # "body":Ljava/lang/String;
    .restart local v4    # "errorMessageForUser":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    .line 466
    .local v2, "connection":Ljava/net/HttpURLConnection;
    :try_start_1
    new-instance v6, Ljava/net/URL;

    iget-object v7, p0, Lcom/localytics/android/PushManager$4;->val$url:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/localytics/android/PushManager$4;->this$0:Lcom/localytics/android/PushManager;

    iget-object v7, v7, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v7}, Lcom/localytics/android/LocalyticsDao;->getProxy()Ljava/net/Proxy;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/localytics/android/BaseUploadThread;->createURLConnection(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 467
    const/16 v6, 0x1388

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 468
    const/16 v6, 0x1388

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 469
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 470
    const-string v6, "POST"

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 471
    const-string v6, "Content-Type"

    const-string v7, "application/json"

    invoke-virtual {v2, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 474
    .local v5, "outputStream":Ljava/io/OutputStream;
    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write([B)V

    .line 475
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 477
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    if-eqz v2, :cond_2

    .line 488
    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 499
    :cond_2
    iget-object v4, p0, Lcom/localytics/android/PushManager$4;->val$successMessage:Ljava/lang/String;

    goto :goto_0

    .line 479
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    :catch_0
    move-exception v3

    .line 481
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "POST for push integration has failed"

    invoke-static {v6, v3}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 486
    if-eqz v2, :cond_0

    .line 488
    :try_start_4
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 493
    .end local v1    # "body":Ljava/lang/String;
    .end local v2    # "connection":Ljava/net/HttpURLConnection;
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 495
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v6, "POST for push integration has failed"

    invoke-static {v6, v3}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 486
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "body":Ljava/lang/String;
    .restart local v2    # "connection":Ljava/net/HttpURLConnection;
    :catchall_0
    move-exception v6

    if-eqz v2, :cond_3

    .line 488
    :try_start_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 449
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/PushManager$4;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 507
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/localytics/android/PushManager$4;->val$canPresentToast:Z

    if-eqz v1, :cond_0

    .line 509
    iget-object v1, p0, Lcom/localytics/android/PushManager$4;->this$0:Lcom/localytics/android/PushManager;

    iget-object v1, v1, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 512
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Exception while handling device info"

    invoke-static {v1, v0}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
