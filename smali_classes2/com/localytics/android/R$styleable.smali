.class public final Lcom/localytics/android/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x2

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_ambientEnabled:I = 0x10

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_liteMode:I = 0x6

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x7

.field public static final MapAttrs_uiMapToolbar:I = 0xf

.field public static final MapAttrs_uiRotateGestures:I = 0x8

.field public static final MapAttrs_uiScrollGestures:I = 0x9

.field public static final MapAttrs_uiTiltGestures:I = 0xa

.field public static final MapAttrs_uiZoomControls:I = 0xb

.field public static final MapAttrs_uiZoomGestures:I = 0xc

.field public static final MapAttrs_useViewLifecycle:I = 0xd

.field public static final MapAttrs_zOrderOnTop:I = 0xe

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 202
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/localytics/android/R$styleable;->AdsAttrs:[I

    .line 206
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/localytics/android/R$styleable;->LoadingImageView:[I

    .line 210
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/localytics/android/R$styleable;->MapAttrs:[I

    .line 228
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/localytics/android/R$styleable;->SignInButton:[I

    return-void

    .line 202
    :array_0
    .array-data 4
        0x7f01005a
        0x7f01005b
        0x7f01005c
    .end array-data

    .line 206
    :array_1
    .array-data 4
        0x7f010175
        0x7f010176
        0x7f010177
    .end array-data

    .line 210
    :array_2
    .array-data 4
        0x7f010178
        0x7f010179
        0x7f01017a
        0x7f01017b
        0x7f01017c
        0x7f01017d
        0x7f01017e
        0x7f01017f
        0x7f010180
        0x7f010181
        0x7f010182
        0x7f010183
        0x7f010184
        0x7f010185
        0x7f010186
        0x7f010187
        0x7f010188
        0x7f010189
        0x7f01018a
        0x7f01018b
        0x7f01018c
        0x7f01018d
        0x7f01018e
    .end array-data

    .line 228
    :array_3
    .array-data 4
        0x7f0101d6
        0x7f0101d7
        0x7f0101d8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
