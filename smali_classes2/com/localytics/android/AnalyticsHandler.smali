.class Lcom/localytics/android/AnalyticsHandler;
.super Lcom/localytics/android/BaseHandler;
.source "AnalyticsHandler.java"

# interfaces
.implements Lcom/localytics/android/LocationListener;


# static fields
.field private static final BASE_PUSH_REGISTRATION_BACKOFF:J = 0x1388L

.field private static final MAX_PUSH_REGISTRATION_RETRIES:I = 0x3

.field private static final MESSAGE_CLOSE:I = 0x66

.field private static final MESSAGE_DISABLE_NOTIFICATIONS:I = 0x6e

.field private static final MESSAGE_OPEN:I = 0x65

.field private static final MESSAGE_OPT_OUT:I = 0x6c

.field private static final MESSAGE_REGISTER_PUSH:I = 0x6d

.field private static final MESSAGE_RETRIEVE_TOKEN_FROM_INSTANCEID:I = 0x71

.field private static final MESSAGE_SET_CUSTOM_DIMENSION:I = 0x6b

.field private static final MESSAGE_SET_IDENTIFIER:I = 0x69

.field private static final MESSAGE_SET_LOCATION:I = 0x6a

.field private static final MESSAGE_SET_PUSH_REGID:I = 0x6f

.field private static final MESSAGE_SET_REFERRERID:I = 0x70

.field private static final MESSAGE_TAG_EVENT:I = 0x67

.field private static final MESSAGE_TAG_SCREEN:I = 0x68

.field private static final PARAM_LOCALYTICS_REFERRER_TEST_MODE:Ljava/lang/String; = "localytics_test_mode"

.field private static final PROJECTION_SET_CUSTOM_DIMENSION:[Ljava/lang/String;

.field private static final PROJECTION_SET_IDENTIFIER:[Ljava/lang/String;

.field private static final SELECTION_SET_CUSTOM_DIMENSION:Ljava/lang/String;

.field private static final SELECTION_SET_IDENTIFIER:Ljava/lang/String;

.field private static sLastLocation:Landroid/location/Location;

.field private static sNumPushRegistrationRetries:I


# instance fields
.field private mAppWasUpgraded:Z

.field private mCachedCustomDimensions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedIdentifiers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFirstSessionEver:Z

.field private mInstallId:Ljava/lang/String;

.field mIsSessionOpen:Z

.field private mLastScreenTag:Ljava/lang/String;

.field protected final mListeners:Lcom/localytics/android/ListenersSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/localytics/android/ListenersSet",
            "<",
            "Lcom/localytics/android/AnalyticsListener;",
            ">;"
        }
    .end annotation
.end field

.field mReferrerTestModeEnabled:Z

.field private mSanitizingDictionary:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSentReferrerTestMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 131
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "custom_dimension_value"

    aput-object v1, v0, v3

    sput-object v0, Lcom/localytics/android/AnalyticsHandler;->PROJECTION_SET_CUSTOM_DIMENSION:[Ljava/lang/String;

    .line 136
    const-string v0, "%s = ?"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "custom_dimension_key"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_CUSTOM_DIMENSION:Ljava/lang/String;

    .line 141
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "key"

    aput-object v1, v0, v3

    const-string v1, "value"

    aput-object v1, v0, v4

    sput-object v0, Lcom/localytics/android/AnalyticsHandler;->PROJECTION_SET_IDENTIFIER:[Ljava/lang/String;

    .line 146
    const-string v0, "%s = ?"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "key"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_IDENTIFIER:Ljava/lang/String;

    .line 152
    const/4 v0, 0x0

    sput-object v0, Lcom/localytics/android/AnalyticsHandler;->sLastLocation:Landroid/location/Location;

    .line 156
    sput v3, Lcom/localytics/android/AnalyticsHandler;->sNumPushRegistrationRetries:I

    return-void
.end method

.method constructor <init>(Lcom/localytics/android/LocalyticsDao;Landroid/os/Looper;)V
    .locals 3
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    const/4 v0, 0x0

    .line 190
    invoke-direct {p0, p1, p2}, Lcom/localytics/android/BaseHandler;-><init>(Lcom/localytics/android/LocalyticsDao;Landroid/os/Looper;)V

    .line 166
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mAppWasUpgraded:Z

    .line 167
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    .line 168
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mReferrerTestModeEnabled:Z

    .line 169
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mSentReferrerTestMode:Z

    .line 171
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    .line 191
    const-string v0, "Analytics"

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->siloName:Ljava/lang/String;

    .line 192
    new-instance v0, Lcom/localytics/android/ListenersSet;

    const-class v1, Lcom/localytics/android/AnalyticsListener;

    invoke-direct {v0, v1}, Lcom/localytics/android/ListenersSet;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    .line 193
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 195
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mSanitizingDictionary:Ljava/util/HashMap;

    .line 196
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mSanitizingDictionary:Ljava/util/HashMap;

    const-string v1, "facebook"

    const-string v2, "Facebook"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mSanitizingDictionary:Ljava/util/HashMap;

    const-string v1, "twitter"

    const-string v2, "Twitter"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mSanitizingDictionary:Ljava/util/HashMap;

    const-string v1, "native"

    const-string v2, "Native"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    return-void
.end method

.method private _addLocationIDsAndCustomDimensions(Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "eventBlob"    # Lorg/json/JSONObject;
    .param p2, "identifiers"    # Lorg/json/JSONObject;
    .param p3, "customerID"    # Ljava/lang/String;
    .param p4, "userType"    # Ljava/lang/String;

    .prologue
    .line 1387
    :try_start_0
    sget-object v2, Lcom/localytics/android/AnalyticsHandler;->sLastLocation:Landroid/location/Location;

    if-eqz v2, :cond_0

    .line 1389
    sget-object v2, Lcom/localytics/android/AnalyticsHandler;->sLastLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    .line 1390
    .local v10, "lat":D
    sget-object v2, Lcom/localytics/android/AnalyticsHandler;->sLastLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    .line 1391
    .local v12, "lng":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v10, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v12, v2

    if-eqz v2, :cond_0

    .line 1393
    const-string v2, "lat"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1394
    const-string v2, "lng"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1398
    .end local v10    # "lat":D
    .end local v12    # "lng":D
    :cond_0
    const-string v2, "cid"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1399
    const-string v2, "utp"

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1401
    invoke-virtual/range {p2 .. p2}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1403
    const-string v2, "ids"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1409
    :cond_1
    const/4 v8, 0x0

    .line 1412
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "custom_dimensions"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1414
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1416
    const-string v2, "custom_dimension_key"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1417
    .local v9, "key":Ljava/lang/String;
    const-string v2, "custom_dimension_value"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1418
    .local v15, "value":Ljava/lang/String;
    const-string v2, "custom_dimension_"

    const-string v3, "c"

    invoke-virtual {v9, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 1419
    .local v14, "newKey":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1424
    .end local v9    # "key":Ljava/lang/String;
    .end local v14    # "newKey":Ljava/lang/String;
    .end local v15    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_2

    .line 1426
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1427
    const/4 v8, 0x0

    :cond_2
    throw v2

    .line 1431
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 1434
    :cond_3
    :goto_1
    return-void

    .line 1424
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_4
    if-eqz v8, :cond_3

    .line 1426
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1427
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private _areNotificationsDisabled()Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 912
    const/4 v7, 0x0

    .line 913
    .local v7, "disabled":Z
    const/4 v6, 0x0

    .line 916
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "push_disabled"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 917
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 919
    const-string v0, "push_disabled"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 920
    .local v8, "disabledBit":I
    if-ne v8, v9, :cond_0

    move v7, v9

    .line 921
    :goto_1
    goto :goto_0

    :cond_0
    move v7, v10

    .line 920
    goto :goto_1

    .line 925
    .end local v8    # "disabledBit":I
    :cond_1
    if-eqz v6, :cond_2

    .line 927
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 931
    :cond_2
    return v7

    .line 925
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 927
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private _clearScreens()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1469
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "screens"

    invoke-virtual {v0, v1, v2, v2}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1470
    iput-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mLastScreenTag:Ljava/lang/String;

    .line 1471
    return-void
.end method

.method private _createCloseBlob(Lcom/localytics/android/AnalyticsHeader;ZJ)Ljava/lang/String;
    .locals 11
    .param p1, "header"    # Lcom/localytics/android/AnalyticsHeader;
    .param p2, "isRecoveredClose"    # Z
    .param p3, "currentTime"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide v8, 0x408f400000000000L    # 1000.0

    .line 1295
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1296
    .local v1, "eventUUID":Ljava/lang/String;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1297
    .local v0, "eventBlob":Lorg/json/JSONObject;
    const-string v5, "dt"

    const-string v6, "c"

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1298
    const-string v5, "u"

    invoke-virtual {v0, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1299
    const-string v5, "su"

    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getOpenSessionUUID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1300
    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getLastSessionStartTime()J

    move-result-wide v2

    .line 1301
    .local v2, "lastSessionStartTime":J
    const-string v5, "ss"

    long-to-double v6, v2

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1302
    const-string v5, "ct"

    long-to-double v6, p3

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1303
    const-string v5, "ctl"

    sub-long v6, p3, v2

    long-to-double v6, v6

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1305
    new-instance v4, Lorg/json/JSONArray;

    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getScreens()Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 1306
    .local v4, "screens":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 1308
    const-string v5, "fl"

    invoke-virtual {v0, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1310
    :cond_0
    const-string v5, "isl"

    invoke-virtual {v0, v5, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1312
    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getIdentifiersObject()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getCustomerID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getUserType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v0, v5, v6, v7}, Lcom/localytics/android/AnalyticsHandler;->_addLocationIDsAndCustomDimensions(Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const-string v5, "%s\n%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/localytics/android/AnalyticsHeader;->getHeaderBlob()Lorg/json/JSONObject;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private _dequeQueuedCloseSessionTag(Z)V
    .locals 12
    .param p1, "saveAsEvent"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1548
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1550
    .local v9, "values":Landroid/content/ContentValues;
    if-eqz p1, :cond_3

    .line 1552
    const/4 v6, 0x0

    .line 1553
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1554
    .local v7, "queuedCloseSessionTag":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1557
    .local v8, "queuedCloseSessionTagUploadFormat":I
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "queued_close_session_blob"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "queued_close_session_blob_upload_format"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1558
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1560
    const-string v1, "queued_close_session_blob"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1561
    const-string v1, "queued_close_session_blob_upload_format"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 1566
    :cond_0
    if-eqz v6, :cond_1

    .line 1568
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1569
    const/4 v6, 0x0

    .line 1573
    :cond_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    if-nez v1, :cond_2

    .line 1577
    :try_start_1
    new-instance v0, Lcom/localytics/android/AnalyticsHeader;

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/AnalyticsHeader;-><init>(Lcom/localytics/android/AnalyticsHandler;Lcom/localytics/android/BaseProvider;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    .local v0, "header":Lcom/localytics/android/AnalyticsHeader;
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v2}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->_createCloseBlob(Lcom/localytics/android/AnalyticsHeader;ZJ)Ljava/lang/String;

    move-result-object v7

    .line 1579
    sget-object v1, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v1}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v8

    .line 1586
    .end local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    :cond_2
    :goto_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1588
    const-string v1, "blob"

    invoke-virtual {v9, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    const-string v1, "upload_format"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1590
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "events"

    invoke-virtual {v1, v2, v9}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1591
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 1592
    iput-boolean v11, p0, Lcom/localytics/android/AnalyticsHandler;->mAppWasUpgraded:Z

    .line 1593
    iput-boolean v11, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    .line 1594
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_clearScreens()V

    .line 1598
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "queuedCloseSessionTag":Ljava/lang/String;
    .end local v8    # "queuedCloseSessionTagUploadFormat":I
    :cond_3
    const-string v1, "queued_close_session_blob"

    invoke-virtual {v9, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1599
    const-string v1, "queued_close_session_blob_upload_format"

    invoke-virtual {v9, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1600
    const-string v1, "last_session_close_time"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1601
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "info"

    invoke-virtual {v1, v2, v9, v10, v10}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1602
    return-void

    .line 1566
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "queuedCloseSessionTag":Ljava/lang/String;
    .restart local v8    # "queuedCloseSessionTagUploadFormat":I
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 1568
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1569
    const/4 v6, 0x0

    :cond_4
    throw v1

    .line 1581
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private _getCustomDimension(I)Ljava/lang/String;
    .locals 9
    .param p1, "dimension"    # I

    .prologue
    const/4 v8, 0x0

    .line 954
    if-ltz p1, :cond_0

    const/16 v0, 0x14

    if-lt p1, v0, :cond_1

    .line 980
    :cond_0
    :goto_0
    return-object v8

    .line 959
    :cond_1
    const/4 v8, 0x0

    .line 960
    .local v8, "value":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->getCustomDimensionAttributeKey(I)Ljava/lang/String;

    move-result-object v7

    .line 961
    .local v7, "key":Ljava/lang/String;
    const/4 v6, 0x0

    .line 964
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "custom_dimensions"

    sget-object v2, Lcom/localytics/android/AnalyticsHandler;->PROJECTION_SET_CUSTOM_DIMENSION:[Ljava/lang/String;

    sget-object v3, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_CUSTOM_DIMENSION:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 966
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 968
    const-string v0, "custom_dimension_value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 973
    :cond_2
    if-eqz v6, :cond_0

    .line 975
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 976
    const/4 v6, 0x0

    goto :goto_0

    .line 973
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 975
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 976
    const/4 v6, 0x0

    :cond_3
    throw v0
.end method

.method private _getPushRegistrationId()Ljava/lang/String;
    .locals 8
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 888
    const/4 v7, 0x0

    .line 889
    .local v7, "pushRegId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 892
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "registration_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 894
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    const-string v0, "registration_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 901
    :cond_0
    if-eqz v6, :cond_1

    .line 903
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 907
    :cond_1
    if-nez v7, :cond_2

    const-string v7, ""

    .end local v7    # "pushRegId":Ljava/lang/String;
    :cond_2
    return-object v7

    .line 901
    .restart local v7    # "pushRegId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 903
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private _registerPush(Ljava/lang/String;)V
    .locals 2
    .param p1, "newSenderID"    # Ljava/lang/String;

    .prologue
    .line 873
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getPushSenderId()Ljava/lang/String;

    move-result-object v0

    .line 876
    .local v0, "senderId":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 879
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/localytics/android/AnalyticsHandler;->_setPushID(Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_0
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_retrieveTokenFromInstanceId(Ljava/lang/String;)V

    .line 883
    return-void
.end method

.method private _retrieveLastScreenTag()Ljava/lang/String;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1476
    const/4 v0, 0x0

    .line 1477
    .local v0, "lastScreen":Ljava/lang/String;
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v2, v2, Lcom/localytics/android/BaseProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "SELECT MAX(rowid), %s FROM %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "name"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "screens"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1481
    .local v1, "query":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1483
    const-string v2, "name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1485
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1487
    return-object v0
.end method

.method private _retrieveTokenFromInstanceId()V
    .locals 1

    .prologue
    .line 2012
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getPushSenderId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->_retrieveTokenFromInstanceId(Ljava/lang/String;)V

    .line 2013
    return-void
.end method

.method private _retrieveTokenFromInstanceId(Ljava/lang/String;)V
    .locals 10
    .param p1, "senderId"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 2017
    iget-object v6, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "com.google.android.c2dm.permission.RECEIVE"

    invoke-static {v6, v7}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 2022
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2024
    const-string v6, "GCM registration failed, got empty sender id"

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 2063
    :cond_0
    :goto_0
    return-void

    .line 2028
    :cond_1
    iget-object v6, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v4

    .line 2029
    .local v4, "instanceID":Lcom/google/android/gms/iid/InstanceID;
    const-string v6, "GCM"

    invoke-virtual {v4, p1, v6}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2030
    .local v5, "token":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2032
    const-string v6, "GCM registered, new id: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 2033
    invoke-direct {p0, v5}, Lcom/localytics/android/AnalyticsHandler;->_setPushID(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2040
    .end local v4    # "instanceID":Lcom/google/android/gms/iid/InstanceID;
    .end local v5    # "token":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 2042
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "Exception while registering GCM"

    invoke-static {v6, v2}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2043
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 2044
    .local v3, "error":Ljava/lang/String;
    const-string v6, "SERVICE_NOT_AVAILABLE"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "TIMEOUT"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2046
    :cond_2
    sget v6, Lcom/localytics/android/AnalyticsHandler;->sNumPushRegistrationRetries:I

    add-int/lit8 v7, v6, 0x1

    sput v7, Lcom/localytics/android/AnalyticsHandler;->sNumPushRegistrationRetries:I

    const/4 v7, 0x3

    if-ge v6, v7, :cond_4

    .line 2048
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    sget v8, Lcom/localytics/android/AnalyticsHandler;->sNumPushRegistrationRetries:I

    add-int/lit8 v8, v8, -0x1

    int-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-long v6, v6

    const-wide/16 v8, 0x1388

    mul-long v0, v6, v8

    .line 2049
    .local v0, "delay":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GCM registration ERROR_SERVICE_NOT_AVAILABLE. Retrying in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " milliseconds."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 2050
    invoke-virtual {p0, p1, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->registerPush(Ljava/lang/String;J)V

    goto :goto_0

    .line 2037
    .end local v0    # "delay":J
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "error":Ljava/lang/String;
    .restart local v4    # "instanceID":Lcom/google/android/gms/iid/InstanceID;
    .restart local v5    # "token":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v6, "GCM registration failed, got empty token"

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 2054
    .end local v4    # "instanceID":Lcom/google/android/gms/iid/InstanceID;
    .end local v5    # "token":Ljava/lang/String;
    .restart local v2    # "e":Ljava/io/IOException;
    .restart local v3    # "error":Ljava/lang/String;
    :cond_4
    sput v9, Lcom/localytics/android/AnalyticsHandler;->sNumPushRegistrationRetries:I

    goto/16 :goto_0

    .line 2061
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "error":Ljava/lang/String;
    :cond_5
    const-string v6, "\'com.google.android.c2dm.permission.RECEIVE\' missing from AndroidManifest.xml"

    invoke-static {v6}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private _reuploadFirstSession(Ljava/lang/String;)V
    .locals 4
    .param p1, "referrerId"    # Ljava/lang/String;

    .prologue
    .line 2185
    invoke-virtual {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_replaceAttributionInFirstSession(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2186
    .local v0, "updatedEventBlob":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2188
    new-instance v1, Lcom/localytics/android/ReferralUploader;

    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getCustomerId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/localytics/android/ReferralUploader;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;Ljava/lang/String;Lcom/localytics/android/LocalyticsDao;)V

    invoke-virtual {v1}, Lcom/localytics/android/ReferralUploader;->start()V

    .line 2190
    :cond_0
    return-void
.end method

.method private _setCustomDimension(ILjava/lang/String;)V
    .locals 8
    .param p1, "dimension"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1642
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->getCustomDimensionAttributeKey(I)Ljava/lang/String;

    move-result-object v0

    .line 1643
    .local v0, "key":Ljava/lang/String;
    monitor-enter p0

    .line 1645
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1647
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "custom_dimensions"

    const-string v4, "%s = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "custom_dimension_key"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 1649
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669
    :cond_0
    :goto_0
    monitor-exit p0

    .line 1670
    return-void

    .line 1654
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1655
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "custom_dimension_key"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    const-string v2, "custom_dimension_value"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "custom_dimensions"

    sget-object v4, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_CUSTOM_DIMENSION:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 1659
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "custom_dimensions"

    invoke-virtual {v2, v3, v1}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1661
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1669
    .end local v1    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1666
    .restart local v1    # "values":Landroid/content/ContentValues;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private _setNotificationsDisabled(I)V
    .locals 4
    .param p1, "disabled"    # I

    .prologue
    const/4 v3, 0x0

    .line 801
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 802
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "push_disabled"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 803
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "info"

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 804
    return-void
.end method

.method private _setOptedOut(Z)V
    .locals 4
    .param p1, "isOptingOut"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1905
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_isOptedOut()Z

    move-result v1

    if-ne v1, p1, :cond_0

    .line 1920
    :goto_0
    return-void

    .line 1910
    :cond_0
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_tagOptEvent(Z)V

    .line 1912
    iget-boolean v1, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 1914
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_close()V

    .line 1917
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1918
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "opt_out"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1919
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "info"

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private _setPushID(Ljava/lang/String;)V
    .locals 6
    .param p1, "pushRegId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1964
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_getPushRegistrationId()Ljava/lang/String;

    move-result-object v1

    .line 1965
    .local v1, "oldRegistrationId":Ljava/lang/String;
    if-nez p1, :cond_1

    const-string v0, ""

    .line 1967
    .local v0, "newRegistrationId":Ljava/lang/String;
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1968
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "registration_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1969
    const-string v3, "registration_version"

    iget-object v4, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/DatapointHelper;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970
    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "info"

    invoke-virtual {v3, v4, v2, v5, v5}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1972
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1974
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_tagPushRegisteredEvent()V

    .line 1976
    :cond_0
    return-void

    .end local v0    # "newRegistrationId":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_1
    move-object v0, p1

    .line 1965
    goto :goto_0
.end method

.method private _setPushID(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "senderId"    # Ljava/lang/String;
    .param p2, "pushRegId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1980
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_getPushRegistrationId()Ljava/lang/String;

    move-result-object v1

    .line 1981
    .local v1, "oldRegistrationId":Ljava/lang/String;
    if-nez p2, :cond_1

    const-string v0, ""

    .line 1983
    .local v0, "newRegistrationId":Ljava/lang/String;
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1984
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "sender_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    const-string v3, "registration_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986
    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "info"

    invoke-virtual {v3, v4, v2, v5, v5}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1988
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1990
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_tagPushRegisteredEvent()V

    .line 1992
    :cond_0
    return-void

    .end local v0    # "newRegistrationId":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_1
    move-object v0, p2

    .line 1981
    goto :goto_0
.end method

.method private _setReferrerId(Ljava/lang/String;)V
    .locals 9
    .param p1, "referrerId"    # Ljava/lang/String;

    .prologue
    .line 2153
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2155
    const/4 v7, 0x0

    .line 2158
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "play_attribution"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2160
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2162
    const-string v0, "play_attribution"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2163
    .local v6, "currentReferrerID":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2165
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2166
    .local v8, "values":Landroid/content/ContentValues;
    const-string v0, "play_attribution"

    invoke-virtual {v8, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v8, v2, v3}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[REFERRAL] _setReferrerId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->i(Ljava/lang/String;)I

    .line 2169
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_reuploadFirstSession(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2175
    .end local v6    # "currentReferrerID":Ljava/lang/String;
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_0
    if-eqz v7, :cond_1

    .line 2177
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2181
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    return-void

    .line 2175
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 2177
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private _tagEvent(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Long;)V
    .locals 12
    .param p1, "event"    # Ljava/lang/String;
    .param p3, "clv"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1134
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v0, Lcom/localytics/android/AnalyticsHeader;

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/AnalyticsHeader;-><init>(Lcom/localytics/android/AnalyticsHandler;Lcom/localytics/android/BaseProvider;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    .local v0, "header":Lcom/localytics/android/AnalyticsHeader;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1137
    .local v10, "eventUUID":Ljava/lang/String;
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v8

    .line 1138
    .local v8, "currentTime":J
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1139
    .local v6, "appContext":Landroid/content/Context;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1140
    .local v11, "eventValues":Landroid/content/ContentValues;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 1141
    .local v7, "eventBlob":Lorg/json/JSONObject;
    const-string v1, "dt"

    const-string v2, "e"

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1142
    const-string v1, "ct"

    long-to-double v2, v8

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-virtual {v7, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1143
    const-string v1, "u"

    invoke-virtual {v7, v1, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1144
    const-string v2, "su"

    iget-boolean v1, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getOpenSessionUUID()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v7, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1145
    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1147
    const-string v1, "n"

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1157
    :goto_1
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1159
    const-string v1, "v"

    invoke-virtual {v7, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1162
    :cond_0
    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getIdentifiersObject()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getCustomerID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getUserType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v7, v1, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->_addLocationIDsAndCustomDimensions(Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    if-eqz p2, :cond_1

    .line 1166
    const-string v1, "attrs"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1169
    :cond_1
    const-string v1, "blob"

    const-string v2, "%s\n%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getHeaderBlob()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const-string v1, "upload_format"

    sget-object v2, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1172
    invoke-virtual {v11}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1174
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "events"

    invoke-virtual {v1, v2, v11}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1176
    :cond_2
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v1}, Lcom/localytics/android/ListenersSet;->getProxy()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/localytics/android/AnalyticsListener;

    if-nez p3, :cond_5

    const-wide/16 v2, 0x0

    :goto_2
    invoke-interface {v1, p1, p2, v2, v3}, Lcom/localytics/android/AnalyticsListener;->localyticsDidTagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 1181
    .end local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .end local v6    # "appContext":Landroid/content/Context;
    .end local v7    # "eventBlob":Lorg/json/JSONObject;
    .end local v8    # "currentTime":J
    .end local v10    # "eventUUID":Ljava/lang/String;
    .end local v11    # "eventValues":Landroid/content/ContentValues;
    :goto_3
    return-void

    .line 1144
    .restart local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .restart local v6    # "appContext":Landroid/content/Context;
    .restart local v7    # "eventBlob":Lorg/json/JSONObject;
    .restart local v8    # "currentTime":J
    .restart local v10    # "eventUUID":Ljava/lang/String;
    .restart local v11    # "eventValues":Landroid/content/ContentValues;
    :cond_3
    const-string v1, ""

    goto/16 :goto_0

    .line 1151
    :cond_4
    const-string v1, "n"

    invoke-virtual {v7, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 1178
    .end local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .end local v6    # "appContext":Landroid/content/Context;
    .end local v7    # "eventBlob":Lorg/json/JSONObject;
    .end local v8    # "currentTime":J
    .end local v10    # "eventUUID":Ljava/lang/String;
    .end local v11    # "eventValues":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    goto :goto_3

    .line 1176
    .restart local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .restart local v6    # "appContext":Landroid/content/Context;
    .restart local v7    # "eventBlob":Lorg/json/JSONObject;
    .restart local v8    # "currentTime":J
    .restart local v10    # "eventUUID":Ljava/lang/String;
    .restart local v11    # "eventValues":Landroid/content/ContentValues;
    :cond_5
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_2
.end method

.method private _tagOpenEvent()V
    .locals 20

    .prologue
    .line 1187
    :try_start_0
    new-instance v2, Lcom/localytics/android/AnalyticsHeader;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/localytics/android/AnalyticsHeader;-><init>(Lcom/localytics/android/AnalyticsHandler;Lcom/localytics/android/BaseProvider;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    .local v2, "header":Lcom/localytics/android/AnalyticsHeader;
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 1191
    .local v17, "infoValues":Landroid/content/ContentValues;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1192
    .local v14, "eventUUID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v8

    .line 1193
    .local v8, "currentTime":J
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 1194
    .local v11, "eventBlob":Lorg/json/JSONObject;
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1195
    .local v15, "eventValues":Landroid/content/ContentValues;
    const-string v3, "dt"

    const-string v4, "s"

    invoke-virtual {v11, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1196
    const-string v3, "ct"

    long-to-double v4, v8

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    invoke-virtual {v11, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1197
    const-string v3, "u"

    invoke-virtual {v11, v3, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1199
    const-wide/16 v12, 0x0

    .line 1200
    .local v12, "elapsedTime":J
    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getLastSessionStartTime()J

    move-result-wide v18

    .line 1201
    .local v18, "lastSessionStartTime":J
    const-wide/16 v4, 0x0

    cmp-long v3, v18, v4

    if-lez v3, :cond_0

    .line 1203
    sub-long v12, v8, v18

    .line 1206
    :cond_0
    const-string v3, "sl"

    long-to-double v4, v12

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    invoke-virtual {v11, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1211
    const-string v3, "nth"

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getSessionSequenceNumber()I

    move-result v4

    invoke-virtual {v11, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1213
    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getIdentifiersObject()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getCustomerID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getUserType()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3, v4, v5}, Lcom/localytics/android/AnalyticsHandler;->_addLocationIDsAndCustomDimensions(Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const-string v3, "%s\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getHeaderBlob()Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 1216
    .local v16, "headerEventBlob":Ljava/lang/String;
    const-string v3, "blob"

    move-object/from16 v0, v16

    invoke-virtual {v15, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    const-string v3, "upload_format"

    sget-object v4, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v4}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1219
    const-string v3, "last_session_open_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1220
    const-string v3, "next_session_number"

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsHeader;->getSessionSequenceNumber()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1221
    const-string v3, "current_session_uuid"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    if-eqz v3, :cond_1

    .line 1224
    const-string v3, "first_open_event_blob"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "info"

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1227
    invoke-virtual {v15}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 1229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v4, "events"

    invoke-virtual {v3, v4, v15}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236
    .end local v2    # "header":Lcom/localytics/android/AnalyticsHeader;
    .end local v8    # "currentTime":J
    .end local v11    # "eventBlob":Lorg/json/JSONObject;
    .end local v12    # "elapsedTime":J
    .end local v14    # "eventUUID":Ljava/lang/String;
    .end local v15    # "eventValues":Landroid/content/ContentValues;
    .end local v16    # "headerEventBlob":Ljava/lang/String;
    .end local v17    # "infoValues":Landroid/content/ContentValues;
    .end local v18    # "lastSessionStartTime":J
    :cond_2
    :goto_0
    return-void

    .line 1232
    :catch_0
    move-exception v10

    .line 1234
    .local v10, "e":Ljava/lang/Exception;
    const-string v3, "Failed to save session open event"

    invoke-static {v3, v10}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private _tagOptEvent(Z)V
    .locals 12
    .param p1, "isOptingOut"    # Z

    .prologue
    .line 1268
    :try_start_0
    new-instance v0, Lcom/localytics/android/AnalyticsHeader;

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/AnalyticsHeader;-><init>(Lcom/localytics/android/AnalyticsHandler;Lcom/localytics/android/BaseProvider;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    .local v0, "header":Lcom/localytics/android/AnalyticsHeader;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1271
    .local v10, "eventUUID":Ljava/lang/String;
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v6

    .line 1272
    .local v6, "currentTime":J
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 1273
    .local v9, "eventBlob":Lorg/json/JSONObject;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1274
    .local v11, "eventValues":Landroid/content/ContentValues;
    const-string v1, "dt"

    const-string v2, "o"

    invoke-virtual {v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1275
    const-string v1, "u"

    invoke-virtual {v9, v1, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1276
    const-string v1, "out"

    invoke-virtual {v9, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1277
    const-string v1, "ct"

    long-to-double v2, v6

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-virtual {v9, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1279
    const-string v1, "blob"

    const-string v2, "%s\n%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/localytics/android/AnalyticsHeader;->getHeaderBlob()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    const-string v1, "upload_format"

    sget-object v2, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1282
    invoke-virtual {v11}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1284
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "events"

    invoke-virtual {v1, v2, v11}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1291
    .end local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .end local v6    # "currentTime":J
    .end local v9    # "eventBlob":Lorg/json/JSONObject;
    .end local v10    # "eventUUID":Ljava/lang/String;
    .end local v11    # "eventValues":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    return-void

    .line 1287
    :catch_0
    move-exception v8

    .line 1289
    .local v8, "e":Ljava/lang/Exception;
    const-string v1, "Failed to save opt in/out event"

    invoke-static {v1, v8}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private _tagPushRegisteredEvent()V
    .locals 2

    .prologue
    .line 1996
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    const-string v1, "Localytics Push Registered"

    invoke-interface {v0, v1}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;)V

    .line 1997
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->upload()V

    .line 1998
    return-void
.end method

.method private _tagQueuedCloseEvent(Z)V
    .locals 11
    .param p1, "isRecoveredClose"    # Z

    .prologue
    .line 1242
    :try_start_0
    new-instance v0, Lcom/localytics/android/AnalyticsHeader;

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/localytics/android/AnalyticsHeader;-><init>(Lcom/localytics/android/AnalyticsHandler;Lcom/localytics/android/BaseProvider;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    .local v0, "header":Lcom/localytics/android/AnalyticsHeader;
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1245
    .local v10, "infoValues":Landroid/content/ContentValues;
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v8

    .line 1247
    .local v8, "currentTime":J
    if-nez p1, :cond_0

    .line 1249
    const-string v1, "last_session_close_time"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1252
    :cond_0
    invoke-direct {p0, v0, p1, v8, v9}, Lcom/localytics/android/AnalyticsHandler;->_createCloseBlob(Lcom/localytics/android/AnalyticsHeader;ZJ)Ljava/lang/String;

    move-result-object v6

    .line 1253
    .local v6, "closeBlob":Ljava/lang/String;
    const-string v1, "queued_close_session_blob"

    invoke-virtual {v10, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    const-string v1, "queued_close_session_blob_upload_format"

    sget-object v2, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v2}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1256
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "info"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v10, v3, v4}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1262
    .end local v0    # "header":Lcom/localytics/android/AnalyticsHeader;
    .end local v6    # "closeBlob":Ljava/lang/String;
    .end local v8    # "currentTime":J
    .end local v10    # "infoValues":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 1258
    :catch_0
    move-exception v7

    .line 1260
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "Failed to save queued session close event"

    invoke-static {v1, v7}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private _tagScreen(Ljava/lang/String;)V
    .locals 3
    .param p1, "screen"    # Ljava/lang/String;

    .prologue
    .line 1451
    iget-boolean v1, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    if-eqz v1, :cond_1

    .line 1453
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLastScreenTag:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1455
    iput-object p1, p0, Lcom/localytics/android/AnalyticsHandler;->mLastScreenTag:Ljava/lang/String;

    .line 1456
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1457
    .local v0, "screenValues":Landroid/content/ContentValues;
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "screens"

    invoke-virtual {v1, v2, v0}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1465
    .end local v0    # "screenValues":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    return-void

    .line 1463
    :cond_1
    const-string v1, "Screen not tagged because a session is not open"

    invoke-static {v1}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/localytics/android/AnalyticsHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_areNotificationsDisabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;Ljava/util/Map;Ljava/lang/Long;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/Map;
    .param p3, "x3"    # Ljava/lang/Long;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/localytics/android/AnalyticsHandler;->_tagEvent(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Long;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/localytics/android/AnalyticsHandler;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_getCustomDimension(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/localytics/android/AnalyticsHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mInstallId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/localytics/android/AnalyticsHandler;->PROJECTION_SET_IDENTIFIER:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_IDENTIFIER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_tagScreen(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/localytics/android/AnalyticsHandler;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/localytics/android/AnalyticsHandler;->_setCustomDimension(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_setPushID(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/localytics/android/AnalyticsHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_setNotificationsDisabled(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/localytics/android/AnalyticsHandler;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_setOptedOut(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_registerPush(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->_setReferrerId(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/localytics/android/AnalyticsHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/localytics/android/AnalyticsHandler;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_retrieveTokenFromInstanceId()V

    return-void
.end method

.method private getCustomDimensionAttributeKey(I)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 985
    if-ltz p1, :cond_0

    const/16 v0, 0x14

    if-ge p1, v0, :cond_0

    .line 987
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "custom_dimension_"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 992
    :goto_0
    return-object v0

    .line 991
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Custom dimension index cannot exceed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 992
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sanitizeMethodString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "method"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 548
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 549
    .local v0, "normalizedMethodString":Ljava/lang/String;
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mSanitizingDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 550
    .local v1, "sanitizedMethodString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 553
    .end local v1    # "sanitizedMethodString":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "sanitizedMethodString":Ljava/lang/String;
    :cond_0
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method protected _close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1616
    iget-boolean v1, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    if-nez v1, :cond_0

    .line 1618
    const-string v1, "Session was not open, so close is not possible."

    invoke-static {v1}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 1631
    :goto_0
    return-void

    .line 1622
    :cond_0
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v1}, Lcom/localytics/android/ListenersSet;->getProxy()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/localytics/android/AnalyticsListener;

    invoke-interface {v1}, Lcom/localytics/android/AnalyticsListener;->localyticsSessionWillClose()V

    .line 1624
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1625
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "last_session_close_time"

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v2}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1626
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v2, "info"

    invoke-virtual {v1, v2, v0, v5, v5}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1628
    invoke-direct {p0, v4}, Lcom/localytics/android/AnalyticsHandler;->_tagQueuedCloseEvent(Z)V

    .line 1630
    iput-boolean v4, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    goto :goto_0
.end method

.method protected _deleteUploadedData(I)V
    .locals 4
    .param p1, "maxRowToDelete"    # I

    .prologue
    .line 796
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "events"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 797
    return-void
.end method

.method protected _getCustomerId()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1010
    const/4 v6, 0x0

    .line 1011
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/localytics/android/AnalyticsHandler;->mInstallId:Ljava/lang/String;

    .line 1014
    .local v7, "customerId":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "customer_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1016
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1018
    const-string v0, "customer_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1023
    :cond_0
    if-eqz v6, :cond_1

    .line 1025
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1029
    :cond_1
    return-object v7

    .line 1023
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1025
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method protected _getDataToUpload()Ljava/util/TreeMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    new-instance v9, Ljava/util/TreeMap;

    invoke-direct {v9}, Ljava/util/TreeMap;-><init>()V

    .line 841
    .local v9, "eventsMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/4 v6, 0x0

    .line 844
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "events"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 846
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v9}, Ljava/util/TreeMap;->size()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    .line 848
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 849
    .local v8, "eventID":I
    const-string v0, "blob"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 850
    .local v7, "eventBlob":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 855
    .end local v7    # "eventBlob":Ljava/lang/String;
    .end local v8    # "eventID":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 857
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 858
    const/4 v6, 0x0

    .line 861
    :cond_0
    :goto_1
    return-object v9

    .line 855
    :cond_1
    if-eqz v6, :cond_0

    .line 857
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 858
    const/4 v6, 0x0

    goto :goto_1
.end method

.method _getIdentifiers()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    const/4 v6, 0x0

    .line 214
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 217
    .local v7, "customerIDs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "identifiers"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 221
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    const-string v0, "key"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 224
    .local v8, "key":Ljava/lang/String;
    const-string v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 225
    .local v9, "value":Ljava/lang/String;
    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 230
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 232
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 233
    const/4 v6, 0x0

    .line 236
    :cond_0
    :goto_1
    return-object v7

    .line 230
    :cond_1
    if-eqz v6, :cond_0

    .line 232
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 233
    const/4 v6, 0x0

    goto :goto_1
.end method

.method protected _getMaxRowToUpload()I
    .locals 9

    .prologue
    .line 815
    const/4 v7, 0x0

    .line 816
    .local v7, "numberOfAttributes":I
    const/4 v6, 0x0

    .line 819
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "events"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 820
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 822
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 827
    :cond_0
    if-eqz v6, :cond_1

    .line 829
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 830
    const/4 v6, 0x0

    :cond_1
    move v8, v7

    .line 833
    .end local v7    # "numberOfAttributes":I
    .local v8, "numberOfAttributes":I
    :goto_0
    return v8

    .line 827
    .end local v8    # "numberOfAttributes":I
    .restart local v7    # "numberOfAttributes":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 829
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 830
    const/4 v6, 0x0

    :cond_2
    move v8, v7

    .line 833
    .end local v7    # "numberOfAttributes":I
    .restart local v8    # "numberOfAttributes":I
    goto :goto_0
.end method

.method _getPushSenderId()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1094
    const/4 v6, 0x0

    .line 1097
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sender_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1099
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1101
    const-string v0, "sender_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1106
    if-eqz v6, :cond_0

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1112
    :cond_0
    :goto_0
    return-object v0

    .line 1106
    :cond_1
    if-eqz v6, :cond_2

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    .line 1112
    goto :goto_0

    .line 1106
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method _getScreens()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1319
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 1320
    .local v7, "screens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "screens"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1322
    .local v6, "query":Landroid/database/Cursor;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1324
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1326
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1328
    return-object v7
.end method

.method protected _getUploadThread(Ljava/util/TreeMap;Ljava/lang/String;)Lcom/localytics/android/BaseUploadThread;
    .locals 2
    .param p2, "customerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/localytics/android/BaseUploadThread;"
        }
    .end annotation

    .prologue
    .line 868
    .local p1, "data":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    new-instance v0, Lcom/localytics/android/AnalyticsUploadHandler;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/localytics/android/AnalyticsUploadHandler;-><init>(Lcom/localytics/android/BaseHandler;Ljava/util/TreeMap;Ljava/lang/String;Lcom/localytics/android/LocalyticsDao;)V

    return-object v0
.end method

.method protected _init()V
    .locals 3

    .prologue
    .line 2067
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    if-nez v0, :cond_0

    .line 2069
    new-instance v0, Lcom/localytics/android/AnalyticsProvider;

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->siloName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-direct {v0, v1, v2}, Lcom/localytics/android/AnalyticsProvider;-><init>(Ljava/lang/String;Lcom/localytics/android/LocalyticsDao;)V

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    .line 2072
    :cond_0
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_initApiKey()V

    .line 2073
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_initCachedIdentifiers()V

    .line 2074
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_initCachedCustomDimensions()V

    .line 2075
    return-void
.end method

.method protected _initApiKey()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 2079
    const/4 v7, 0x0

    .line 2082
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/DatapointHelper;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 2083
    .local v6, "appVersion":Ljava/lang/String;
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "app_version"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "uuid"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "next_session_number"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "customer_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2085
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088
    const-string v0, "Loading details for API key %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 2090
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2091
    .local v9, "values":Landroid/content/ContentValues;
    const-string v0, "app_version"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2093
    const-string v0, "app_version"

    invoke-virtual {v9, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mAppWasUpgraded:Z

    .line 2097
    :cond_0
    invoke-virtual {v9}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2099
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2102
    :cond_1
    const-string v0, "next_session_number"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v10, :cond_3

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    .line 2103
    const-string v0, "uuid"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mInstallId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2135
    :goto_1
    if-eqz v7, :cond_2

    .line 2137
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2141
    :cond_2
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_retrieveLastScreenTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLastScreenTag:Ljava/lang/String;

    .line 2143
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    invoke-virtual {v0}, Lcom/localytics/android/BaseProvider;->vacuumIfNecessary()V

    .line 2144
    return-void

    :cond_3
    move v0, v11

    .line 2102
    goto :goto_0

    .line 2108
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_4
    :try_start_1
    const-string v0, "Performing first-time initialization for new API key %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v3}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 2110
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2111
    .local v8, "installationID":Ljava/lang/String;
    iput-object v8, p0, Lcom/localytics/android/AnalyticsHandler;->mInstallId:Ljava/lang/String;

    .line 2112
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2113
    .restart local v9    # "values":Landroid/content/ContentValues;
    const-string v0, "api_key"

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    const-string v0, "uuid"

    invoke-virtual {v9, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2115
    const-string v0, "created_time"

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2116
    const-string v0, "opt_out"

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2117
    const-string v0, "push_disabled"

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2118
    const-string v0, "customer_id"

    invoke-virtual {v9, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    const-string v0, "user_type"

    const-string v1, "anonymous"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2120
    const-string v0, "fb_attribution"

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/localytics/android/DatapointHelper;->getFBAttribution(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    const-string v0, "first_android_id"

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/localytics/android/DatapointHelper;->getAndroidIdOrNull(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122
    const-string v0, "package_name"

    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123
    const-string v0, "app_version"

    invoke-virtual {v9, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2124
    const-string v0, "next_session_number"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2125
    const-string v0, "next_header_number"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2126
    const-string v0, "last_session_open_time"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2127
    const-string v0, "last_session_close_time"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2128
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    invoke-virtual {v0, v1, v9}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 2135
    .end local v6    # "appVersion":Ljava/lang/String;
    .end local v8    # "installationID":Ljava/lang/String;
    .end local v9    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 2137
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method protected _initCachedCustomDimensions()V
    .locals 4

    .prologue
    .line 1841
    monitor-enter p0

    .line 1843
    :try_start_0
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 1845
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    .line 1847
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x14

    if-ge v0, v1, :cond_1

    .line 1849
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->_getCustomDimension(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1847
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1851
    :cond_1
    monitor-exit p0

    .line 1852
    return-void

    .line 1851
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected _initCachedIdentifiers()V
    .locals 2

    .prologue
    .line 1829
    monitor-enter p0

    .line 1831
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1833
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    .line 1835
    :cond_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getIdentifiers()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1836
    monitor-exit p0

    .line 1837
    return-void

    .line 1836
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected _isOptedOut()Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1880
    const/4 v6, 0x0

    .line 1883
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "opt_out"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1885
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1887
    const-string v0, "opt_out"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v7

    .line 1892
    :goto_0
    if-eqz v6, :cond_0

    .line 1894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1895
    const/4 v6, 0x0

    .line 1899
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v8

    .line 1887
    goto :goto_0

    .line 1892
    :cond_2
    if-eqz v6, :cond_3

    .line 1894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1895
    const/4 v6, 0x0

    :cond_3
    move v0, v8

    .line 1899
    goto :goto_1

    .line 1892
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 1894
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1895
    const/4 v6, 0x0

    :cond_4
    throw v0
.end method

.method protected _onUploadCompleted(ZLjava/lang/String;)V
    .locals 1
    .param p1, "successful"    # Z
    .param p2, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 809
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    invoke-virtual {v0}, Lcom/localytics/android/BaseProvider;->vacuumIfNecessary()V

    .line 810
    return-void
.end method

.method protected _open()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1501
    iget-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    if-eqz v0, :cond_1

    .line 1503
    const-string v0, "Session was already open"

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 1544
    :cond_0
    :goto_0
    return-void

    .line 1507
    :cond_1
    const/4 v6, 0x0

    .line 1510
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "last_session_close_time"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1511
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1513
    const-string v0, "last_session_close_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1514
    .local v8, "sessionCloseTime":J
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v8

    sget-wide v2, Lcom/localytics/android/Constants;->SESSION_EXPIRATION:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    move v7, v10

    .line 1516
    .local v7, "newSession":Z
    :goto_1
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v0}, Lcom/localytics/android/ListenersSet;->getProxy()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/localytics/android/AnalyticsListener;

    iget-boolean v2, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    iget-boolean v3, p0, Lcom/localytics/android/AnalyticsHandler;->mAppWasUpgraded:Z

    if-nez v7, :cond_5

    move v1, v10

    :goto_2
    invoke-interface {v0, v2, v3, v1}, Lcom/localytics/android/AnalyticsListener;->localyticsSessionWillOpen(ZZZ)V

    .line 1518
    if-eqz v7, :cond_6

    const-string v0, "Opening new session"

    :goto_3
    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 1520
    invoke-direct {p0, v7}, Lcom/localytics/android/AnalyticsHandler;->_dequeQueuedCloseSessionTag(Z)V

    .line 1521
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/localytics/android/AnalyticsHandler;->mIsSessionOpen:Z

    .line 1523
    if-eqz v7, :cond_2

    .line 1525
    invoke-direct {p0}, Lcom/localytics/android/AnalyticsHandler;->_tagOpenEvent()V

    .line 1530
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/BaseProvider;->deleteOldFiles(Landroid/content/Context;)V

    .line 1533
    :cond_2
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v0}, Lcom/localytics/android/ListenersSet;->getProxy()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/localytics/android/AnalyticsListener;

    iget-boolean v2, p0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    iget-boolean v3, p0, Lcom/localytics/android/AnalyticsHandler;->mAppWasUpgraded:Z

    if-nez v7, :cond_7

    move v1, v10

    :goto_4
    invoke-interface {v0, v2, v3, v1}, Lcom/localytics/android/AnalyticsListener;->localyticsSessionDidOpen(ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1538
    .end local v7    # "newSession":Z
    .end local v8    # "sessionCloseTime":J
    :cond_3
    if-eqz v6, :cond_0

    .line 1540
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1541
    const/4 v6, 0x0

    goto :goto_0

    .restart local v8    # "sessionCloseTime":J
    :cond_4
    move v7, v11

    .line 1514
    goto :goto_1

    .restart local v7    # "newSession":Z
    :cond_5
    move v1, v11

    .line 1516
    goto :goto_2

    .line 1518
    :cond_6
    :try_start_1
    const-string v0, "Opening old closed session and reconnecting"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_7
    move v1, v11

    .line 1533
    goto :goto_4

    .line 1538
    .end local v7    # "newSession":Z
    .end local v8    # "sessionCloseTime":J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_8

    .line 1540
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1541
    const/4 v6, 0x0

    :cond_8
    throw v0
.end method

.method _replaceAttributionInFirstSession(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "referrerId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 2196
    const/4 v8, 0x0

    .line 2199
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v1, "info"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "first_open_event_blob"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/localytics/android/BaseProvider;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2201
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2203
    const-string v0, "first_open_event_blob"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2204
    .local v10, "firstEventBlob":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2209
    const-string v0, "[\n]"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2212
    .local v7, "blobs":[Ljava/lang/String;
    :try_start_1
    new-instance v12, Lorg/json/JSONObject;

    const/4 v0, 0x0

    aget-object v0, v7, v0

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2213
    .local v12, "headerBlob":Lorg/json/JSONObject;
    const-string v0, "attrs"

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/json/JSONObject;

    .line 2215
    .local v11, "headerAttributes":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v0}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/DatapointHelper;->getAdvertisingInfo(Landroid/content/Context;)Lcom/localytics/android/DatapointHelper$AdvertisingInfo;

    move-result-object v6

    .line 2216
    .local v6, "advertisingInfo":Lcom/localytics/android/DatapointHelper$AdvertisingInfo;
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v11, v6, v0}, Lcom/localytics/android/AnalyticsHandler;->_updateHeaderForTestModeAttribution(Ljava/lang/String;Lorg/json/JSONObject;Lcom/localytics/android/DatapointHelper$AdvertisingInfo;Z)V

    .line 2218
    const-string v0, "aurl"

    invoke-virtual {v11, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2219
    const-string v0, "%s\n%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-object v3, v7, v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2230
    if-eqz v8, :cond_0

    .line 2232
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2236
    .end local v6    # "advertisingInfo":Lcom/localytics/android/DatapointHelper$AdvertisingInfo;
    .end local v7    # "blobs":[Ljava/lang/String;
    .end local v10    # "firstEventBlob":Ljava/lang/String;
    .end local v11    # "headerAttributes":Lorg/json/JSONObject;
    .end local v12    # "headerBlob":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v0

    .line 2221
    .restart local v7    # "blobs":[Ljava/lang/String;
    .restart local v10    # "firstEventBlob":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 2223
    .local v9, "e":Lorg/json/JSONException;
    :try_start_2
    const-string v0, "JSONException"

    invoke-static {v0, v9}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2230
    .end local v7    # "blobs":[Ljava/lang/String;
    .end local v9    # "e":Lorg/json/JSONException;
    .end local v10    # "firstEventBlob":Ljava/lang/String;
    :cond_1
    if-eqz v8, :cond_2

    .line 2232
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v13

    .line 2236
    goto :goto_0

    .line 2230
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 2232
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected _setIdentifier(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1674
    if-eqz p2, :cond_0

    .line 1676
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 1679
    :cond_0
    const-string v2, "customer_id"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1681
    invoke-virtual {p0}, Lcom/localytics/android/AnalyticsHandler;->_getCustomerId()Ljava/lang/String;

    move-result-object v0

    .line 1684
    .local v0, "currentCustomerId":Ljava/lang/String;
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1737
    .end local v0    # "currentCustomerId":Ljava/lang/String;
    :goto_0
    return-void

    .line 1691
    .restart local v0    # "currentCustomerId":Ljava/lang/String;
    :cond_1
    const-string v2, "first_name"

    invoke-virtual {p0, v2, v4}, Lcom/localytics/android/AnalyticsHandler;->_setIdentifier(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    const-string v2, "last_name"

    invoke-virtual {p0, v2, v4}, Lcom/localytics/android/AnalyticsHandler;->_setIdentifier(Ljava/lang/String;Ljava/lang/String;)V

    .line 1693
    const-string v2, "full_name"

    invoke-virtual {p0, v2, v4}, Lcom/localytics/android/AnalyticsHandler;->_setIdentifier(Ljava/lang/String;Ljava/lang/String;)V

    .line 1694
    const-string v2, "email"

    invoke-virtual {p0, v2, v4}, Lcom/localytics/android/AnalyticsHandler;->_setIdentifier(Ljava/lang/String;Ljava/lang/String;)V

    .line 1696
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1697
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1699
    const-string v2, "customer_id"

    iget-object v3, p0, Lcom/localytics/android/AnalyticsHandler;->mInstallId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700
    const-string v2, "user_type"

    const-string v3, "anonymous"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    :goto_1
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "info"

    invoke-virtual {v2, v3, v1, v4, v4}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1710
    .end local v0    # "currentCustomerId":Ljava/lang/String;
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_2
    monitor-enter p0

    .line 1712
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1714
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "identifiers"

    const-string v4, "%s = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "key"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/localytics/android/BaseProvider;->remove(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_3

    .line 1716
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1736
    :cond_3
    :goto_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1704
    .restart local v0    # "currentCustomerId":Ljava/lang/String;
    .restart local v1    # "values":Landroid/content/ContentValues;
    :cond_4
    const-string v2, "customer_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    const-string v2, "user_type"

    const-string v3, "known"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1721
    .end local v0    # "currentCustomerId":Ljava/lang/String;
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_5
    :try_start_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1722
    .restart local v1    # "values":Landroid/content/ContentValues;
    const-string v2, "key"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1723
    const-string v2, "value"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1724
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "identifiers"

    sget-object v4, Lcom/localytics/android/AnalyticsHandler;->SELECTION_SET_IDENTIFIER:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/localytics/android/BaseProvider;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    .line 1726
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mProvider:Lcom/localytics/android/BaseProvider;

    const-string v3, "identifiers"

    invoke-virtual {v2, v3, v1}, Lcom/localytics/android/BaseProvider;->insert(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1728
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1733
    :cond_6
    iget-object v2, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method _updateHeaderForTestModeAttribution(Ljava/lang/String;Lorg/json/JSONObject;Lcom/localytics/android/DatapointHelper$AdvertisingInfo;Z)V
    .locals 16
    .param p1, "playAttribution"    # Ljava/lang/String;
    .param p2, "headerAttributes"    # Lorg/json/JSONObject;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "advertisingInfo"    # Lcom/localytics/android/DatapointHelper$AdvertisingInfo;
    .param p4, "ignoreFirstSession"    # Z

    .prologue
    .line 1334
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/localytics/android/AnalyticsHandler;->mSentReferrerTestMode:Z

    if-nez v13, :cond_6

    .line 1342
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 1344
    invoke-static/range {p1 .. p1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "[&]"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1345
    .local v4, "attributions":[Ljava/lang/String;
    move-object v2, v4

    .local v2, "arr$":[Ljava/lang/String;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v11, :cond_3

    aget-object v3, v2, v8

    .line 1347
    .local v3, "attribution":Ljava/lang/String;
    const-string v13, "[=]"

    invoke-virtual {v3, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 1348
    .local v10, "keyValue":[Ljava/lang/String;
    array-length v13, v10

    const/4 v14, 0x1

    if-le v13, v14, :cond_1

    .line 1350
    const/4 v13, 0x0

    aget-object v13, v10, v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    .local v9, "key":Ljava/lang/String;
    const/4 v13, 0x1

    aget-object v13, v10, v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    .line 1351
    .local v12, "value":Ljava/lang/String;
    const-string v13, "localytics_test_mode"

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const-string v13, "1"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "true"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    :cond_0
    const/4 v13, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/localytics/android/AnalyticsHandler;->mReferrerTestModeEnabled:Z

    .line 1345
    .end local v9    # "key":Ljava/lang/String;
    .end local v12    # "value":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1351
    .restart local v9    # "key":Ljava/lang/String;
    .restart local v12    # "value":Ljava/lang/String;
    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 1355
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "attribution":Ljava/lang/String;
    .end local v4    # "attributions":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v9    # "key":Ljava/lang/String;
    .end local v10    # "keyValue":[Ljava/lang/String;
    .end local v11    # "len$":I
    .end local v12    # "value":Ljava/lang/String;
    :cond_3
    if-nez p4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/localytics/android/AnalyticsHandler;->mFirstSessionEver:Z

    if-eqz v13, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/localytics/android/AnalyticsHandler;->mReferrerTestModeEnabled:Z

    if-eqz v13, :cond_6

    .line 1359
    :try_start_0
    const-string v13, "[REFERRAL] using fake id for attribution test mode"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->i(Ljava/lang/String;)I

    .line 1360
    new-instance v13, Ljava/security/SecureRandom;

    invoke-direct {v13}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v13}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v7

    .line 1361
    .local v7, "fakeDeviceID":Ljava/lang/String;
    const-string v13, "aid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1362
    const-string v13, "du"

    invoke-static {v7}, Lcom/localytics/android/DatapointHelper;->getSha256_buggy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1363
    const-string v13, "caid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1364
    if-eqz p3, :cond_5

    .line 1366
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1367
    .local v6, "fakeAdvertisingId":Ljava/lang/String;
    const-string v13, "gadid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1368
    const-string v13, "gcadid"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1370
    .end local v6    # "fakeAdvertisingId":Ljava/lang/String;
    :cond_5
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/localytics/android/AnalyticsHandler;->mSentReferrerTestMode:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1378
    .end local v7    # "fakeDeviceID":Ljava/lang/String;
    :cond_6
    :goto_2
    return-void

    .line 1372
    :catch_0
    move-exception v5

    .line 1374
    .local v5, "e":Lorg/json/JSONException;
    const-string v13, "Exception adding values to object"

    invoke-static {v13, v5}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method addListener(Lcom/localytics/android/AnalyticsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/localytics/android/AnalyticsListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 208
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v0, p1}, Lcom/localytics/android/ListenersSet;->add(Ljava/lang/Object;)V

    .line 209
    return-void
.end method

.method areNotificationsDisabled()Z
    .locals 1

    .prologue
    .line 178
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$1;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$1;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getBool(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method closeSession()V
    .locals 1

    .prologue
    .line 1798
    const/16 v0, 0x66

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1799
    return-void
.end method

.method getCachedCustomDimensions()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1816
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1817
    .local v0, "customDimensions":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    monitor-enter p0

    .line 1819
    :try_start_0
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 1821
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedCustomDimensions:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1823
    :cond_0
    monitor-exit p0

    .line 1824
    return-object v0

    .line 1823
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getCachedIdentifiers()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1803
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1804
    .local v0, "identifiers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    .line 1806
    :try_start_0
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 1808
    iget-object v1, p0, Lcom/localytics/android/AnalyticsHandler;->mCachedIdentifiers:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1810
    :cond_0
    monitor-exit p0

    .line 1811
    return-object v0

    .line 1810
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getCustomDimension(I)Ljava/lang/String;
    .locals 1
    .param p1, "dimension"    # I

    .prologue
    .line 936
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$14;

    invoke-direct {v0, p0, p1}, Lcom/localytics/android/AnalyticsHandler$14;-><init>(Lcom/localytics/android/AnalyticsHandler;I)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getString(Ljava/util/concurrent/Callable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCustomerIdFuture()Ljava/util/concurrent/FutureTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 998
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$15;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$15;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getFuture(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/FutureTask;

    move-result-object v0

    return-object v0
.end method

.method getIdentifier(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 1766
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$19;

    invoke-direct {v0, p0, p1}, Lcom/localytics/android/AnalyticsHandler$19;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getString(Ljava/util/concurrent/Callable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getInstallationId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1035
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$16;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$16;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getString(Ljava/util/concurrent/Callable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPushRegistrationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1047
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$17;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$17;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getString(Ljava/util/concurrent/Callable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPushSenderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1082
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$18;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$18;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getString(Ljava/util/concurrent/Callable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleMessageExtended(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 560
    iget v13, p1, Landroid/os/Message;->what:I

    packed-switch v13, :pswitch_data_0

    .line 787
    invoke-super {p0, p1}, Lcom/localytics/android/BaseHandler;->handleMessageExtended(Landroid/os/Message;)V

    .line 791
    :goto_0
    return-void

    .line 564
    :pswitch_0
    const-string v13, "Analytics handler received MESSAGE_OPEN"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 566
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$2;

    invoke-direct {v13, p0}, Lcom/localytics/android/AnalyticsHandler$2;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 584
    :pswitch_1
    const-string v13, "Analytics handler received MESSAGE_CLOSE"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 586
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$3;

    invoke-direct {v13, p0}, Lcom/localytics/android/AnalyticsHandler$3;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 604
    :pswitch_2
    const-string v13, "Analytics handler received MESSAGE_TAG_EVENT"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 606
    iget-object v13, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, [Ljava/lang/Object;

    move-object v8, v13

    check-cast v8, [Ljava/lang/Object;

    .line 607
    .local v8, "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v4, v8, v13

    check-cast v4, Ljava/lang/String;

    .line 609
    .local v4, "event":Ljava/lang/String;
    const/4 v13, 0x1

    aget-object v0, v8, v13

    check-cast v0, Ljava/util/Map;

    .line 610
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v13, 0x2

    aget-object v1, v8, v13

    check-cast v1, Ljava/lang/Long;

    .line 612
    .local v1, "clv":Ljava/lang/Long;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$4;

    invoke-direct {v13, p0, v4, v0, v1}, Lcom/localytics/android/AnalyticsHandler$4;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;Ljava/util/Map;Ljava/lang/Long;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 630
    .end local v0    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "clv":Ljava/lang/Long;
    .end local v4    # "event":Ljava/lang/String;
    .end local v8    # "params":[Ljava/lang/Object;
    :pswitch_3
    const-string v13, "Analytics handler received MESSAGE_TAG_SCREEN"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 632
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    .line 634
    .local v11, "screen":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$5;

    invoke-direct {v13, p0, v11}, Lcom/localytics/android/AnalyticsHandler$5;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 652
    .end local v11    # "screen":Ljava/lang/String;
    :pswitch_4
    const-string v13, "Analytics handler received MESSAGE_SET_CUSTOM_DIMENSION"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 654
    iget-object v13, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, [Ljava/lang/Object;

    move-object v8, v13

    check-cast v8, [Ljava/lang/Object;

    .line 655
    .restart local v8    # "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v13, v8, v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 656
    .local v2, "dimension":I
    const/4 v13, 0x1

    aget-object v12, v8, v13

    check-cast v12, Ljava/lang/String;

    .line 658
    .local v12, "value":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$6;

    invoke-direct {v13, p0, v2, v12}, Lcom/localytics/android/AnalyticsHandler$6;-><init>(Lcom/localytics/android/AnalyticsHandler;ILjava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 670
    .end local v2    # "dimension":I
    .end local v8    # "params":[Ljava/lang/Object;
    .end local v12    # "value":Ljava/lang/String;
    :pswitch_5
    const-string v13, "Analytics handler received MESSAGE_SET_IDENTIFIER"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 672
    iget-object v13, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, [Ljava/lang/Object;

    move-object v8, v13

    check-cast v8, [Ljava/lang/Object;

    .line 673
    .restart local v8    # "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v6, v8, v13

    check-cast v6, Ljava/lang/String;

    .line 674
    .local v6, "key":Ljava/lang/String;
    const/4 v13, 0x1

    aget-object v12, v8, v13

    check-cast v12, Ljava/lang/String;

    .line 676
    .restart local v12    # "value":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$7;

    invoke-direct {v13, p0, v6, v12}, Lcom/localytics/android/AnalyticsHandler$7;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 688
    .end local v6    # "key":Ljava/lang/String;
    .end local v8    # "params":[Ljava/lang/Object;
    .end local v12    # "value":Ljava/lang/String;
    :pswitch_6
    const-string v13, "Analytics handler received MESSAGE_SET_LOCATION"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 690
    iget-object v13, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, Landroid/location/Location;

    sput-object v13, Lcom/localytics/android/AnalyticsHandler;->sLastLocation:Landroid/location/Location;

    goto/16 :goto_0

    .line 695
    :pswitch_7
    const-string v13, "Analytics handler received MESSAGE_SET_PUSH_REGID"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 697
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    .line 699
    .local v9, "pushRegId":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$8;

    invoke-direct {v13, p0, v9}, Lcom/localytics/android/AnalyticsHandler$8;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 711
    .end local v9    # "pushRegId":Ljava/lang/String;
    :pswitch_8
    const-string v13, "Analytics handler received MESSAGE_DISABLE_NOTIFICATIONS"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 713
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 715
    .local v3, "disabled":I
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$9;

    invoke-direct {v13, p0, v3}, Lcom/localytics/android/AnalyticsHandler$9;-><init>(Lcom/localytics/android/AnalyticsHandler;I)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 727
    .end local v3    # "disabled":I
    :pswitch_9
    const-string v13, "Analytics handler received MESSAGE_OPT_OUT"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 729
    iget v13, p1, Landroid/os/Message;->arg1:I

    if-eqz v13, :cond_0

    const/4 v5, 0x1

    .line 731
    .local v5, "isOptingOut":Z
    :goto_1
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$10;

    invoke-direct {v13, p0, v5}, Lcom/localytics/android/AnalyticsHandler$10;-><init>(Lcom/localytics/android/AnalyticsHandler;Z)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 729
    .end local v5    # "isOptingOut":Z
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 743
    :pswitch_a
    const-string v13, "Analytics handler received MESSAGE_REGISTER_PUSH"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 745
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    .line 747
    .local v7, "newSenderId":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$11;

    invoke-direct {v13, p0, v7}, Lcom/localytics/android/AnalyticsHandler$11;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 759
    .end local v7    # "newSenderId":Ljava/lang/String;
    :pswitch_b
    const-string v13, "Analytics handler received MESSAGE_SET_REFERRERID"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 761
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    .line 763
    .local v10, "referrerId":Ljava/lang/String;
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$12;

    invoke-direct {v13, p0, v10}, Lcom/localytics/android/AnalyticsHandler$12;-><init>(Lcom/localytics/android/AnalyticsHandler;Ljava/lang/String;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 774
    .end local v10    # "referrerId":Ljava/lang/String;
    :pswitch_c
    const-string v13, "Analytics handler received MESSAGE_RETRIEVE_TOKEN_FROM_INSTANCEID"

    invoke-static {v13}, Lcom/localytics/android/Localytics$Log;->d(Ljava/lang/String;)I

    .line 776
    new-instance v13, Lcom/localytics/android/AnalyticsHandler$13;

    invoke-direct {v13, p0}, Lcom/localytics/android/AnalyticsHandler$13;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v13}, Lcom/localytics/android/AnalyticsHandler;->_runBatchTransactionOnProvider(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 560
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_7
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method isOptedOut()Z
    .locals 1

    .prologue
    .line 1947
    new-instance v0, Lcom/localytics/android/AnalyticsHandler$20;

    invoke-direct {v0, p0}, Lcom/localytics/android/AnalyticsHandler$20;-><init>(Lcom/localytics/android/AnalyticsHandler;)V

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->getBool(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public localyticsDidTriggerRegions(Ljava/util/List;Lcom/localytics/android/Region$Event;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "event"    # Lcom/localytics/android/Region$Event;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/Region;",
            ">;",
            "Lcom/localytics/android/Region$Event;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2248
    .local p1, "regions":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/Region;>;"
    return-void
.end method

.method public localyticsDidUpdateLocation(Landroid/location/Location;)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2242
    invoke-virtual {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->setLocation(Landroid/location/Location;)V

    .line 2243
    return-void
.end method

.method public localyticsDidUpdateMonitoredGeofences(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/CircularRegion;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/localytics/android/CircularRegion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2253
    .local p1, "added":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/CircularRegion;>;"
    .local p2, "removed":Ljava/util/List;, "Ljava/util/List<Lcom/localytics/android/CircularRegion;>;"
    return-void
.end method

.method openSession()V
    .locals 1

    .prologue
    .line 1741
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1742
    return-void
.end method

.method registerPush(Ljava/lang/String;J)V
    .locals 2
    .param p1, "senderId"    # Ljava/lang/String;
    .param p2, "delay"    # J

    .prologue
    .line 2002
    const/16 v0, 0x6d

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/localytics/android/AnalyticsHandler;->queueMessageDelayed(Landroid/os/Message;J)Z

    .line 2003
    return-void
.end method

.method retrieveTokenFromInstanceId()V
    .locals 1

    .prologue
    .line 2007
    const/16 v0, 0x71

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 2008
    return-void
.end method

.method setCustomDimension(ILjava/lang/String;)V
    .locals 4
    .param p1, "dimension"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1746
    if-ltz p1, :cond_0

    const/16 v0, 0x14

    if-lt p1, v0, :cond_1

    .line 1748
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only valid dimensions are 0 - 19"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1751
    :cond_1
    const/16 v0, 0x6b

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1752
    return-void
.end method

.method setDeveloperListener(Lcom/localytics/android/AnalyticsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/localytics/android/AnalyticsListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 203
    iget-object v0, p0, Lcom/localytics/android/AnalyticsHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v0, p1}, Lcom/localytics/android/ListenersSet;->setDevListener(Ljava/lang/Object;)V

    .line 204
    return-void
.end method

.method setIdentifier(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1756
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1761
    :cond_0
    const/16 v0, 0x69

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1762
    return-void
.end method

.method setLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 1875
    const/16 v0, 0x6a

    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-virtual {p0, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1876
    return-void
.end method

.method setNotificationsDisabled(Z)V
    .locals 3
    .param p1, "disabled"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1959
    const/16 v2, 0x6e

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1960
    return-void

    :cond_0
    move v0, v1

    .line 1959
    goto :goto_0
.end method

.method setOptedOut(Z)V
    .locals 5
    .param p1, "isOptingOut"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1940
    const-string v2, "Requested opt-out state is %b"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 1942
    const/16 v2, 0x6c

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1943
    return-void

    :cond_0
    move v0, v1

    .line 1942
    goto :goto_0
.end method

.method setPushRegistrationId(Ljava/lang/String;)V
    .locals 1
    .param p1, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 1077
    const/16 v0, 0x6f

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1078
    return-void
.end method

.method setReferrerId(Ljava/lang/String;)V
    .locals 1
    .param p1, "referrerId"    # Ljava/lang/String;

    .prologue
    .line 2148
    const/16 v0, 0x70

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 2149
    return-void
.end method

.method public tagAddedToCart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 4
    .param p1, "itemName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "itemType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "itemPrice"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p5, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 319
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p5, :cond_0

    .line 321
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 324
    :cond_0
    if-eqz p1, :cond_1

    .line 326
    const-string v1, "Item Name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    :cond_1
    if-eqz p2, :cond_2

    .line 330
    const-string v1, "Item ID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    :cond_2
    if-eqz p3, :cond_3

    .line 334
    const-string v1, "Item Type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    :cond_3
    if-eqz p4, :cond_4

    .line 338
    const-string v1, "Item Price"

    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    :cond_4
    const-string v1, "Localytics Added To Cart"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 342
    return-void
.end method

.method public tagCompletedCheckout(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 5
    .param p1, "totalPrice"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "itemCount"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p3, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 366
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p3, :cond_0

    .line 368
    invoke-interface {v0, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 370
    :cond_0
    if-eqz p1, :cond_1

    .line 372
    const-string v1, "Total Price"

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    :cond_1
    if-eqz p2, :cond_2

    .line 376
    const-string v1, "Item Count"

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    :cond_2
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 380
    .local v2, "clv":J
    :goto_0
    sget-object v1, Lcom/localytics/android/Constants;->IGNORE_STANDARD_EVENT_CLV:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 382
    const-wide/16 v2, 0x0

    .line 385
    :cond_3
    const-string v1, "Localytics Completed Checkout"

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 386
    return-void

    .line 379
    .end local v2    # "clv":J
    :cond_4
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public tagContentRated(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 4
    .param p1, "contentName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "contentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "contentType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "rating"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 464
    .local p5, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 465
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p5, :cond_0

    .line 467
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 470
    :cond_0
    if-eqz p1, :cond_1

    .line 472
    const-string v1, "Content Name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    :cond_1
    if-eqz p2, :cond_2

    .line 476
    const-string v1, "Content ID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    :cond_2
    if-eqz p3, :cond_3

    .line 480
    const-string v1, "Content Type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    :cond_3
    if-eqz p4, :cond_4

    .line 484
    const-string v1, "Content Rating"

    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    :cond_4
    const-string v1, "Localytics Content Rated"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 488
    return-void
.end method

.method public tagContentViewed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1, "contentName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "contentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "contentType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390
    .local p4, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 391
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p4, :cond_0

    .line 393
    invoke-interface {v0, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 396
    :cond_0
    if-eqz p1, :cond_1

    .line 398
    const-string v1, "Content Name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    :cond_1
    if-eqz p2, :cond_2

    .line 402
    const-string v1, "Content ID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    :cond_2
    if-eqz p3, :cond_3

    .line 406
    const-string v1, "Content Type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    :cond_3
    const-string v1, "Localytics Content Viewed"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 410
    return-void
.end method

.method tagEvent(Ljava/lang/String;Ljava/util/Map;J)V
    .locals 11
    .param p1, "eventName"    # Ljava/lang/String;
    .param p3, "customerValueIncrease"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v10, 0x32

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 242
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 244
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "event cannot be null or empty"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 247
    :cond_0
    if-eqz p2, :cond_5

    .line 253
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 255
    const-string v4, "attributes is empty.  Did the caller make an error?"

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 258
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v4

    if-le v4, v10, :cond_2

    .line 260
    const-string v4, "attributes size is %d, exceeding the maximum size of %d.  Did the caller make an error?"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 263
    :cond_2
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 265
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 266
    .local v2, "key":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 268
    .local v3, "value":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 270
    const-string v4, "attributes cannot contain null or empty keys"

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 272
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 274
    const-string v4, "attributes cannot contain null or empty values"

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    goto :goto_0

    .line 279
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_5
    const/16 v4, 0x67

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v7

    aput-object p2, v5, v8

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {p0, v4, v5}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 280
    return-void
.end method

.method public tagInvited(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .param p1, "methodName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 531
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 532
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 534
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 537
    :cond_0
    if-eqz p1, :cond_1

    .line 539
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->sanitizeMethodString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 540
    .local v1, "sanitizedMethodString":Ljava/lang/String;
    const-string v2, "Method Name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    .end local v1    # "sanitizedMethodString":Ljava/lang/String;
    :cond_1
    const-string v2, "Localytics Invited"

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v0, v4, v5}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 544
    return-void
.end method

.method public tagLoggedIn(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .param p1, "methodName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 509
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 510
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 512
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 515
    :cond_0
    if-eqz p1, :cond_1

    .line 517
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->sanitizeMethodString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 518
    .local v1, "sanitizedMethodString":Ljava/lang/String;
    const-string v2, "Method Name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    .end local v1    # "sanitizedMethodString":Ljava/lang/String;
    :cond_1
    const-string v2, "Localytics Logged In"

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v0, v4, v5}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 522
    return-void
.end method

.method public tagLoggedOut(Ljava/util/Map;)V
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 526
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "Localytics Logged Out"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, p1, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 527
    return-void
.end method

.method public tagPurchased(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 5
    .param p1, "itemName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "itemType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "itemPrice"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p5, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 285
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p5, :cond_0

    .line 287
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 290
    :cond_0
    if-eqz p1, :cond_1

    .line 292
    const-string v1, "Item Name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_1
    if-eqz p2, :cond_2

    .line 296
    const-string v1, "Item ID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    :cond_2
    if-eqz p3, :cond_3

    .line 300
    const-string v1, "Item Type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    :cond_3
    if-eqz p4, :cond_4

    .line 304
    const-string v1, "Item Price"

    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    :cond_4
    if-eqz p4, :cond_6

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 308
    .local v2, "clv":J
    :goto_0
    sget-object v1, Lcom/localytics/android/Constants;->IGNORE_STANDARD_EVENT_CLV:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 310
    const-wide/16 v2, 0x0

    .line 313
    :cond_5
    const-string v1, "Localytics Purchased"

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 314
    return-void

    .line 307
    .end local v2    # "clv":J
    :cond_6
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public tagRegistered(Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .param p1, "methodName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 493
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 495
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 498
    :cond_0
    if-eqz p1, :cond_1

    .line 500
    invoke-direct {p0, p1}, Lcom/localytics/android/AnalyticsHandler;->sanitizeMethodString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "sanitizedMethodString":Ljava/lang/String;
    const-string v2, "Method Name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    .end local v1    # "sanitizedMethodString":Ljava/lang/String;
    :cond_1
    const-string v2, "Localytics Registered"

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v0, v4, v5}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 505
    return-void
.end method

.method tagScreen(Ljava/lang/String;)V
    .locals 2
    .param p1, "screen"    # Ljava/lang/String;

    .prologue
    .line 1865
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1867
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "event cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1870
    :cond_0
    const/16 v0, 0x68

    invoke-virtual {p0, v0, p1}, Lcom/localytics/android/AnalyticsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/localytics/android/AnalyticsHandler;->queueMessage(Landroid/os/Message;)Z

    .line 1871
    return-void
.end method

.method public tagSearched(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 4
    .param p1, "queryText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "contentType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "resultCount"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 414
    .local p4, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 415
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p4, :cond_0

    .line 417
    invoke-interface {v0, p4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 420
    :cond_0
    if-eqz p1, :cond_1

    .line 422
    const-string v1, "Search Query"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    :cond_1
    if-eqz p2, :cond_2

    .line 426
    const-string v1, "Content Type"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    :cond_2
    if-eqz p3, :cond_3

    .line 430
    const-string v1, "Search Result Count"

    invoke-virtual {p3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    :cond_3
    const-string v1, "Localytics Searched"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 433
    return-void
.end method

.method public tagShared(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1, "contentName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "contentId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "contentType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "methodName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 437
    .local p5, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 438
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p5, :cond_0

    .line 440
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 443
    :cond_0
    if-eqz p1, :cond_1

    .line 445
    const-string v1, "Content Name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    :cond_1
    if-eqz p2, :cond_2

    .line 449
    const-string v1, "Content ID"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    :cond_2
    if-eqz p3, :cond_3

    .line 453
    const-string v1, "Content Type"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    :cond_3
    if-eqz p4, :cond_4

    .line 457
    const-string v1, "Method Name"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    :cond_4
    const-string v1, "Localytics Shared"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 460
    return-void
.end method

.method public tagStartedCheckout(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 4
    .param p1, "totalPrice"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "itemCount"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 346
    .local p3, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 347
    .local v0, "attributesCopy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p3, :cond_0

    .line 349
    invoke-interface {v0, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 351
    :cond_0
    if-eqz p1, :cond_1

    .line 353
    const-string v1, "Total Price"

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_1
    if-eqz p2, :cond_2

    .line 357
    const-string v1, "Item Count"

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_2
    const-string v1, "Localytics Started Checkout"

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/localytics/android/AnalyticsHandler;->tagEvent(Ljava/lang/String;Ljava/util/Map;J)V

    .line 361
    return-void
.end method
