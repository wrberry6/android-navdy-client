.class Lcom/google/maps/internal/OkHttpPendingResult$1;
.super Ljava/lang/Object;
.source "OkHttpPendingResult.java"

# interfaces
.implements Lcom/squareup/okhttp/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/maps/internal/OkHttpPendingResult;->await()Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/maps/internal/OkHttpPendingResult;

.field final synthetic val$parent:Lcom/google/maps/internal/OkHttpPendingResult;

.field final synthetic val$waiter:Ljava/util/concurrent/BlockingQueue;


# direct methods
.method constructor <init>(Lcom/google/maps/internal/OkHttpPendingResult;Ljava/util/concurrent/BlockingQueue;Lcom/google/maps/internal/OkHttpPendingResult;)V
    .locals 0

    .prologue
    .line 138
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult$1;, "Lcom/google/maps/internal/OkHttpPendingResult.1;"
    iput-object p1, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->this$0:Lcom/google/maps/internal/OkHttpPendingResult;

    iput-object p2, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$waiter:Ljava/util/concurrent/BlockingQueue;

    iput-object p3, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$parent:Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/squareup/okhttp/Request;Ljava/io/IOException;)V
    .locals 4
    .param p1, "request"    # Lcom/squareup/okhttp/Request;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 141
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult$1;, "Lcom/google/maps/internal/OkHttpPendingResult.1;"
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$waiter:Ljava/util/concurrent/BlockingQueue;

    new-instance v1, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    iget-object v2, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->this$0:Lcom/google/maps/internal/OkHttpPendingResult;

    iget-object v3, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$parent:Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-direct {v1, v2, v3, p2}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;-><init>(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/google/maps/internal/OkHttpPendingResult;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public onResponse(Lcom/squareup/okhttp/Response;)V
    .locals 4
    .param p1, "response"    # Lcom/squareup/okhttp/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult$1;, "Lcom/google/maps/internal/OkHttpPendingResult.1;"
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$waiter:Ljava/util/concurrent/BlockingQueue;

    new-instance v1, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    iget-object v2, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->this$0:Lcom/google/maps/internal/OkHttpPendingResult;

    iget-object v3, p0, Lcom/google/maps/internal/OkHttpPendingResult$1;->val$parent:Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;-><init>(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/google/maps/internal/OkHttpPendingResult;Lcom/squareup/okhttp/Response;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method
