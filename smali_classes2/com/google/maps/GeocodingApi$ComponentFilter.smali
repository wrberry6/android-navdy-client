.class public Lcom/google/maps/GeocodingApi$ComponentFilter;
.super Ljava/lang/Object;
.source "GeocodingApi.java"

# interfaces
.implements Lcom/google/maps/internal/StringJoin$UrlValue;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/maps/GeocodingApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFilter"
.end annotation


# instance fields
.field private final component:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "component"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/google/maps/GeocodingApi$ComponentFilter;->component:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/google/maps/GeocodingApi$ComponentFilter;->value:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public static administrativeArea(Ljava/lang/String;)Lcom/google/maps/GeocodingApi$ComponentFilter;
    .locals 2
    .param p0, "administrativeArea"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v0, Lcom/google/maps/GeocodingApi$ComponentFilter;

    const-string v1, "administrative_area"

    invoke-direct {v0, v1, p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static country(Ljava/lang/String;)Lcom/google/maps/GeocodingApi$ComponentFilter;
    .locals 2
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 148
    new-instance v0, Lcom/google/maps/GeocodingApi$ComponentFilter;

    const-string v1, "country"

    invoke-direct {v0, v1, p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static locality(Ljava/lang/String;)Lcom/google/maps/GeocodingApi$ComponentFilter;
    .locals 2
    .param p0, "locality"    # Ljava/lang/String;

    .prologue
    .line 127
    new-instance v0, Lcom/google/maps/GeocodingApi$ComponentFilter;

    const-string v1, "locality"

    invoke-direct {v0, v1, p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static postalCode(Ljava/lang/String;)Lcom/google/maps/GeocodingApi$ComponentFilter;
    .locals 2
    .param p0, "postalCode"    # Ljava/lang/String;

    .prologue
    .line 141
    new-instance v0, Lcom/google/maps/GeocodingApi$ComponentFilter;

    const-string v1, "postal_code"

    invoke-direct {v0, v1, p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static route(Ljava/lang/String;)Lcom/google/maps/GeocodingApi$ComponentFilter;
    .locals 2
    .param p0, "route"    # Ljava/lang/String;

    .prologue
    .line 120
    new-instance v0, Lcom/google/maps/GeocodingApi$ComponentFilter;

    const-string v1, "route"

    invoke-direct {v0, v1, p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/maps/GeocodingApi$ComponentFilter;->toUrlValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUrlValue()Ljava/lang/String;
    .locals 4

    .prologue
    .line 113
    const/16 v0, 0x3a

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/maps/GeocodingApi$ComponentFilter;->component:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/maps/GeocodingApi$ComponentFilter;->value:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/maps/internal/StringJoin;->join(C[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
