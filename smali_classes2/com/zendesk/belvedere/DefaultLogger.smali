.class Lcom/zendesk/belvedere/DefaultLogger;
.super Ljava/lang/Object;
.source "DefaultLogger.java"

# interfaces
.implements Lcom/zendesk/belvedere/BelvedereLogger;


# instance fields
.field private loggable:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    if-eqz v0, :cond_0

    .line 19
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    if-eqz v0, :cond_0

    .line 33
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    if-eqz v0, :cond_0

    .line 40
    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 42
    :cond_0
    return-void
.end method

.method public setLoggable(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    .line 47
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/zendesk/belvedere/DefaultLogger;->loggable:Z

    if-eqz v0, :cond_0

    .line 26
    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :cond_0
    return-void
.end method
