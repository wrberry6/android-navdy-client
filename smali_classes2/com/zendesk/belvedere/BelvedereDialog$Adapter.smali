.class Lcom/zendesk/belvedere/BelvedereDialog$Adapter;
.super Landroid/widget/ArrayAdapter;
.source "BelvedereDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/belvedere/BelvedereDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/zendesk/belvedere/BelvedereIntent;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/zendesk/belvedere/BelvedereDialog;


# direct methods
.method constructor <init>(Lcom/zendesk/belvedere/BelvedereDialog;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->this$0:Lcom/zendesk/belvedere/BelvedereDialog;

    .line 132
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 133
    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->context:Landroid/content/Context;

    .line 134
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 138
    move-object v2, p2

    .line 139
    .local v2, "row":Landroid/view/View;
    if-nez p2, :cond_0

    .line 140
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/zendesk/belvedere/R$layout;->belvedere_dialog_row:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 143
    :cond_0
    invoke-virtual {p0, p1}, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/belvedere/BelvedereIntent;

    .line 144
    .local v0, "intent":Lcom/zendesk/belvedere/BelvedereIntent;
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->context:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->from(Lcom/zendesk/belvedere/BelvedereIntent;Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;

    move-result-object v1

    .line 145
    .local v1, "item":Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;
    sget v3, Lcom/zendesk/belvedere/R$id;->belvedere_dialog_row_image:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->getDrawable()I

    move-result v5

    invoke-static {v4, v5}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    sget v3, Lcom/zendesk/belvedere/R$id;->belvedere_dialog_row_text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 149
    return-object v2
.end method
