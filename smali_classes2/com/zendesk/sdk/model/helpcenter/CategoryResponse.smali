.class public Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;
.super Ljava/lang/Object;
.source "CategoryResponse.java"


# instance fields
.field private category:Lcom/zendesk/sdk/model/helpcenter/Category;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategory()Lcom/zendesk/sdk/model/helpcenter/Category;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;->category:Lcom/zendesk/sdk/model/helpcenter/Category;

    return-object v0
.end method
