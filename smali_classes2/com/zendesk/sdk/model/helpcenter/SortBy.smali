.class public final enum Lcom/zendesk/sdk/model/helpcenter/SortBy;
.super Ljava/lang/Enum;
.source "SortBy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/SortBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortBy;

.field public static final enum CREATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

.field public static final enum POSITION:Lcom/zendesk/sdk/model/helpcenter/SortBy;

.field public static final enum TITLE:Lcom/zendesk/sdk/model/helpcenter/SortBy;

.field public static final enum UPDATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    const-string v1, "POSITION"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/helpcenter/SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->POSITION:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/model/helpcenter/SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->TITLE:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    const-string v1, "CREATED_AT"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/model/helpcenter/SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->CREATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    const-string v1, "UPDATED_AT"

    invoke-direct {v0, v1, v5}, Lcom/zendesk/sdk/model/helpcenter/SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->UPDATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/zendesk/sdk/model/helpcenter/SortBy;

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortBy;->POSITION:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortBy;->TITLE:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortBy;->CREATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/SortBy;->UPDATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    aput-object v1, v0, v5

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SortBy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/model/helpcenter/SortBy;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/zendesk/sdk/model/helpcenter/SortBy;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/SortBy;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/model/helpcenter/SortBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/model/helpcenter/SortBy;

    return-object v0
.end method


# virtual methods
.method public getApiValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/helpcenter/SortBy;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
