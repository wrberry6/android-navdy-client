.class public abstract Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "NetworkAwareActionbarActivity.java"

# interfaces
.implements Lcom/zendesk/sdk/network/NetworkAware;
.implements Lcom/zendesk/sdk/network/Retryable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$NetworkAvailabilityBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field protected mNetworkAvailable:Z

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mNetworkPreviouslyUnavailable:Z

.field private mNoNetworkView:Landroid/view/View;

.field private mRetryView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findNoNetworkView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private findNoNetworkView()Landroid/view/View;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNoNetworkView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 273
    sget v0, Lcom/zendesk/sdk/R$id;->activity_network_no_connectivity:I

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNoNetworkView:Landroid/view/View;

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNoNetworkView:Landroid/view/View;

    return-object v0
.end method

.method private findRetryView()Landroid/view/View;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mRetryView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 266
    sget v0, Lcom/zendesk/sdk/R$id;->retry_view_container:I

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mRetryView:Landroid/view/View;

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mRetryView:Landroid/view/View;

    return-object v0
.end method

.method private registerForNetworkCallbacks()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 148
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 150
    sget-object v2, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Adding pre-Lollipop network callbacks..."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    new-instance v2, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$NetworkAvailabilityBroadcastReceiver;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$NetworkAvailabilityBroadcastReceiver;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)V

    iput-object v2, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 153
    iget-object v2, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 188
    :goto_0
    return-void

    .line 156
    :cond_0
    sget-object v2, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Adding Lollipop network callbacks..."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    const-string v2, "connectivity"

    .line 159
    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 161
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 163
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;

    invoke-direct {v2, p0, v1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 185
    new-instance v2, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v2}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 186
    invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 185
    invoke-virtual {v0, v2, v3}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    goto :goto_0
.end method

.method private setRetryViewState(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p1, "shouldShow"    # Z
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "retryAction"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v7, 0x0

    .line 206
    iget-boolean v4, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkAvailable:Z

    if-nez v4, :cond_1

    .line 207
    sget-object v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Network is not available, will not alter retry view state"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_2

    .line 212
    sget-object v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "No retry view was found, will not alter view state"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 216
    :cond_2
    if-eqz p1, :cond_6

    .line 218
    sget v4, Lcom/zendesk/sdk/R$id;->retry_view_button:I

    invoke-virtual {p0, v4}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 219
    .local v0, "retryButton":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 220
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    :cond_3
    sget v4, Lcom/zendesk/sdk/R$id;->retry_view_text:I

    invoke-virtual {p0, v4}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 224
    .local v1, "retryText":Landroid/widget/TextView;
    if-eqz v1, :cond_4

    .line 225
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_4
    invoke-static {p2}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 229
    sget-object v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Slide in requested but we have no message or action to perform"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 233
    :cond_5
    sget-object v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Sliding in retry view..."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    sget v4, Lcom/zendesk/sdk/R$anim;->info_slide_in:I

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 236
    .local v2, "slideIn":Landroid/view/animation/Animation;
    new-instance v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$4;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$4;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)V

    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 243
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 247
    .end local v0    # "retryButton":Landroid/view/View;
    .end local v1    # "retryText":Landroid/widget/TextView;
    .end local v2    # "slideIn":Landroid/view/animation/Animation;
    :cond_6
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 249
    sget-object v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Sliding out retry view..."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    sget v4, Lcom/zendesk/sdk/R$anim;->info_slide_out:I

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 252
    .local v3, "slideOut":Landroid/view/animation/Animation;
    new-instance v4, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$5;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$5;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 259
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private unregisterForNetworkCallbacks()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 192
    iget-object v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 196
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v1, :cond_1

    .line 197
    const-string v1, "connectivity"

    .line 198
    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 200
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    iget-object v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 202
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-static {p0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkAvailable:Z

    .line 121
    return-void
.end method

.method public onNetworkAvailable()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 60
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Network is available."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkAvailable:Z

    .line 63
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findNoNetworkView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 64
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "The no network view is null, nothing to do."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-boolean v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkPreviouslyUnavailable:Z

    if-nez v1, :cond_1

    .line 69
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Network was not previously unavailable, no need to perform animation"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :cond_1
    iput-boolean v4, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkPreviouslyUnavailable:Z

    .line 75
    sget v1, Lcom/zendesk/sdk/R$anim;->info_slide_out:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 76
    .local v0, "slideOut":Landroid/view/animation/Animation;
    new-instance v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$1;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$1;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 83
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findNoNetworkView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onNetworkUnavailable()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 92
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Network is unavailable."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    iput-boolean v4, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkAvailable:Z

    .line 94
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->mNetworkPreviouslyUnavailable:Z

    .line 96
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 97
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "No network: ensuring that the retry view is gone."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findRetryView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 101
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findNoNetworkView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 102
    sget-object v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->LOG_TAG:Ljava/lang/String;

    const-string v2, "No network: cannot show view because it is missing."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :goto_0
    return-void

    .line 106
    :cond_1
    sget v1, Lcom/zendesk/sdk/R$anim;->info_slide_in:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 107
    .local v0, "slideIn":Landroid/view/animation/Animation;
    new-instance v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$2;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 114
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->findNoNetworkView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 132
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->unregisterForNetworkCallbacks()V

    .line 133
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 126
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->registerForNetworkCallbacks()V

    .line 127
    return-void
.end method

.method public onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "retryAction"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 137
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->setRetryViewState(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 138
    return-void
.end method

.method public onRetryUnavailable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->setRetryViewState(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 143
    return-void
.end method
