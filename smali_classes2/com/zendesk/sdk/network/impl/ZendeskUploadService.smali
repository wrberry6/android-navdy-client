.class Lcom/zendesk/sdk/network/impl/ZendeskUploadService;
.super Ljava/lang/Object;
.source "ZendeskUploadService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskUploadService"


# instance fields
.field private final uploadService:Lcom/zendesk/sdk/network/UploadService;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/network/UploadService;)V
    .locals 0
    .param p1, "uploadService"    # Lcom/zendesk/sdk/network/UploadService;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;->uploadService:Lcom/zendesk/sdk/network/UploadService;

    .line 28
    return-void
.end method


# virtual methods
.method deleteAttachment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;->uploadService:Lcom/zendesk/sdk/network/UploadService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/UploadService;->deleteAttachment(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 54
    return-void
.end method

.method uploadAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UploadResponseWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UploadResponseWrapper;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;->uploadService:Lcom/zendesk/sdk/network/UploadService;

    invoke-static {p4}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v1

    invoke-static {v1, p3}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/zendesk/sdk/network/UploadService;->uploadAttachment(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p5}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 43
    return-void
.end method
