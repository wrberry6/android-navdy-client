.class public Lcom/zendesk/sdk/network/impl/RestAdapterModule;
.super Ljava/lang/Object;
.source "RestAdapterModule.java"


# instance fields
.field private final retrofit:Lretrofit2/Retrofit;


# direct methods
.method public constructor <init>(Lretrofit2/Retrofit;)V
    .locals 0
    .param p1, "retrofit"    # Lretrofit2/Retrofit;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/RestAdapterModule;->retrofit:Lretrofit2/Retrofit;

    .line 15
    return-void
.end method


# virtual methods
.method public getRetrofit()Lretrofit2/Retrofit;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/RestAdapterModule;->retrofit:Lretrofit2/Retrofit;

    return-object v0
.end method
