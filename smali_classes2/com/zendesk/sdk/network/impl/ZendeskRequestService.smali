.class Lcom/zendesk/sdk/network/impl/ZendeskRequestService;
.super Ljava/lang/Object;
.source "ZendeskRequestService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskRequestService"


# instance fields
.field private final requestService:Lcom/zendesk/sdk/network/RequestService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/RequestService;)V
    .locals 0
    .param p1, "requestService"    # Lcom/zendesk/sdk/network/RequestService;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    .line 35
    return-void
.end method


# virtual methods
.method addComment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 5
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "requestId"    # Ljava/lang/String;
    .param p3, "comment"    # Lcom/zendesk/sdk/model/request/EndUserComment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/EndUserComment;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Request;>;"
    new-instance v1, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;-><init>()V

    .line 118
    .local v1, "updateRequestWrapper":Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;
    new-instance v0, Lcom/zendesk/sdk/model/request/Request;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/Request;-><init>()V

    .line 119
    .local v0, "request":Lcom/zendesk/sdk/model/request/Request;
    invoke-virtual {v0, p3}, Lcom/zendesk/sdk/model/request/Request;->setComment(Lcom/zendesk/sdk/model/request/EndUserComment;)V

    .line 120
    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;->setRequest(Lcom/zendesk/sdk/model/request/Request;)V

    .line 121
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v2, p1, p2, v1}, Lcom/zendesk/sdk/network/RequestService;->addComment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;)Lretrofit2/Call;

    move-result-object v2

    new-instance v3, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v4, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$4;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestService;)V

    invoke-direct {v3, p4, v4}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v2, v3}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 127
    return-void
.end method

.method public createRequest(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "sdkGuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Request;>;"
    new-instance v0, Lcom/zendesk/sdk/model/request/CreateRequestWrapper;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/CreateRequestWrapper;-><init>()V

    .line 52
    .local v0, "wrapper":Lcom/zendesk/sdk/model/request/CreateRequestWrapper;
    invoke-virtual {v0, p3}, Lcom/zendesk/sdk/model/request/CreateRequestWrapper;->setRequest(Lcom/zendesk/sdk/model/request/CreateRequest;)V

    .line 54
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v1, p1, p2, v0}, Lcom/zendesk/sdk/network/RequestService;->createRequest(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequestWrapper;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$1;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestService;)V

    invoke-direct {v2, p4, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 60
    return-void
.end method

.method getAllRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "include"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v0, p1, p2, p3}, Lcom/zendesk/sdk/network/RequestService;->getAllRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$2;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestService;)V

    invoke-direct {v1, p4, v2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 78
    return-void
.end method

.method getAllRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "requestId"    # Ljava/lang/String;
    .param p3, "status"    # Ljava/lang/String;
    .param p4, "include"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/zendesk/sdk/network/RequestService;->getManyRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$3;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestService;)V

    invoke-direct {v1, p5, v2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 95
    return-void
.end method

.method getComments(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CommentsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CommentsResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/RequestService;->getComments(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 106
    return-void
.end method

.method getRequest(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/RequestResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/RequestResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->requestService:Lcom/zendesk/sdk/network/RequestService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/RequestService;->getRequest(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 139
    return-void
.end method
