.class public interface abstract Lcom/zendesk/sdk/network/AccessProvider;
.super Ljava/lang/Object;
.source "AccessProvider.java"


# virtual methods
.method public abstract getAndStoreAuthTokenViaAnonymous(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/AnonymousIdentity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAndStoreAuthTokenViaJwt(Lcom/zendesk/sdk/model/access/JwtIdentity;Lcom/zendesk/service/ZendeskCallback;)V
    .param p1    # Lcom/zendesk/sdk/model/access/JwtIdentity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/JwtIdentity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation
.end method
