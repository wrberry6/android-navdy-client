.class Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;
.super Ljava/lang/Object;
.source "ZendeskIdentityStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/IdentityStorage;


# static fields
.field private static final IDENTITY_KEY:Ljava/lang/String; = "zendesk-identity"

.field private static final IDENTITY_TYPE_KEY:Ljava/lang/String; = "zendesk-identity-type"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final PREFS_NAME:Ljava/lang/String; = "zendesk-token"

.field private static final TOKEN_KEY:Ljava/lang/String; = "stored_token"

.field private static final UUID_KEY:Ljava/lang/String; = "uuid"


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final storage:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/gson/Gson;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    .line 47
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 48
    sget-object v0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Storage was not initialised, user token will not be stored."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :cond_0
    iput-object p2, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    .line 52
    return-void

    .line 45
    :cond_1
    const-string v0, "zendesk-token"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public anonymiseIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 199
    const/4 v2, 0x0

    .line 201
    .local v2, "identity":Lcom/zendesk/sdk/model/access/Identity;
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-nez v4, :cond_1

    .line 202
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Cannot anonymise identity as the storage is null"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    :cond_0
    :goto_0
    return-object v2

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v2

    .line 207
    if-nez v2, :cond_2

    .line 208
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Cannot anonymise a null identity."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 211
    :cond_2
    instance-of v4, v2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v4, :cond_5

    move-object v1, v2

    .line 213
    check-cast v1, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 215
    .local v1, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getEmail()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 216
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v3, 0x1

    .line 218
    .local v3, "needsAnonymisation":Z
    :cond_4
    if-eqz v3, :cond_0

    .line 219
    new-instance v4, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    invoke-direct {v4}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;-><init>()V

    .line 220
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getExternalId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withExternalIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    move-result-object v4

    .line 221
    invoke-virtual {v4}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->build()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v0

    .line 223
    .local v0, "anonymisedIdentity":Lcom/zendesk/sdk/model/access/Identity;
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 224
    move-object v2, v0

    goto :goto_0

    .line 229
    .end local v0    # "anonymisedIdentity":Lcom/zendesk/sdk/model/access/Identity;
    .end local v1    # "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .end local v3    # "needsAnonymisation":Z
    :cond_5
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Identity is not anonymous, no anonymisation is required"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public clearUserData()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 244
    :cond_0
    return-void
.end method

.method public getCacheKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    const-string v0, "zendesk-token"

    return-object v0
.end method

.method public getIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 169
    const/4 v1, 0x0

    .line 171
    .local v1, "identity":Lcom/zendesk/sdk/model/access/Identity;
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v4, :cond_0

    .line 172
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    const-string v5, "zendesk-identity"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173
    .local v2, "identityString":Ljava/lang/String;
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    const-string v5, "zendesk-identity-type"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175
    .local v3, "identityType":Ljava/lang/String;
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    invoke-static {v3}, Lcom/zendesk/sdk/model/access/AuthenticationType;->getAuthType(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v0

    .line 178
    .local v0, "authenticationType":Lcom/zendesk/sdk/model/access/AuthenticationType;
    sget-object v4, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne v4, v0, :cond_1

    .line 179
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Loading Jwt identity"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    const-class v5, Lcom/zendesk/sdk/model/access/JwtIdentity;

    invoke-virtual {v4, v2, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    check-cast v1, Lcom/zendesk/sdk/model/access/Identity;

    .line 193
    .end local v0    # "authenticationType":Lcom/zendesk/sdk/model/access/AuthenticationType;
    .end local v2    # "identityString":Ljava/lang/String;
    .end local v3    # "identityType":Ljava/lang/String;
    .restart local v1    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    :cond_0
    :goto_0
    return-object v1

    .line 182
    .restart local v0    # "authenticationType":Lcom/zendesk/sdk/model/access/AuthenticationType;
    .restart local v2    # "identityString":Ljava/lang/String;
    .restart local v3    # "identityType":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne v4, v0, :cond_2

    .line 183
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Loading Anonymous identity"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    const-class v5, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    invoke-virtual {v4, v2, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    check-cast v1, Lcom/zendesk/sdk/model/access/Identity;

    .restart local v1    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    goto :goto_0

    .line 187
    :cond_2
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Unknown identity type, identity will be null"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getStoredAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;
    .locals 5

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 69
    .local v0, "accessToken":Lcom/zendesk/sdk/model/access/AccessToken;
    iget-object v2, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    const-string v3, "stored_token"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "accessTokenString":Ljava/lang/String;
    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    const-class v3, Lcom/zendesk/sdk/model/access/AccessToken;

    invoke-virtual {v2, v1, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "accessToken":Lcom/zendesk/sdk/model/access/AccessToken;
    check-cast v0, Lcom/zendesk/sdk/model/access/AccessToken;

    .line 78
    .end local v1    # "accessTokenString":Ljava/lang/String;
    .restart local v0    # "accessToken":Lcom/zendesk/sdk/model/access/AccessToken;
    :cond_0
    return-object v0
.end method

.method public getStoredAccessTokenAsBearerToken()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->getStoredAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;

    move-result-object v0

    .line 87
    .local v0, "accessToken":Lcom/zendesk/sdk/model/access/AccessToken;
    if-nez v0, :cond_0

    .line 88
    sget-object v2, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v3, "There is no stored access token, have you initialised an identity and requested an access token?"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    :cond_0
    const-string v1, "Bearer %s"

    .line 92
    .local v1, "bearerFormat":Ljava/lang/String;
    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AccessToken;->getAccessToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 98
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    const-string v2, "uuid"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Fetching UUID from preferences store"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    const-string v2, "uuid"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :cond_1
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Generating new UUID"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "id":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 109
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Storing new UUID in preference store"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "uuid"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public storeAccessToken(Lcom/zendesk/sdk/model/access/AccessToken;)V
    .locals 3
    .param p1, "accessToken"    # Lcom/zendesk/sdk/model/access/AccessToken;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "accessTokenString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "stored_token"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 62
    .end local v0    # "accessTokenString":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 8
    .param p1, "identity"    # Lcom/zendesk/sdk/model/access/Identity;

    .prologue
    const/4 v7, 0x0

    .line 119
    iget-object v5, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    if-nez v5, :cond_1

    .line 120
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Storage is not initialised, will not store the identity"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    if-nez p1, :cond_2

    .line 125
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v6, "identity is null, will not store the identity"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 129
    :cond_2
    const/4 v3, 0x0

    .line 130
    .local v3, "identityString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 131
    .local v1, "authenticationType":Ljava/lang/String;
    const/4 v4, 0x0

    .line 133
    .local v4, "sdkGuid":Ljava/lang/String;
    instance-of v5, p1, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v5, :cond_4

    .line 134
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Storing anonymous identity"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    iget-object v5, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    const-class v6, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    invoke-virtual {v5, p1, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v3

    .line 136
    sget-object v5, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/access/AuthenticationType;->getAuthenticationType()Ljava/lang/String;

    move-result-object v1

    move-object v0, p1

    .line 138
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 139
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getSdkGuid()Ljava/lang/String;

    move-result-object v4

    .line 151
    .end local v0    # "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    :goto_1
    invoke-static {v3}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    .line 153
    iget-object v5, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 155
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "zendesk-identity"

    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "zendesk-identity-type"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 157
    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 158
    const-string v5, "uuid"

    invoke-interface {v2, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 161
    :cond_3
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 141
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_4
    instance-of v5, p1, Lcom/zendesk/sdk/model/access/JwtIdentity;

    if-eqz v5, :cond_5

    .line 142
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Storing jwt identity"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    iget-object v5, p0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->gson:Lcom/google/gson/Gson;

    const-class v6, Lcom/zendesk/sdk/model/access/JwtIdentity;

    invoke-virtual {v5, p1, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v3

    .line 144
    sget-object v5, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/access/AuthenticationType;->getAuthenticationType()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 147
    :cond_5
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Unknown authentication type, identity will not be stored"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
