.class Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;
.super Ljava/lang/Object;
.source "ZendeskSdkSettingsStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkSettingsStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MOBILE_SETTINGS_KEY:Ljava/lang/String; = "mobileconfiguration"

.field private static final MOBILE_SETTINGS_TIMESTAMP_KEY:Ljava/lang/String; = "mobileconfigurationtimestamp"

.field static final PREFERENCES_FILE:Ljava/lang/String; = "ZendeskConfigStorage"


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "ZendeskConfigStorage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 41
    iput-object p2, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->gson:Lcom/google/gson/Gson;

    .line 42
    return-void
.end method


# virtual methods
.method public areSettingsUpToDate(JLjava/util/concurrent/TimeUnit;)Z
    .locals 7
    .param p1, "duration"    # J
    .param p3, "timeUnit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 52
    iget-object v2, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "mobileconfigurationtimestamp"

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 54
    .local v0, "lastDownloaded":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public deleteStoredSettings()V
    .locals 3

    .prologue
    .line 108
    sget-object v0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Deleting stored settings"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 110
    return-void
.end method

.method public getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 66
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "mobileconfiguration"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "settingsJson":Ljava/lang/String;
    const/4 v2, 0x0

    .line 70
    .local v2, "settings":Lcom/zendesk/sdk/model/settings/MobileSettings;
    if-eqz v3, :cond_0

    .line 71
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Found stored settings"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :try_start_0
    iget-object v4, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->gson:Lcom/google/gson/Gson;

    const-class v5, Lcom/zendesk/sdk/model/settings/MobileSettings;

    invoke-virtual {v4, v3, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/zendesk/sdk/model/settings/MobileSettings;

    move-object v2, v0

    .line 75
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Successfully deserialised settings from storage"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    new-instance v4, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-direct {v4, v2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;-><init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    return-object v4

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Lcom/google/gson/JsonSyntaxException;
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Error deserialising MobileSettings from storage, going to remove stored settings"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->deleteStoredSettings()V

    goto :goto_0

    .line 83
    .end local v1    # "e":Lcom/google/gson/JsonSyntaxException;
    :cond_0
    sget-object v4, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v5, "No stored configuration was found, returning null settings"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public hasStoredSdkSettings()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "mobileconfiguration"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setStoredSettings(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 6
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/4 v3, 0x0

    .line 92
    if-nez p1, :cond_0

    .line 93
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Not going to save null MobileSettings, use deleteStoredSettings()"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->gson:Lcom/google/gson/Gson;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getMobileSettings()Lcom/zendesk/sdk/model/settings/MobileSettings;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "mobileSettingsJson":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 99
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "mobileconfiguration"

    .line 100
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "mobileconfigurationtimestamp"

    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 102
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 103
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Successfully saved settings to storage"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
