.class public Lcom/zendesk/sdk/util/ScopeCache;
.super Ljava/lang/Object;
.source "ScopeCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ScopeCache"


# instance fields
.field private cache:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private provided:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    .local p0, "this":Lcom/zendesk/sdk/util/ScopeCache;, "Lcom/zendesk/sdk/util/ScopeCache<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->provided:Z

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->cache:Ljava/lang/Object;

    .line 24
    return-void
.end method


# virtual methods
.method public declared-synchronized get(Lcom/zendesk/sdk/util/DependencyProvider;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/util/DependencyProvider",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/zendesk/sdk/util/ScopeCache;, "Lcom/zendesk/sdk/util/ScopeCache<TT;>;"
    .local p1, "provider":Lcom/zendesk/sdk/util/DependencyProvider;, "Lcom/zendesk/sdk/util/DependencyProvider<+TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->provided:Z

    if-nez v0, :cond_0

    .line 38
    invoke-interface {p1}, Lcom/zendesk/sdk/util/DependencyProvider;->provideDependency()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->cache:Ljava/lang/Object;

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->provided:Z

    .line 40
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "ScopeCache"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/zendesk/sdk/util/ScopeCache;->cache:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Creating new object"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/util/ScopeCache;->cache:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
