.class Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;
.super Ljava/lang/Object;
.source "RateMyAppSendFeedbackButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->getOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    iget-object v5, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    invoke-static {v5}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->access$000(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 58
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 60
    .local v3, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const-string v5, "rma_dialog"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 62
    .local v4, "rmaDialog":Landroid/support/v4/app/Fragment;
    if-eqz v4, :cond_0

    .line 63
    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 66
    :cond_0
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 67
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    .line 69
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 70
    const-string v5, "feedback"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 72
    .local v1, "feedbackDialogFragment":Landroid/support/v4/app/Fragment;
    if-eqz v1, :cond_1

    .line 73
    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 76
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 78
    iget-object v5, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    invoke-static {v5}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->access$100(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Lcom/zendesk/sdk/feedback/FeedbackConnector;

    move-result-object v5

    invoke-static {v5}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->newInstance(Lcom/zendesk/sdk/feedback/FeedbackConnector;)Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    move-result-object v0

    .line 80
    .local v0, "feedbackDialog":Lcom/zendesk/sdk/rating/ui/FeedbackDialog;
    iget-object v5, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    invoke-static {v5}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->access$200(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V

    .line 82
    const-string v5, "feedback"

    invoke-virtual {v0, v3, v5}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    .line 83
    return-void
.end method
