.class Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
.super Ljava/lang/Object;
.source "TargetConfiguration.java"


# instance fields
.field private final mBackStackActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeepLinkType:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

.field private final mFallbackActivity:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;Ljava/util/List;Landroid/content/Intent;)V
    .locals 0
    .param p1, "deepLinkType"    # Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .param p3, "fallbackActivity"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    .local p2, "backStackActivities":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mDeepLinkType:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 20
    iput-object p2, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mBackStackActivities:Ljava/util/List;

    .line 21
    iput-object p3, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mFallbackActivity:Landroid/content/Intent;

    .line 22
    return-void
.end method


# virtual methods
.method public getBackStackActivities()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mBackStackActivities:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mBackStackActivities:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 32
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mDeepLinkType:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    return-object v0
.end method

.method public getFallbackActivity()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->mFallbackActivity:Landroid/content/Intent;

    return-object v0
.end method
