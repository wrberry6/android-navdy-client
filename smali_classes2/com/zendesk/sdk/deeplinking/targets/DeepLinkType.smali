.class public final enum Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
.super Ljava/lang/Enum;
.source "DeepLinkType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

.field public static final enum Article:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

.field public static final enum Request:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

.field public static final enum Unknown:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    const-string v1, "Request"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Request:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 18
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    const-string v1, "Article"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Article:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 24
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Unknown:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Request:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Article:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Unknown:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->$VALUES:[Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->$VALUES:[Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    return-object v0
.end method
