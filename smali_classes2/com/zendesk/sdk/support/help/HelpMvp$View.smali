.class public interface abstract Lcom/zendesk/sdk/support/help/HelpMvp$View;
.super Ljava/lang/Object;
.source "HelpMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
.end method

.method public abstract removeItem(I)V
.end method

.method public abstract showItems(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)V"
        }
    .end annotation
.end method
