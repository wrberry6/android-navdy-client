.class public Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;
.super Ljava/lang/Object;
.source "ConnectedElementLocationFilter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryFilter;


# instance fields
.field private locations:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "locations"    # Ljava/util/List;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;->locations:Ljava/util/List;

    .line 70
    return-void
.end method

.method public static getLocations(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v0, "locations":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;

    invoke-direct {v1, v0}, Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    .line 62
    return-object v0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 74
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;->locations:Ljava/util/List;

    new-instance v1, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_1
    return-void
.end method
