.class public Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
.super Lcom/vividsolutions/jts/planargraph/DirectedEdge;
.source "LineMergeDirectedEdge.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 0
    .param p1, "from"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p2, "to"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p3, "directionPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "edgeDirection"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 62
    return-void
.end method


# virtual methods
.method public getNext()Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v3

    if-ne v2, v3, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 78
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 76
    goto :goto_1
.end method
