.class public abstract Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;
.super Ljava/lang/Object;
.source "PointShapeFactory.java"

# interfaces
.implements Lcom/vividsolutions/jts/awt/PointShapeFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/awt/PointShapeFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BasePointShapeFactory"
.end annotation


# static fields
.field public static DEFAULT_SIZE:D


# instance fields
.field protected size:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    sput-wide v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->DEFAULT_SIZE:D

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-wide v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->DEFAULT_SIZE:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->size:D

    .line 75
    return-void
.end method

.method public constructor <init>(D)V
    .locals 3
    .param p1, "size"    # D

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-wide v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->DEFAULT_SIZE:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->size:D

    .line 84
    iput-wide p1, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;->size:D

    .line 85
    return-void
.end method


# virtual methods
.method public abstract createPoint(Ljava/awt/geom/Point2D;)Ljava/awt/Shape;
.end method
