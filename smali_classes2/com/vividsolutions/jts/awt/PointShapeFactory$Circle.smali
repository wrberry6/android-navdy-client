.class public Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;
.super Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;
.source "PointShapeFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/awt/PointShapeFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Circle"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;-><init>()V

    .line 262
    return-void
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "size"    # D

    .prologue
    .line 271
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;-><init>(D)V

    .line 272
    return-void
.end method


# virtual methods
.method public createPoint(Ljava/awt/geom/Point2D;)Ljava/awt/Shape;
    .locals 12
    .param p1, "point"    # Ljava/awt/geom/Point2D;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    const-wide/16 v2, 0x0

    .line 282
    new-instance v1, Ljava/awt/geom/Ellipse2D$Double;

    iget-wide v6, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;->size:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;->size:D

    move-wide v4, v2

    invoke-direct/range {v1 .. v9}, Ljava/awt/geom/Ellipse2D$Double;-><init>(DDDD)V

    .line 288
    .local v1, "pointMarker":Ljava/awt/geom/Ellipse2D$Double;
    invoke-virtual {p1}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;->size:D

    div-double/2addr v4, v10

    sub-double/2addr v2, v4

    iput-wide v2, v1, Ljava/awt/geom/Ellipse2D$Double;->x:D

    .line 289
    invoke-virtual {p1}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;->size:D

    div-double/2addr v4, v10

    sub-double/2addr v2, v4

    iput-wide v2, v1, Ljava/awt/geom/Ellipse2D$Double;->y:D

    .line 291
    return-object v1
.end method
