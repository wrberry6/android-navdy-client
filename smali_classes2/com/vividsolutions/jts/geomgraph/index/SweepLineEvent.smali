.class public Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
.super Ljava/lang/Object;
.source "SweepLineEvent.java"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final DELETE:I = 0x2

.field public static final INSERT:I = 0x1


# instance fields
.field private deleteEventIndex:I

.field edgeSet:Ljava/lang/Object;

.field private eventType:I

.field private insertEvent:Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

.field private obj:Ljava/lang/Object;

.field private xValue:D


# direct methods
.method public constructor <init>(Ljava/lang/Object;DLcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Ljava/lang/Object;)V
    .locals 2
    .param p1, "edgeSet"    # Ljava/lang/Object;
    .param p2, "x"    # D
    .param p4, "insertEvent"    # Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    .line 57
    iput-wide p2, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->xValue:D

    .line 58
    iput-object p4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->insertEvent:Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    .line 60
    if-eqz p4, :cond_0

    .line 61
    const/4 v0, 0x2

    iput v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    .line 62
    :cond_0
    iput-object p5, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->obj:Ljava/lang/Object;

    .line 63
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 80
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 81
    .local v0, "pe":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    iget-wide v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->xValue:D

    iget-wide v6, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->xValue:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 82
    :cond_1
    iget-wide v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->xValue:D

    iget-wide v6, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->xValue:D

    cmpl-double v3, v4, v6

    if-lez v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 83
    :cond_2
    iget v3, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    iget v4, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    if-lt v3, v4, :cond_0

    .line 84
    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    iget v3, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->eventType:I

    if-le v1, v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 85
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeleteEventIndex()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->deleteEventIndex:I

    return v0
.end method

.method public getInsertEvent()Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->insertEvent:Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    return-object v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method public isDelete()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->insertEvent:Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInsert()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->insertEvent:Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDeleteEventIndex(I)V
    .locals 0
    .param p1, "deleteEventIndex"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->deleteEventIndex:I

    return-void
.end method
