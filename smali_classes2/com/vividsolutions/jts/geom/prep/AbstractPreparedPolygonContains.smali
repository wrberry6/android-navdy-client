.class abstract Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;
.super Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;
.source "AbstractPreparedPolygonContains.java"


# instance fields
.field private hasNonProperIntersection:Z

.field private hasProperIntersection:Z

.field private hasSegmentIntersection:Z

.field protected requireSomePointInInterior:Z


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 2
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->requireSomePointInInterior:Z

    .line 75
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasSegmentIntersection:Z

    .line 76
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasProperIntersection:Z

    .line 77
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasNonProperIntersection:Z

    .line 87
    return-void
.end method

.method private findAndClassifyIntersections(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 225
    invoke-static {p1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 227
    .local v2, "lineSegStr":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v1}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    .line 228
    .local v1, "li":Lcom/vividsolutions/jts/algorithm/LineIntersector;
    new-instance v0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 229
    .local v0, "intDetector":Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->setFindAllIntersectionTypes(Z)V

    .line 230
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->intersects(Ljava/util/Collection;Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;)Z

    .line 232
    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection()Z

    move-result v3

    iput-boolean v3, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasSegmentIntersection:Z

    .line 233
    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection()Z

    move-result v3

    iput-boolean v3, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasProperIntersection:Z

    .line 234
    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasNonProperIntersection()Z

    move-result v3

    iput-boolean v3, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasNonProperIntersection:Z

    .line 235
    return-void
.end method

.method private isProperIntersectionImpliesNotContainedSituation(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x1

    .line 197
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v0

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->isSingleShell(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSingleShell(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 215
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v4

    if-eq v4, v3, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v2

    .line 217
    :cond_1
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    .line 218
    .local v1, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v0

    .line 219
    .local v0, "numHoles":I
    if-nez v0, :cond_0

    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method protected eval(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->isAllTestComponentsInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    .line 105
    .local v0, "isAllInTargetArea":Z
    if-nez v0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 115
    :cond_1
    iget-boolean v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->requireSomePointInInterior:Z

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v4

    if-nez v4, :cond_2

    .line 117
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->isAnyTestComponentInTargetInterior(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    .line 118
    .local v1, "isAnyInTargetInterior":Z
    goto :goto_0

    .line 135
    .end local v1    # "isAnyInTargetInterior":Z
    :cond_2
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->isProperIntersectionImpliesNotContainedSituation(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    .line 140
    .local v3, "properIntersectionImpliesNotContained":Z
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->findAndClassifyIntersections(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 142
    if-eqz v3, :cond_3

    iget-boolean v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasProperIntersection:Z

    if-nez v4, :cond_0

    .line 159
    :cond_3
    iget-boolean v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasSegmentIntersection:Z

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasNonProperIntersection:Z

    if-eqz v4, :cond_0

    .line 168
    :cond_4
    iget-boolean v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->hasSegmentIntersection:Z

    if-eqz v4, :cond_5

    .line 169
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->fullTopologicalPredicate(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    goto :goto_0

    .line 178
    :cond_5
    instance-of v4, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v4, :cond_6

    .line 180
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getRepresentativePoints()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, p1, v4}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;->isAnyTargetComponentInAreaTest(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Z

    move-result v2

    .line 181
    .local v2, "isTargetInTestArea":Z
    if-nez v2, :cond_0

    .line 183
    .end local v2    # "isTargetInTestArea":Z
    :cond_6
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected abstract fullTopologicalPredicate(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method
