.class public Lcom/vividsolutions/jts/io/ByteOrderDataInStream;
.super Ljava/lang/Object;
.source "ByteOrderDataInStream.java"


# instance fields
.field private buf1:[B

.field private buf4:[B

.field private buf8:[B

.field private byteOrder:I

.field private stream:Lcom/vividsolutions/jts/io/InStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    .line 47
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf1:[B

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf4:[B

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/io/InStream;)V
    .locals 1
    .param p1, "stream"    # Lcom/vividsolutions/jts/io/InStream;

    .prologue
    const/4 v0, 0x1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    .line 47
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf1:[B

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf4:[B

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    .line 58
    iput-object p1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    .line 59
    return-void
.end method


# virtual methods
.method public readByte()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    iget-object v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf1:[B

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/io/InStream;->read([B)V

    .line 85
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf1:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    return v0
.end method

.method public readDouble()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    iget-object v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/io/InStream;->read([B)V

    .line 105
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    iget v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/ByteOrderValues;->getDouble([BI)D

    move-result-wide v0

    return-wide v0
.end method

.method public readInt()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    iget-object v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf4:[B

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/io/InStream;->read([B)V

    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf4:[B

    iget v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/ByteOrderValues;->getInt([BI)I

    move-result v0

    return v0
.end method

.method public readLong()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    iget-object v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/io/InStream;->read([B)V

    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->buf8:[B

    iget v1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/ByteOrderValues;->getLong([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method public setInStream(Lcom/vividsolutions/jts/io/InStream;)V
    .locals 0
    .param p1, "stream"    # Lcom/vividsolutions/jts/io/InStream;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->stream:Lcom/vividsolutions/jts/io/InStream;

    .line 70
    return-void
.end method

.method public setOrder(I)V
    .locals 0
    .param p1, "byteOrder"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/vividsolutions/jts/io/ByteOrderDataInStream;->byteOrder:I

    .line 74
    return-void
.end method
