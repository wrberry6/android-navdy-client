.class public Lcom/vividsolutions/jts/algorithm/CGAlgorithms;
.super Ljava/lang/Object;
.source "CGAlgorithms.java"


# static fields
.field public static final CLOCKWISE:I = -0x1

.field public static final COLLINEAR:I = 0x0

.field public static final COUNTERCLOCKWISE:I = 0x1

.field public static final LEFT:I = 0x1

.field public static final RIGHT:I = -0x1

.field public static final STRAIGHT:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    return-void
.end method

.method public static computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 1
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 289
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    return v0
.end method

.method public static distanceLineLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 24
    .param p0, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "C"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "D"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 423
    invoke-virtual/range {p0 .. p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 424
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 485
    :goto_0
    return-wide v4

    .line 425
    :cond_0
    invoke-virtual/range {p2 .. p3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 426
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto :goto_0

    .line 455
    :cond_1
    const/4 v14, 0x0

    .line 456
    .local v14, "noIntersection":Z
    invoke-static/range {p0 .. p3}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 457
    const/4 v14, 0x1

    .line 477
    :cond_2
    :goto_1
    if-eqz v14, :cond_6

    .line 478
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    invoke-static/range {p1 .. p3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v10

    invoke-static/range {v4 .. v11}, Lcom/vividsolutions/jts/math/MathUtil;->min(DDDD)D

    move-result-wide v4

    goto :goto_0

    .line 460
    :cond_3
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    move-object/from16 v0, p3

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    sub-double v12, v4, v6

    .line 462
    .local v12, "denom":D
    const-wide/16 v4, 0x0

    cmpl-double v4, v12, v4

    if-nez v4, :cond_4

    .line 463
    const/4 v14, 0x1

    goto :goto_1

    .line 466
    :cond_4
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    move-object/from16 v0, p3

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    sub-double v18, v4, v6

    .line 467
    .local v18, "r_num":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    sub-double v22, v4, v6

    .line 469
    .local v22, "s_num":D
    div-double v20, v22, v12

    .line 470
    .local v20, "s":D
    div-double v16, v18, v12

    .line 472
    .local v16, "r":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v16, v4

    if-ltz v4, :cond_5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v16, v4

    if-gtz v4, :cond_5

    const-wide/16 v4, 0x0

    cmpg-double v4, v20, v4

    if-ltz v4, :cond_5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v20, v4

    if-lez v4, :cond_2

    .line 473
    :cond_5
    const/4 v14, 0x1

    goto/16 :goto_1

    .line 485
    .end local v12    # "denom":D
    .end local v16    # "r":D
    .end local v18    # "r_num":D
    .end local v20    # "s":D
    .end local v22    # "s_num":D
    :cond_6
    const-wide/16 v4, 0x0

    goto/16 :goto_0
.end method

.method public static distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 16
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 309
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v8, v8, v10

    if-nez v8, :cond_0

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v8, v8, v10

    if-nez v8, :cond_0

    .line 310
    invoke-virtual/range {p0 .. p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    .line 347
    :goto_0
    return-wide v8

    .line 326
    :cond_0
    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v8, v10

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v10, v12

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    add-double v2, v8, v10

    .line 327
    .local v2, "len2":D
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v8, v10

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v10, v12

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    div-double v4, v8, v2

    .line 330
    .local v4, "r":D
    const-wide/16 v8, 0x0

    cmpg-double v8, v4, v8

    if-gtz v8, :cond_1

    .line 331
    invoke-virtual/range {p0 .. p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    goto :goto_0

    .line 332
    :cond_1
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v4, v8

    if-ltz v8, :cond_2

    .line 333
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    goto :goto_0

    .line 345
    :cond_2
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    div-double v6, v8, v2

    .line 347
    .local v6, "s":D
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    goto/16 :goto_0
.end method

.method public static distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 7
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "line"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 391
    array-length v3, p1

    if-nez v3, :cond_0

    .line 392
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v6, "Line array must contain at least one vertex"

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 395
    :cond_0
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 396
    .local v4, "minDistance":D
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 397
    aget-object v3, p1, v2

    add-int/lit8 v6, v2, 0x1

    aget-object v6, p1, v6

    invoke-static {p0, v3, v6}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 398
    .local v0, "dist":D
    cmpg-double v3, v0, v4

    if-gez v3, :cond_1

    .line 399
    move-wide v4, v0

    .line 396
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 402
    .end local v0    # "dist":D
    :cond_2
    return-wide v4
.end method

.method public static distancePointLinePerpendicular(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 12
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 373
    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    add-double v0, v4, v6

    .line 374
    .local v0, "len2":D
    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    div-double v2, v4, v0

    .line 377
    .local v2, "s":D
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    return-wide v4
.end method

.method public static isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 18
    .param p0, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 211
    move-object/from16 v0, p0

    array-length v13, v0

    add-int/lit8 v9, v13, -0x1

    .line 213
    .local v9, "nPts":I
    const/4 v13, 0x3

    if-ge v9, v13, :cond_0

    .line 214
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Ring has fewer than 3 points, so orientation cannot be determined"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 218
    :cond_0
    const/4 v13, 0x0

    aget-object v4, p0, v13

    .line 219
    .local v4, "hiPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .line 220
    .local v3, "hiIndex":I
    const/4 v5, 0x1

    .local v5, "i":I
    :goto_0
    if-gt v5, v9, :cond_2

    .line 221
    aget-object v11, p0, v5

    .line 222
    .local v11, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v14, v11, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v0, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    cmpl-double v13, v14, v16

    if-lez v13, :cond_1

    .line 223
    move-object v4, v11

    .line 224
    move v3, v5

    .line 220
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 229
    .end local v11    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    move v7, v3

    .line 231
    .local v7, "iPrev":I
    :cond_3
    add-int/lit8 v7, v7, -0x1

    .line 232
    if-gez v7, :cond_4

    .line 233
    move v7, v9

    .line 234
    :cond_4
    aget-object v13, p0, v7

    invoke-virtual {v13, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v13

    if-eqz v13, :cond_5

    if-ne v7, v3, :cond_3

    .line 237
    :cond_5
    move v6, v3

    .line 239
    .local v6, "iNext":I
    :cond_6
    add-int/lit8 v13, v6, 0x1

    rem-int v6, v13, v9

    .line 240
    aget-object v13, p0, v6

    invoke-virtual {v13, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v13

    if-eqz v13, :cond_7

    if-ne v6, v3, :cond_6

    .line 242
    :cond_7
    aget-object v12, p0, v7

    .line 243
    .local v12, "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v10, p0, v6

    .line 251
    .local v10, "next":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v12, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v13

    if-nez v13, :cond_8

    invoke-virtual {v10, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v13

    if-nez v13, :cond_8

    invoke-virtual {v12, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 252
    :cond_8
    const/4 v8, 0x0

    .line 274
    :goto_1
    return v8

    .line 254
    :cond_9
    invoke-static {v12, v4, v10}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 265
    .local v2, "disc":I
    const/4 v8, 0x0

    .line 266
    .local v8, "isCCW":Z
    if-nez v2, :cond_b

    .line 268
    iget-wide v14, v12, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    cmpl-double v13, v14, v16

    if-lez v13, :cond_a

    const/4 v8, 0x1

    :goto_2
    goto :goto_1

    :cond_a
    const/4 v8, 0x0

    goto :goto_2

    .line 272
    :cond_b
    if-lez v2, :cond_c

    const/4 v8, 0x1

    :goto_3
    goto :goto_1

    :cond_c
    const/4 v8, 0x0

    goto :goto_3
.end method

.method public static isOnLine(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pt"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 179
    new-instance v1, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v1}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    .line 180
    .local v1, "lineIntersector":Lcom/vividsolutions/jts/algorithm/LineIntersector;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 181
    add-int/lit8 v4, v0, -0x1

    aget-object v2, p1, v4

    .line 182
    .local v2, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v3, p1, v0

    .line 183
    .local v3, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, p0, v2, v3}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 184
    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 185
    const/4 v4, 0x1

    .line 188
    .end local v2    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return v4

    .line 180
    .restart local v2    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v3    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    .end local v2    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 148
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static length(Lcom/vividsolutions/jts/geom/CoordinateSequence;)D
    .locals 24
    .param p0, "pts"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 566
    invoke-interface/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v7

    .line 567
    .local v7, "n":I
    const/4 v11, 0x1

    if-gt v7, v11, :cond_1

    .line 568
    const-wide/16 v8, 0x0

    .line 589
    :cond_0
    return-wide v8

    .line 570
    :cond_1
    const-wide/16 v8, 0x0

    .line 572
    .local v8, "len":D
    new-instance v10, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v10}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 573
    .local v10, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v11, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 574
    iget-wide v12, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 575
    .local v12, "x0":D
    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    .line 577
    .local v16, "y0":D
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_0

    .line 578
    move-object/from16 v0, p0

    invoke-interface {v0, v6, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 579
    iget-wide v14, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 580
    .local v14, "x1":D
    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    .line 581
    .local v18, "y1":D
    sub-double v2, v14, v12

    .line 582
    .local v2, "dx":D
    sub-double v4, v18, v16

    .line 584
    .local v4, "dy":D
    mul-double v20, v2, v2

    mul-double v22, v4, v4

    add-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    add-double v8, v8, v20

    .line 586
    move-wide v12, v14

    .line 587
    move-wide/from16 v16, v18

    .line 577
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public static locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 1
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 167
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;->locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    return v0
.end method

.method public static orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 1
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 117
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    return v0
.end method

.method public static signedArea(Lcom/vividsolutions/jts/geom/CoordinateSequence;)D
    .locals 18
    .param p0, "ring"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 530
    invoke-interface/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v3

    .line 531
    .local v3, "n":I
    const/4 v7, 0x3

    if-ge v3, v7, :cond_0

    .line 532
    const-wide/16 v12, 0x0

    .line 553
    :goto_0
    return-wide v12

    .line 537
    :cond_0
    new-instance v4, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 538
    .local v4, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 539
    .local v5, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v6, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v6}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 540
    .local v6, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v7, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 541
    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v7, v6}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 542
    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 543
    .local v10, "x0":D
    iget-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v12, v10

    iput-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 544
    const-wide/16 v8, 0x0

    .line 545
    .local v8, "sum":D
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    add-int/lit8 v7, v3, -0x1

    if-ge v2, v7, :cond_1

    .line 546
    iget-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v12, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 547
    iget-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iput-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 548
    iget-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 549
    add-int/lit8 v7, v2, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v7, v6}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 550
    iget-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v12, v10

    iput-wide v12, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 551
    iget-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v14, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v0, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    add-double/2addr v8, v12

    .line 545
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 553
    :cond_1
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v8, v12

    goto :goto_0
.end method

.method public static signedArea([Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 14
    .param p0, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 499
    array-length v1, p0

    const/4 v12, 0x3

    if-ge v1, v12, :cond_0

    .line 500
    const-wide/16 v12, 0x0

    .line 513
    :goto_0
    return-wide v12

    .line 501
    :cond_0
    const-wide/16 v2, 0x0

    .line 506
    .local v2, "sum":D
    const/4 v1, 0x0

    aget-object v1, p0, v1

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 507
    .local v6, "x0":D
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 508
    aget-object v1, p0, v0

    iget-wide v12, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v4, v12, v6

    .line 509
    .local v4, "x":D
    add-int/lit8 v1, v0, 0x1

    aget-object v1, p0, v1

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 510
    .local v8, "y1":D
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 511
    .local v10, "y2":D
    sub-double v12, v10, v8

    mul-double/2addr v12, v4

    add-double/2addr v2, v12

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 513
    .end local v4    # "x":D
    .end local v8    # "y1":D
    .end local v10    # "y2":D
    :cond_1
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v2, v12

    goto :goto_0
.end method
