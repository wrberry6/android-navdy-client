.class public Lcom/vividsolutions/jts/algorithm/match/HausdorffSimilarityMeasure;
.super Ljava/lang/Object;
.source "HausdorffSimilarityMeasure.java"

# interfaces
.implements Lcom/vividsolutions/jts/algorithm/match/SimilarityMeasure;


# static fields
.field private static final DENSIFY_FRACTION:D = 0.25


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method public static diagonalSize(Lcom/vividsolutions/jts/geom/Envelope;)D
    .locals 8
    .param p0, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->isNull()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x0

    .line 93
    :goto_0
    return-wide v4

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v2

    .line 92
    .local v2, "width":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v0

    .line 93
    .local v0, "hgt":D
    mul-double v4, v2, v2

    mul-double v6, v0, v0

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    goto :goto_0
.end method


# virtual methods
.method public measure(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 12
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 75
    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    invoke-static {p1, p2, v8, v9}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)D

    move-result-wide v0

    .line 77
    .local v0, "distance":D
    new-instance v2, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 78
    .local v2, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 79
    invoke-static {v2}, Lcom/vividsolutions/jts/algorithm/match/HausdorffSimilarityMeasure;->diagonalSize(Lcom/vividsolutions/jts/geom/Envelope;)D

    move-result-wide v4

    .line 81
    .local v4, "envSize":D
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double v10, v0, v4

    sub-double v6, v8, v10

    .line 84
    .local v6, "measure":D
    return-wide v6
.end method
