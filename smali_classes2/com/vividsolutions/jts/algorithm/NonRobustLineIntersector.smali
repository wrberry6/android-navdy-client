.class public Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;
.super Lcom/vividsolutions/jts/algorithm/LineIntersector;
.source "NonRobustLineIntersector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/LineIntersector;-><init>()V

    .line 66
    return-void
.end method

.method private computeCollinearIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 19
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p3"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "p4"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 255
    const-wide/16 v6, 0x0

    .line 256
    .local v6, "r1":D
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 257
    .local v8, "r2":D
    invoke-direct/range {p0 .. p3}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->rParameter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v10

    .line 258
    .local v10, "r3":D
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->rParameter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v12

    .line 260
    .local v12, "r4":D
    cmpg-double v18, v10, v12

    if-gez v18, :cond_1

    .line 261
    move-object/from16 v4, p3

    .line 262
    .local v4, "q3":Lcom/vividsolutions/jts/geom/Coordinate;
    move-wide v14, v10

    .line 263
    .local v14, "t3":D
    move-object/from16 v5, p4

    .line 264
    .local v5, "q4":Lcom/vividsolutions/jts/geom/Coordinate;
    move-wide/from16 v16, v12

    .line 273
    .local v16, "t4":D
    :goto_0
    cmpl-double v18, v14, v8

    if-gtz v18, :cond_0

    cmpg-double v18, v16, v6

    if-gez v18, :cond_2

    .line 274
    :cond_0
    const/16 v18, 0x0

    .line 296
    :goto_1
    return v18

    .line 267
    .end local v4    # "q3":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v5    # "q4":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v14    # "t3":D
    .end local v16    # "t4":D
    :cond_1
    move-object/from16 v4, p4

    .line 268
    .restart local v4    # "q3":Lcom/vividsolutions/jts/geom/Coordinate;
    move-wide v14, v12

    .line 269
    .restart local v14    # "t3":D
    move-object/from16 v5, p3

    .line 270
    .restart local v5    # "q4":Lcom/vividsolutions/jts/geom/Coordinate;
    move-wide/from16 v16, v10

    .restart local v16    # "t4":D
    goto :goto_0

    .line 278
    :cond_2
    move-object/from16 v0, p1

    if-ne v5, v0, :cond_3

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 280
    const/16 v18, 0x1

    goto :goto_1

    .line 282
    :cond_3
    move-object/from16 v0, p2

    if-ne v4, v0, :cond_4

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 284
    const/16 v18, 0x1

    goto :goto_1

    .line 288
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 289
    cmpl-double v18, v14, v6

    if-lez v18, :cond_5

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 292
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pb:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 293
    cmpg-double v18, v16, v8

    if-gez v18, :cond_6

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pb:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->setCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 296
    :cond_6
    const/16 v18, 0x2

    goto :goto_1
.end method

.method public static isSameSignAndNonZero(DD)Z
    .locals 4
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 58
    cmpl-double v1, p0, v2

    if-eqz v1, :cond_0

    cmpl-double v1, p2, v2

    if-nez v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpg-double v1, p0, v2

    if-gez v1, :cond_2

    cmpg-double v1, p2, v2

    if-ltz v1, :cond_3

    :cond_2
    cmpl-double v1, p0, v2

    if-lez v1, :cond_0

    cmpl-double v1, p2, v2

    if-lez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rParameter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 12
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 309
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 310
    .local v0, "dx":D
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 311
    .local v2, "dy":D
    cmpl-double v6, v0, v2

    if-lez v6, :cond_0

    .line 312
    iget-wide v6, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v8, v10

    div-double v4, v6, v8

    .line 317
    .local v4, "r":D
    :goto_0
    return-wide v4

    .line 315
    .end local v4    # "r":D
    :cond_0
    iget-wide v6, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v10

    div-double v4, v6, v8

    .restart local v4    # "r":D
    goto :goto_0
.end method


# virtual methods
.method protected computeIntersect(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 36
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p3"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "p4"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 147
    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 153
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    sub-double v4, v30, v32

    .line 154
    .local v4, "a1":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v32, v0

    sub-double v8, v30, v32

    .line 155
    .local v8, "b1":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v30, v30, v32

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v32, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v34, v0

    mul-double v32, v32, v34

    sub-double v12, v30, v32

    .line 160
    .local v12, "c1":D
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    mul-double v30, v30, v4

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v32, v32, v8

    add-double v30, v30, v32

    add-double v26, v30, v12

    .line 161
    .local v26, "r3":D
    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    mul-double v30, v30, v4

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v32, v32, v8

    add-double v30, v30, v32

    add-double v28, v30, v12

    .line 167
    .local v28, "r4":D
    const-wide/16 v30, 0x0

    cmpl-double v30, v26, v30

    if-eqz v30, :cond_0

    const-wide/16 v30, 0x0

    cmpl-double v30, v28, v30

    if-eqz v30, :cond_0

    invoke-static/range {v26 .. v29}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isSameSignAndNonZero(DD)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 170
    const/16 v30, 0x0

    .line 228
    :goto_0
    return v30

    .line 176
    :cond_0
    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    sub-double v6, v30, v32

    .line 177
    .local v6, "a2":D
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v32, v0

    sub-double v10, v30, v32

    .line 178
    .local v10, "b2":D
    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v30, v30, v32

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v32, v0

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v34, v0

    mul-double v32, v32, v34

    sub-double v14, v30, v32

    .line 183
    .local v14, "c2":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    mul-double v30, v30, v6

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v32, v32, v10

    add-double v30, v30, v32

    add-double v22, v30, v14

    .line 184
    .local v22, "r1":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    mul-double v30, v30, v6

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v32, v0

    mul-double v32, v32, v10

    add-double v30, v30, v32

    add-double v24, v30, v14

    .line 191
    .local v24, "r2":D
    const-wide/16 v30, 0x0

    cmpl-double v30, v22, v30

    if-eqz v30, :cond_1

    const-wide/16 v30, 0x0

    cmpl-double v30, v24, v30

    if-eqz v30, :cond_1

    invoke-static/range {v22 .. v25}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isSameSignAndNonZero(DD)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 194
    const/16 v30, 0x0

    goto :goto_0

    .line 200
    :cond_1
    mul-double v30, v4, v10

    mul-double v32, v6, v8

    sub-double v16, v30, v32

    .line 201
    .local v16, "denom":D
    const-wide/16 v30, 0x0

    cmpl-double v30, v16, v30

    if-nez v30, :cond_2

    .line 202
    invoke-direct/range {p0 .. p4}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->computeCollinearIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v30

    goto/16 :goto_0

    .line 204
    :cond_2
    mul-double v30, v8, v14

    mul-double v32, v10, v12

    sub-double v18, v30, v32

    .line 205
    .local v18, "numX":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    div-double v32, v18, v16

    move-wide/from16 v0, v32

    move-object/from16 v2, v30

    iput-wide v0, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 213
    mul-double v30, v6, v12

    mul-double v32, v4, v14

    sub-double v20, v30, v32

    .line 214
    .local v20, "numY":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    div-double v32, v20, v16

    move-wide/from16 v0, v32

    move-object/from16 v2, v30

    iput-wide v0, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 218
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 220
    :cond_3
    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 225
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-object/from16 v30, v0

    if-eqz v30, :cond_5

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->pa:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 228
    :cond_5
    const/16 v30, 0x1

    goto/16 :goto_0
.end method

.method public computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 20
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 82
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 88
    move-object/from16 v0, p3

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v4, v14, v16

    .line 89
    .local v4, "a1":D
    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v6, v14, v16

    .line 90
    .local v6, "b1":D
    move-object/from16 v0, p3

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    sub-double v8, v14, v16

    .line 95
    .local v8, "c1":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v14, v4

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    mul-double v16, v16, v6

    add-double v14, v14, v16

    add-double v12, v14, v8

    .line 98
    .local v12, "r":D
    const-wide/16 v14, 0x0

    cmpl-double v14, v12, v14

    if-eqz v14, :cond_0

    .line 99
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->result:I

    .line 116
    :goto_0
    return-void

    .line 105
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->rParameter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v10

    .line 106
    .local v10, "dist":D
    const-wide/16 v14, 0x0

    cmpg-double v14, v10, v14

    if-ltz v14, :cond_1

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    cmpl-double v14, v10, v14

    if-lez v14, :cond_2

    .line 107
    :cond_1
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->result:I

    goto :goto_0

    .line 111
    :cond_2
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 112
    invoke-virtual/range {p1 .. p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_3

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 113
    :cond_3
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->isProper:Z

    .line 115
    :cond_4
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/vividsolutions/jts/algorithm/NonRobustLineIntersector;->result:I

    goto :goto_0
.end method
