.class public final Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
.super Ljava/lang/Object;
.source "RuntimeTypeAdapterFactory.java"

# interfaces
.implements Lcom/google/gson/TypeAdapterFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/gson/TypeAdapterFactory;"
    }
.end annotation


# instance fields
.field private final aliasToSubtype:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final baseType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final labelToSubtype:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final subtypeToLabel:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final typeFieldName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p2, "typeFieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory<TT;>;"
    .local p1, "baseType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->labelToSubtype:Ljava/util/Map;

    .line 128
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->aliasToSubtype:Ljava/util/Map;

    .line 129
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->subtypeToLabel:Ljava/util/Map;

    .line 132
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 135
    :cond_1
    iput-object p1, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->baseType:Ljava/lang/Class;

    .line 136
    iput-object p2, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->typeFieldName:Ljava/lang/String;

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->typeFieldName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/lang/Class;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->baseType:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->subtypeToLabel:Ljava/util/Map;

    return-object v0
.end method

.method public static of(Ljava/lang/Class;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "baseType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    const-string v1, "type"

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .locals 1
    .param p1, "typeFieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "baseType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
    .locals 6
    .param p1, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken",
            "<TR;>;)",
            "Lcom/google/gson/TypeAdapter",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory<TT;>;"
    .local p2, "type":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<TR;>;"
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->baseType:Ljava/lang/Class;

    if-eq v4, v5, :cond_0

    .line 211
    const/4 v4, 0x0

    .line 228
    :goto_0
    return-object v4

    .line 214
    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 216
    .local v2, "labelToDelegate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/gson/TypeAdapter<*>;>;"
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 218
    .local v3, "subtypeToDelegate":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<*>;Lcom/google/gson/TypeAdapter<*>;>;"
    iget-object v4, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->labelToSubtype:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 219
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v4

    invoke-virtual {p1, p0, v4}, Lcom/google/gson/Gson;->getDelegateAdapter(Lcom/google/gson/TypeAdapterFactory;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    .line 220
    .local v0, "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 223
    .end local v0    # "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;"
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    :cond_1
    iget-object v4, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->aliasToSubtype:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 224
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v4

    invoke-virtual {p1, p0, v4}, Lcom/google/gson/Gson;->getDelegateAdapter(Lcom/google/gson/TypeAdapterFactory;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    .line 225
    .restart local v0    # "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 228
    .end local v0    # "delegate":Lcom/google/gson/TypeAdapter;, "Lcom/google/gson/TypeAdapter<*>;"
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    :cond_2
    new-instance v4, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;

    invoke-direct {v4, p0, v2, v3}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory$1;-><init>(Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public registerSubtype(Ljava/lang/Class;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory<TT;>;"
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->registerSubtype(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    move-result-object v0

    return-object v0
.end method

.method public registerSubtype(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .locals 2
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 163
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory<TT;>;"
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 164
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->subtypeToLabel:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->labelToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "types and labels must be unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_3
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->aliasToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "alias already exists for label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_4
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->labelToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->subtypeToLabel:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    return-object p0
.end method

.method public registerSubtypeAlias(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .locals 2
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "this":Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;, "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory<TT;>;"
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 186
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->labelToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->aliasToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "labels must be unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->subtypeToLabel:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "subtype must already have a mapping to add an alias"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_4
    iget-object v0, p0, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->aliasToSubtype:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    return-object p0
.end method
