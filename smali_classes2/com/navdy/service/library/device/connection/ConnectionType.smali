.class public final enum Lcom/navdy/service/library/device/connection/ConnectionType;
.super Ljava/lang/Enum;
.source "ConnectionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/device/connection/ConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum BT_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum EA_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum HTTP:Lcom/navdy/service/library/device/connection/ConnectionType;

.field public static final enum TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;


# instance fields
.field private final mServiceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "HTTP"

    const-string v2, "_http._tcp."

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->HTTP:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 11
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "TCP_PROTOBUF"

    const-string v2, "_navdybus._tcp."

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 12
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "BT_PROTOBUF"

    invoke-direct {v0, v1, v5}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 13
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "EA_PROTOBUF"

    invoke-direct {v0, v1, v6}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 14
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "BT_TUNNEL"

    invoke-direct {v0, v1, v7}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 15
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "EA_TUNNEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 16
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    const-string v1, "BT_IAP2_LINK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 9
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->HTTP:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_TUNNEL:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->$VALUES:[Lcom/navdy/service/library/device/connection/ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionType;->mServiceType:Ljava/lang/String;

    .line 22
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "serviceType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lcom/navdy/service/library/device/connection/ConnectionType;->mServiceType:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static fromServiceType(Ljava/lang/String;)Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 5
    .param p0, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionType;->values()[Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 34
    .local v0, "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionType;->getServiceType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 39
    .end local v0    # "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    :goto_1
    return-object v0

    .line 33
    .restart local v0    # "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    .end local v0    # "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getServiceTypes()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionType;->values()[Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v2

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 45
    .local v0, "names":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionType;->values()[Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 46
    .local v1, "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->getServiceType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 47
    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->getServiceType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    .end local v1    # "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    :cond_1
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->$VALUES:[Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/connection/ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method


# virtual methods
.method public getServiceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionType;->mServiceType:Ljava/lang/String;

    return-object v0
.end method
