.class Lcom/navdy/service/library/device/connection/ConnectionService$1;
.super Lcom/navdy/hud/app/IEventSource$Stub;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-direct {p0}, Lcom/navdy/hud/app/IEventSource$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addEventListener(Lcom/navdy/hud/app/IEventListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/navdy/hud/app/IEventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 280
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->listenerLock:Ljava/lang/Object;

    monitor-enter v2

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$200(Lcom/navdy/service/library/device/connection/ConnectionService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$300(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    .line 283
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "listener added: pid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionService$1;->getCallingPid()I

    move-result v4

    invoke-static {v3, v4}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 285
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    new-instance v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 291
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    new-instance v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v3, v3, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 295
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 296
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    .line 297
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "send device info"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 306
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->sendEventsOnLocalConnect()V

    .line 307
    return-void

    .line 283
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 301
    :cond_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    new-instance v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    const-string v3, ""

    sget-object v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 302
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    new-instance v2, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    const-string v3, ""

    sget-object v4, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    goto :goto_0
.end method

.method public postEvent([B)V
    .locals 10
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 320
    const/4 v3, 0x0

    .line 321
    .local v3, "sentEvent":Z
    const/4 v0, 0x0

    .line 322
    .local v0, "createList":Z
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v7, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->listenerLock:Ljava/lang/Object;

    monitor-enter v7

    .line 323
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v6}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$400(Lcom/navdy/service/library/device/connection/ConnectionService;)[Lcom/navdy/hud/app/IEventListener;

    move-result-object v6

    array-length v6, v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v6, :cond_0

    .line 325
    :try_start_1
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v6}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$400(Lcom/navdy/service/library/device/connection/ConnectionService;)[Lcom/navdy/hud/app/IEventListener;

    move-result-object v6

    aget-object v6, v6, v2

    invoke-interface {v6, p1}, Lcom/navdy/hud/app/IEventListener;->onEvent([B)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    const/4 v3, 0x1

    .line 323
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 327
    :catch_0
    move-exception v1

    .line 328
    .local v1, "exception":Landroid/os/DeadObjectException;
    :try_start_2
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v6, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Reaping dead listener"

    invoke-virtual {v6, v8, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 329
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v6}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$200(Lcom/navdy/service/library/device/connection/ConnectionService;)Ljava/util/List;

    move-result-object v6

    iget-object v8, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v8}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$400(Lcom/navdy/service/library/device/connection/ConnectionService;)[Lcom/navdy/hud/app/IEventListener;

    move-result-object v8

    aget-object v8, v8, v2

    invoke-interface {v6, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 330
    const/4 v0, 0x1

    .line 337
    goto :goto_1

    .line 331
    .end local v1    # "exception":Landroid/os/DeadObjectException;
    :catch_1
    move-exception v5

    .line 334
    .local v5, "tooLargeException":Landroid/os/TransactionTooLargeException;
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v6, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Communication Pipe is full:"

    invoke-virtual {v6, v8, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 345
    .end local v5    # "tooLargeException":Landroid/os/TransactionTooLargeException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 335
    :catch_2
    move-exception v4

    .line 336
    .local v4, "throwable":Ljava/lang/Throwable;
    :try_start_3
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v6, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Exception throws by remote:"

    invoke-virtual {v6, v8, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 339
    .end local v4    # "throwable":Ljava/lang/Throwable;
    :cond_0
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v6, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v6

    if-eqz v6, :cond_1

    if-nez v3, :cond_1

    .line 340
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v6, v6, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No one listening for event - byte length "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 342
    :cond_1
    if-eqz v0, :cond_2

    .line 343
    iget-object v6, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v6}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$300(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    .line 345
    :cond_2
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 346
    return-void
.end method

.method public postRemoteEvent(Ljava/lang/String;[B)V
    .locals 5
    .param p1, "remoteDeviceId"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 350
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 351
    :cond_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "illegal argument"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 352
    new-instance v2, Landroid/os/RemoteException;

    const-string v3, "ILLEGAL_ARGUMENT"

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 354
    :cond_1
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v0, p1}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    .line 357
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 358
    invoke-static {p2}, Lcom/navdy/service/library/events/WireUtil;->getEventType([B)Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    move-result-object v1

    .line 359
    .local v1, "messageType":Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v2, p2, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->processLocalEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 360
    :cond_2
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Connection service ignored message:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 379
    .end local v1    # "messageType":Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    :cond_3
    :goto_0
    return-void

    .line 366
    :cond_4
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v2

    if-nez v2, :cond_6

    .line 368
    :cond_5
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "app not connected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 373
    :cond_6
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 374
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2, p2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent([B)Z

    goto :goto_0

    .line 377
    :cond_7
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device id mismatch: deviceId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " remote:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeEventListener(Lcom/navdy/hud/app/IEventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/navdy/hud/app/IEventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$200(Lcom/navdy/service/library/device/connection/ConnectionService;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-static {v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$300(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    .line 314
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listener removed: pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$1;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionService$1;->getCallingPid()I

    move-result v3

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 316
    return-void

    .line 314
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
