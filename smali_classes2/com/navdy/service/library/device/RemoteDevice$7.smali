.class synthetic Lcom/navdy/service/library/device/RemoteDevice$7;
.super Ljava/lang/Object;
.source "RemoteDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 364
    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionType;->values()[Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    :try_start_0
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->EA_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_IAP2_LINK:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 313
    :goto_3
    invoke-static {}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->values()[Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

    :try_start_4
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$7;->$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus:[I

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    .line 364
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_0
.end method
