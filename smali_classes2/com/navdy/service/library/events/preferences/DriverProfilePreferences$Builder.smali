.class public final Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DriverProfilePreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public additionalLocales:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public auto_on_enabled:Ljava/lang/Boolean;

.field public car_make:Ljava/lang/String;

.field public car_model:Ljava/lang/String;

.field public car_year:Ljava/lang/String;

.field public device_name:Ljava/lang/String;

.field public dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

.field public display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

.field public driver_email:Ljava/lang/String;

.field public driver_name:Ljava/lang/String;

.field public feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

.field public limit_bandwidth:Ljava/lang/Boolean;

.field public locale:Ljava/lang/String;

.field public obdBlacklistLastModified:Ljava/lang/Long;

.field public obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public photo_checksum:Ljava/lang/String;

.field public profile_is_public:Ljava/lang/Boolean;

.field public serial_number:Ljava/lang/Long;

.field public unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 285
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 289
    if-nez p1, :cond_0

    .line 309
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 291
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_name:Ljava/lang/String;

    .line 292
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->device_name:Ljava/lang/String;

    .line 293
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->profile_is_public:Ljava/lang/Boolean;

    .line 294
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->photo_checksum:Ljava/lang/String;

    .line 295
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_email:Ljava/lang/String;

    .line 296
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_make:Ljava/lang/String;

    .line 297
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_model:Ljava/lang/String;

    .line 298
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_year:Ljava/lang/String;

    .line 299
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->auto_on_enabled:Ljava/lang/Boolean;

    .line 300
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 301
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->locale:Ljava/lang/String;

    .line 302
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 303
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 304
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 305
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->limit_bandwidth:Ljava/lang/Boolean;

    .line 306
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdBlacklistLastModified:Ljava/lang/Long;

    .line 307
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 308
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->additionalLocales:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public additionalLocales(Ljava/util/List;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;"
        }
    .end annotation

    .prologue
    .line 472
    .local p1, "additionalLocales":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->additionalLocales:Ljava/util/List;

    .line 473
    return-object p0
.end method

.method public auto_on_enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "auto_on_enabled"    # Ljava/lang/Boolean;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->auto_on_enabled:Ljava/lang/Boolean;

    .line 391
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    .locals 2

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->checkRequiredFields()V

    .line 479
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v0

    return-object v0
.end method

.method public car_make(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "car_make"    # Ljava/lang/String;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_make:Ljava/lang/String;

    .line 367
    return-object p0
.end method

.method public car_model(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "car_model"    # Ljava/lang/String;

    .prologue
    .line 374
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_model:Ljava/lang/String;

    .line 375
    return-object p0
.end method

.method public car_year(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "car_year"    # Ljava/lang/String;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_year:Ljava/lang/String;

    .line 383
    return-object p0
.end method

.method public device_name(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "device_name"    # Ljava/lang/String;

    .prologue
    .line 332
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->device_name:Ljava/lang/String;

    .line 333
    return-object p0
.end method

.method public dial_long_press_action(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "dial_long_press_action"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 464
    return-object p0
.end method

.method public display_format(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "display_format"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 399
    return-object p0
.end method

.method public driver_email(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "driver_email"    # Ljava/lang/String;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_email:Ljava/lang/String;

    .line 359
    return-object p0
.end method

.method public driver_name(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "driver_name"    # Ljava/lang/String;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_name:Ljava/lang/String;

    .line 325
    return-object p0
.end method

.method public feature_mode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "feature_mode"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .prologue
    .line 429
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 430
    return-object p0
.end method

.method public limit_bandwidth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "limit_bandwidth"    # Ljava/lang/Boolean;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->limit_bandwidth:Ljava/lang/Boolean;

    .line 443
    return-object p0
.end method

.method public locale(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->locale:Ljava/lang/String;

    .line 408
    return-object p0
.end method

.method public obdBlacklistLastModified(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "obdBlacklistLastModified"    # Ljava/lang/Long;

    .prologue
    .line 455
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdBlacklistLastModified:Ljava/lang/Long;

    .line 456
    return-object p0
.end method

.method public obdScanSetting(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "obdScanSetting"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 435
    return-object p0
.end method

.method public photo_checksum(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "photo_checksum"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->photo_checksum:Ljava/lang/String;

    .line 351
    return-object p0
.end method

.method public profile_is_public(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "profile_is_public"    # Ljava/lang/Boolean;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->profile_is_public:Ljava/lang/Boolean;

    .line 342
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 317
    return-object p0
.end method

.method public unit_system(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .locals 0
    .param p1, "unit_system"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .prologue
    .line 416
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 417
    return-object p0
.end method
