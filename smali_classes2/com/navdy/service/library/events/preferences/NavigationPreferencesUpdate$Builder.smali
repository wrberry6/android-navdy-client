.class public final Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationPreferencesUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

.field public serial_number:Ljava/lang/Long;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 102
    if-nez p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 104
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 105
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 106
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->checkRequiredFields()V

    .line 144
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    move-result-object v0

    return-object v0
.end method

.method public preferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    .locals 0
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 138
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 130
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 114
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 122
    return-object p0
.end method
