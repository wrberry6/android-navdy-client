.class public final enum Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;
.super Ljava/lang/Enum;
.source "NavigationPreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/NavigationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RerouteForTraffic"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

.field public static final enum REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

.field public static final enum REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

.field public static final enum REROUTE_NEVER:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 293
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    const-string v1, "REROUTE_CONFIRM"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 294
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    const-string v1, "REROUTE_AUTOMATIC"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 295
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    const-string v1, "REROUTE_NEVER"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_NEVER:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 291
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_NEVER:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->$VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 300
    iput p3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->value:I

    .line 301
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 291
    const-class v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;
    .locals 1

    .prologue
    .line 291
    sget-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->$VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->value:I

    return v0
.end method
