.class public final Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TripUpdateAck.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/location/TripUpdateAck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/location/TripUpdateAck;",
        ">;"
    }
.end annotation


# instance fields
.field public sequence_number:Ljava/lang/Integer;

.field public trip_number:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/location/TripUpdateAck;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/location/TripUpdateAck;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 75
    if-nez p1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->trip_number:Ljava/lang/Long;

    .line 77
    iget-object v0, p1, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->sequence_number:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/location/TripUpdateAck;
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->checkRequiredFields()V

    .line 99
    new-instance v0, Lcom/navdy/service/library/events/location/TripUpdateAck;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/location/TripUpdateAck;-><init>(Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;Lcom/navdy/service/library/events/location/TripUpdateAck$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->build()Lcom/navdy/service/library/events/location/TripUpdateAck;

    move-result-object v0

    return-object v0
.end method

.method public sequence_number(Ljava/lang/Integer;)Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;
    .locals 0
    .param p1, "sequence_number"    # Ljava/lang/Integer;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->sequence_number:Ljava/lang/Integer;

    .line 93
    return-object p0
.end method

.method public trip_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;
    .locals 0
    .param p1, "trip_number"    # Ljava/lang/Long;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->trip_number:Ljava/lang/Long;

    .line 85
    return-object p0
.end method
