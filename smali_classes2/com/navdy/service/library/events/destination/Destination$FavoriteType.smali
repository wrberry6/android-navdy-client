.class public final enum Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
.super Ljava/lang/Enum;
.source "Destination.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/destination/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FavoriteType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final enum FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final enum FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final enum FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final enum FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final enum FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 459
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    const-string v1, "FAVORITE_NONE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 460
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    const-string v1, "FAVORITE_HOME"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 461
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    const-string v1, "FAVORITE_WORK"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 462
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    const-string v1, "FAVORITE_CONTACT"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 466
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    const-string v1, "FAVORITE_CUSTOM"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 457
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->$VALUES:[Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 470
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 471
    iput p3, p0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->value:I

    .line 472
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 457
    const-class v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->$VALUES:[Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->value:I

    return v0
.end method
