.class public final Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PlaceTypeSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public destinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public request_id:Ljava/lang/String;

.field public request_status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 90
    if-nez p1, :cond_0

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id:Ljava/lang/String;

    .line 92
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 93
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;-><init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public destinations(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)",
            "Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations:Ljava/util/List;

    .line 118
    return-object p0
.end method

.method public request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;
    .locals 0
    .param p1, "request_status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 110
    return-object p0
.end method
