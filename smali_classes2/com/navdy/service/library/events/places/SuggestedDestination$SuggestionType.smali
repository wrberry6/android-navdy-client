.class public final enum Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;
.super Ljava/lang/Enum;
.source "SuggestedDestination.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/SuggestedDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

.field public static final enum SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

.field public static final enum SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    new-instance v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    const-string v1, "SUGGESTION_RECOMMENDATION"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .line 116
    new-instance v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    const-string v1, "SUGGESTION_CALENDAR"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .line 113
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    sget-object v1, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->$VALUES:[Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput p3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->value:I

    .line 122
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 113
    const-class v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->$VALUES:[Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->value:I

    return v0
.end method
