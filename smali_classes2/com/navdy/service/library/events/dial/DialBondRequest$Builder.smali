.class public final Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DialBondRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/dial/DialBondRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/dial/DialBondRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/dial/DialBondRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/dial/DialBondRequest;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 56
    if-nez p1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 65
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/dial/DialBondRequest;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/navdy/service/library/events/dial/DialBondRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;Lcom/navdy/service/library/events/dial/DialBondRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;->build()Lcom/navdy/service/library/events/dial/DialBondRequest;

    move-result-object v0

    return-object v0
.end method
