.class public final Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopDriveRecordingResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 58
    if-nez p1, :cond_0

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;->checkRequiredFields()V

    .line 73
    new-instance v0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;->build()Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 67
    return-object p0
.end method
