.class final enum Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
.super Ljava/lang/Enum;
.source "NavCoordTestSuite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Step"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum GOOGLE_DIRECTIONS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_GEOCODER_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_GEOCODER_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_NATIVE_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_WEB_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum HERE_WEB_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

.field public static final enum NAVDY_PROCESS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;


# instance fields
.field markerColor:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 281
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_NATIVE_PLACES_SEARCH_WITH_NAME"

    const-string v2, "white"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_NATIVE_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 282
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_WEB_PLACES_SEARCH_WITH_NAME"

    const-string v2, "brown"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_WEB_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 283
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME"

    const-string v2, "green"

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 284
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_WEB_PLACES_SEARCH_WITHOUT_NAME"

    const-string v2, "purple"

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_WEB_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 285
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_GEOCODER_WITH_NAME"

    const-string v2, "yellow"

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_GEOCODER_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 286
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "HERE_GEOCODER_WITHOUT_NAME"

    const/4 v2, 0x5

    const-string v3, "blue"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_GEOCODER_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 287
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "GOOGLE_DIRECTIONS"

    const/4 v2, 0x6

    const-string v3, "orange"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->GOOGLE_DIRECTIONS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 288
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    const-string v1, "NAVDY_PROCESS"

    const/4 v2, 0x7

    const-string v3, "black"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->NAVDY_PROCESS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 280
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    sget-object v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_NATIVE_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_WEB_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_WEB_PLACES_SEARCH_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_GEOCODER_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_GEOCODER_WITHOUT_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->GOOGLE_DIRECTIONS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->NAVDY_PROCESS:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->$VALUES:[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "markerColor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 290
    const-string v0, "red"

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->markerColor:Ljava/lang/String;

    .line 293
    iput-object p3, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->markerColor:Ljava/lang/String;

    .line 294
    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->isLastStep()Z

    move-result v0

    return v0
.end method

.method public static firstStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    .locals 2

    .prologue
    .line 301
    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->values()[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method private isLastStep()Z
    .locals 2

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->values()[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 280
    const-class v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->$VALUES:[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    return-object v0
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->markerColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public nextStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->isLastStep()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->firstStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v0

    .line 308
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->values()[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method
