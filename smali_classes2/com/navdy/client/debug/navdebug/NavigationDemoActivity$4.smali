.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;
.super Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;
.source "NavigationDemoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnded(Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    .prologue
    .line 482
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$1000(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    .line 483
    return-void
.end method

.method public onRouteUpdated(Lcom/here/android/mpa/routing/Route;)V
    .locals 2
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 474
    invoke-super {p0, p1}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;->onRouteUpdated(Lcom/here/android/mpa/routing/Route;)V

    .line 475
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$800(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$700(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 476
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    new-instance v1, Lcom/here/android/mpa/mapping/MapRoute;

    invoke-direct {v1, p1}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/here/android/mpa/routing/Route;)V

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$702(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/mapping/MapRoute;)Lcom/here/android/mpa/mapping/MapRoute;

    .line 477
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$800(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$700(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 478
    return-void
.end method

.method public onRunningStateChanged()V
    .locals 4

    .prologue
    .line 453
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$900(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/NavigationManager;->getRunningState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    move-result-object v0

    .line 454
    .local v0, "navigationState":Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Running state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 456
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$9;->$SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 467
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown nav state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 470
    :goto_0
    return-void

    .line 458
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->toggleButton:Landroid/widget/Button;

    const-string v2, "||"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 461
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->toggleButton:Landroid/widget/Button;

    const-string v2, ">"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 464
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->toggleButton:Landroid/widget/Button;

    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 456
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
