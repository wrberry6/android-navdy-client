.class public abstract Lcom/navdy/client/debug/file/FileTransferManager;
.super Ljava/lang/Object;
.source "FileTransferManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;
    }
.end annotation


# static fields
.field protected static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected final mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

.field protected final mDestinationFileName:Ljava/lang/String;

.field protected final mDestinationFolder:Ljava/lang/String;

.field protected final mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

.field protected final mFileType:Lcom/navdy/service/library/events/file/FileType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/file/FileTransferManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/file/FileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "destinationFolder"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 39
    iput-object p4, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .line 40
    iput-object v0, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    .line 41
    iput-object v0, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDestinationFileName:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDestinationFolder:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/file/TransferDataSource;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Lcom/navdy/service/library/file/TransferDataSource;
    .param p3, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p4, "destinationFileName"    # Ljava/lang/String;
    .param p5, "fileTransferListener"    # Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mContext:Landroid/content/Context;

    .line 49
    iput-object p5, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    .line 50
    invoke-virtual {p2}, Lcom/navdy/service/library/file/TransferDataSource;->getFile()Ljava/io/File;

    move-result-object v0

    .line 51
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    const-string v1, "File does not exist"

    invoke-virtual {p0, v2, v1}, Lcom/navdy/client/debug/file/FileTransferManager;->postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 54
    :cond_0
    iput-object p2, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDataSource:Lcom/navdy/service/library/file/TransferDataSource;

    .line 55
    iput-object p3, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 56
    iput-object p4, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDestinationFileName:Ljava/lang/String;

    .line 57
    iput-object v2, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mDestinationFolder:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public abstract cancelFileUpload()V
.end method

.method protected postOnError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 3
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    invoke-interface {v1, p1, p2}, Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;->onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/debug/file/FileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Bad listener : onError "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected postOnFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 80
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    invoke-interface {v1, p1}, Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;->onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/debug/file/FileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Bad listener : onFileTransferResponse "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected postOnFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/debug/file/FileTransferManager;->mFileTransferListener:Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;

    invoke-interface {v1, p1}, Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;->onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/debug/file/FileTransferManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Bad listener : onFileTransferStatus "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public abstract pullFile()Z
.end method

.method public abstract sendFile()Z
.end method

.method public abstract sendFile(J)Z
.end method
