.class Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;
.super Ljava/lang/Object;
.source "MockLocationTransmitter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 71
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-virtual {v1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 72
    .local v0, "location":Landroid/location/Location;
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1, v0}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$000(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;Landroid/location/Location;)Z

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 74
    .local v2, "now":J
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$100(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1, v2, v3}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$102(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;J)J

    .line 78
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    .line 79
    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 80
    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setAltitude(D)V

    .line 81
    invoke-virtual {v0, v8}, Landroid/location/Location;->setBearing(F)V

    .line 82
    invoke-virtual {v0, v8}, Landroid/location/Location;->setSpeed(F)V

    .line 83
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1, v0}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$200(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;Landroid/location/Location;)Z

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$300(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-static {v1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->access$400(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 87
    :cond_1
    return-void
.end method
