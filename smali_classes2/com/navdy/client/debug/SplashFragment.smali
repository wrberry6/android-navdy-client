.class public Lcom/navdy/client/debug/SplashFragment;
.super Landroid/app/Fragment;
.source "SplashFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method onConnectClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100286
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/navdy/client/debug/SplashFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/navdy/client/debug/DevicePickerFragment;

    invoke-static {v0, v1}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 39
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    const v1, 0x7f03009b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 31
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 33
    return-object v0
.end method
