.class Lcom/navdy/client/debug/HudSettingsFragment$2;
.super Ljava/lang/Object;
.source "HudSettingsFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/HudSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/HudSettingsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/HudSettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/HudSettingsFragment;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "i"    # I
    .param p3, "b"    # Z

    .prologue
    .line 170
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 190
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Unknown slider"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 173
    :sswitch_0
    const-string v0, "map.zoom"

    .line 174
    .local v0, "setting":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    int-to-double v6, p2

    const-wide v8, 0x402638e38e38e38eL    # 11.11111111111111

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 175
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/HudSettingsFragment;->zoomLevelTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    invoke-static {v2, v0, v1}, Lcom/navdy/client/debug/HudSettingsFragment;->access$000(Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void

    .line 178
    .end local v0    # "setting":Ljava/lang/String;
    :sswitch_1
    const-string v0, "map.tilt"

    .line 179
    .restart local v0    # "setting":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/HudSettingsFragment;->tiltLevelTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 182
    .end local v0    # "setting":Ljava/lang/String;
    :sswitch_2
    const-string v0, "screen.brightness"

    .line 183
    .restart local v0    # "setting":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/HudSettingsFragment;->brightnessTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 186
    .end local v0    # "setting":Ljava/lang/String;
    :sswitch_3
    const-string v0, "screen.led_brightness"

    .line 187
    .restart local v0    # "setting":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$2;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightnessTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 171
    :sswitch_data_0
    .sparse-switch
        0x7f100257 -> :sswitch_2
        0x7f10025a -> :sswitch_3
        0x7f10025f -> :sswitch_0
        0x7f100261 -> :sswitch_1
    .end sparse-switch
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 198
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 203
    return-void
.end method
