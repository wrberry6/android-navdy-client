.class public Lcom/navdy/client/debug/NavDebugActionDialog;
.super Landroid/app/Dialog;
.source "NavDebugActionDialog.java"


# static fields
.field private static final COORD_TEST_ADDRESS:I = 0x0

.field private static final COORD_TEST_NAME:I = 0x1

.field private static final GEO_INTENT_CALENDAR:I = 0x7

.field private static final GEO_INTENT_HANGOUT:I = 0x8

.field private static final GOOGLE_MAPS_INTENT_ADDRESS:I = 0x4

.field private static final GOOGLE_MAPS_INTENT_COORD:I = 0x5

.field private static final GOOGLE_MAPS_INTENT_URL_WITH_BOTH:I = 0x6

.field private static final GOOGLE_NAV_INTENT_ADDRESS:I = 0x2

.field private static final GOOGLE_NAV_INTENT_COORD:I = 0x3

.field private static final SHARE_INTENT_ASSISTANT:I = 0xa

.field private static final SHARE_INTENT_MAPS:I = 0x9

.field private static final SHARE_INTENT_YELP:I = 0xb

.field private static final STREET_VIEW:I = 0xc


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method


# virtual methods
.method public createAndShow(Lcom/navdy/client/app/framework/models/Destination;)Landroid/support/v7/app/AlertDialog;
    .locals 5
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/navdy/client/debug/NavDebugActionDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 47
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 48
    .local v3, "res":Landroid/content/res/Resources;
    const v4, 0x7f090024

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "options":[Ljava/lang/CharSequence;
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v4, 0x7f0800a9

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 53
    new-instance v4, Lcom/navdy/client/debug/NavDebugActionDialog$1;

    invoke-direct {v4, p0, p1, v1}, Lcom/navdy/client/debug/NavDebugActionDialog$1;-><init>(Lcom/navdy/client/debug/NavDebugActionDialog;Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V

    invoke-virtual {v0, v2, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 165
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
