.class public final enum Lcom/navdy/client/ota/OTAUpdateUIClient$Error;
.super Ljava/lang/Enum;
.source "OTAUpdateUIClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/ota/OTAUpdateUIClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/ota/OTAUpdateUIClient$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

.field public static final enum NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

.field public static final enum SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    const-string v1, "NO_CONNECTIVITY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    .line 14
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateUIClient$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/ota/OTAUpdateUIClient$Error;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-virtual {v0}, [Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    return-object v0
.end method
