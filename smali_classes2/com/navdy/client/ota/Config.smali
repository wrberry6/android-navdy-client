.class public Lcom/navdy/client/ota/Config;
.super Ljava/lang/Object;
.source "Config.java"


# instance fields
.field mUpdateInterval:J


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const v0, 0x7f0e0012

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/navdy/client/ota/Config;->mUpdateInterval:J

    .line 15
    return-void
.end method
