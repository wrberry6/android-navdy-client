.class Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/support/v4/util/Pair",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

.field final synthetic val$autocompleteResults:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity$7$1;Ljava/util/List;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    .prologue
    .line 561
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->val$autocompleteResults:Ljava/util/List;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/support/v4/util/Pair;
    .locals 3
    .param p1, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 564
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationsFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 566
    .local v0, "knownDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getSearchHistoryFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 567
    .local v1, "pastQueries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Landroid/support/v4/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 561
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->doInBackground([Ljava/lang/String;)Landroid/support/v4/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/support/v4/util/Pair;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 572
    .local p1, "pair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v3

    const-string v4, "Search_Autocomplete"

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 574
    iget-object v0, p1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 575
    .local v0, "knownDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iget-object v1, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    .line 577
    .local v1, "previousSearches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 579
    .local v2, "query":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 580
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 581
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v4, v4, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setLastQuery(Ljava/lang/String;)V

    .line 582
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->val$contactList:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithContactsForAutoComplete(Ljava/util/List;)V

    .line 583
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/util/List;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v6, v6, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->val$contactList:Ljava/util/List;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->val$autocompleteResults:Ljava/util/List;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItemIfEmpty([Ljava/util/List;)V

    .line 587
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v4, v4, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithDatabaseData(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 590
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->val$autocompleteResults:Ljava/util/List;

    iget-object v5, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v5, v5, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    invoke-virtual {v3, v4, v1, v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithAutocomplete(Ljava/util/List;Ljava/util/ArrayList;Ljava/lang/CharSequence;)V

    .line 594
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->this$2:Lcom/navdy/client/app/ui/search/SearchActivity$7$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1500(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 600
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 561
    check-cast p1, Landroid/support/v4/util/Pair;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->onPostExecute(Landroid/support/v4/util/Pair;)V

    return-void
.end method
