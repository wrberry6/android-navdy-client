.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field final synthetic val$suggestions:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->val$suggestions:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 426
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v2, :cond_0

    .line 427
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->val$suggestions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    .line 430
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 431
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v2, "first_time_on_homescreen"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 433
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "first_time_on_homescreen"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 435
    const-string v2, "user_watched_the_demo"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 437
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 438
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startActivity(Landroid/content/Intent;)V

    .line 441
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method
