.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->updateOrSaveFavoriteToDbAsync(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    .prologue
    .line 738
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryCompleted(ILandroid/net/Uri;)V
    .locals 2
    .param p1, "nbRows"    # I
    .param p2, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 741
    if-gtz p1, :cond_0

    .line 742
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$700(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Something went wrong while trying to save the favorite to the db."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->onFavoriteListChanged()V

    .line 746
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideProgressDialog()V

    .line 747
    return-void
.end method
