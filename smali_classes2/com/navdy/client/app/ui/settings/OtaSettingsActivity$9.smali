.class synthetic Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

.field static final synthetic $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

.field static final synthetic $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

.field static final synthetic $SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

.field static final synthetic $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 725
    invoke-static {}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->values()[Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-virtual {v1}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_12

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildType;->eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-virtual {v1}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_11

    .line 351
    :goto_1
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->values()[Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    :try_start_2
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_10

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->DEVICE_NOT_CONNECTED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_f

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_e

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_d

    .line 306
    :goto_5
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->values()[Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    :try_start_6
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_c

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_b

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NOT_ENOUGH_SPACE:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_a

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_8

    .line 262
    :goto_a
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->values()[Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error:[I

    :try_start_b
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_7

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_6

    .line 192
    :goto_c
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService$State;->values()[Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    :try_start_d
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_5

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_4

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_3

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_2

    :goto_10
    :try_start_11
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_1

    :goto_11
    :try_start_12
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_INSTALL:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_0

    :goto_12
    return-void

    :catch_0
    move-exception v0

    goto :goto_12

    :catch_1
    move-exception v0

    goto :goto_11

    :catch_2
    move-exception v0

    goto :goto_10

    :catch_3
    move-exception v0

    goto :goto_f

    :catch_4
    move-exception v0

    goto :goto_e

    :catch_5
    move-exception v0

    goto :goto_d

    .line 262
    :catch_6
    move-exception v0

    goto :goto_c

    :catch_7
    move-exception v0

    goto :goto_b

    .line 306
    :catch_8
    move-exception v0

    goto :goto_a

    :catch_9
    move-exception v0

    goto/16 :goto_9

    :catch_a
    move-exception v0

    goto/16 :goto_8

    :catch_b
    move-exception v0

    goto/16 :goto_7

    :catch_c
    move-exception v0

    goto/16 :goto_6

    .line 351
    :catch_d
    move-exception v0

    goto/16 :goto_5

    :catch_e
    move-exception v0

    goto/16 :goto_4

    :catch_f
    move-exception v0

    goto/16 :goto_3

    :catch_10
    move-exception v0

    goto/16 :goto_2

    .line 725
    :catch_11
    move-exception v0

    goto/16 :goto_1

    :catch_12
    move-exception v0

    goto/16 :goto_0
.end method
