.class public Lcom/navdy/client/app/ui/settings/ReplyViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ReplyViewHolder.java"


# instance fields
.field public final more:Landroid/widget/ImageView;

.field public final text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 22
    const v0, 0x7f10000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->text:Landroid/widget/TextView;

    .line 23
    const v0, 0x7f1003bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ReplyViewHolder;->more:Landroid/widget/ImageView;

    .line 25
    return-void
.end method
