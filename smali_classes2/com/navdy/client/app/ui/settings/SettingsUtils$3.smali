.class final Lcom/navdy/client/app/ui/settings/SettingsUtils$3;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/SettingsUtils;->setLimitCellularData(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$isLimited:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$3;->val$isLimited:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 292
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "limit_cellular_data"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$3;->val$isLimited:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 293
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    .line 294
    return-void
.end method
