.class Lcom/navdy/client/app/ui/settings/SupportActivity$1;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/SupportActivity;->onHelpCenterClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/SupportActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/SupportActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/SupportActivity;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/SupportActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/SupportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/client/app/framework/util/ZendeskJWT;->getZendeskUri()Landroid/net/Uri;

    move-result-object v1

    .line 48
    .local v1, "zendeskUri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 49
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/SupportActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/SupportActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/settings/SupportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v0, "browserIntent":Landroid/content/Intent;
    const-string v2, "type"

    const-string v3, "Zendesk"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v2, "extra_html"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/SupportActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/SupportActivity;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/settings/SupportActivity;->startActivity(Landroid/content/Intent;)V

    .line 54
    .end local v0    # "browserIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
