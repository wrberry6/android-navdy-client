.class public Lcom/navdy/client/app/ui/glances/GlanceUtils;
.super Ljava/lang/Object;
.source "GlanceUtils.java"


# static fields
.field private static logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/glances/GlanceUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/glances/GlanceUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildGlancesPreferences(JZZZ)Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .locals 6
    .param p0, "serialNumber"    # J
    .param p2, "glancesAreEnabled"    # Z
    .param p3, "readAloud"    # Z
    .param p4, "showContent"    # Z

    .prologue
    .line 86
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v5, "settings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/notification/NotificationSetting;>;"
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;-><init>()V

    const-string v1, "com.navdy.phone"

    .line 88
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    const-string v1, "com.navdy.phone"

    .line 89
    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    .line 87
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;-><init>()V

    const-string v1, "com.navdy.sms"

    .line 92
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    const-string v1, "com.navdy.sms"

    .line 93
    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    .line 91
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;-><init>()V

    const-string v1, "com.navdy.fuel"

    .line 96
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    const-string v1, "com.navdy.fuel"

    .line 97
    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    .line 95
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;-><init>()V

    const-string v1, "com.navdy.music"

    .line 100
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    const-string v1, "com.navdy.music"

    .line 101
    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    .line 99
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;-><init>()V

    const-string v1, "com.navdy.traffic"

    .line 104
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->app(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    const-string v1, "com.navdy.traffic"

    .line 105
    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledInItself(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/navdy/service/library/events/notification/NotificationSetting$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationSetting;

    move-result-object v0

    .line 103
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    .line 107
    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->buildGlancesPreferences(JZZZLjava/util/List;)Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static buildGlancesPreferences(JZZZLjava/util/List;)Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .locals 2
    .param p0, "serialNumber"    # J
    .param p2, "glancesAreEnabled"    # Z
    .param p3, "readAloud"    # Z
    .param p4, "showContent"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZZZ",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationSetting;",
            ">;)",
            "Lcom/navdy/service/library/events/preferences/NotificationPreferences;"
        }
    .end annotation

    .prologue
    .line 115
    .local p5, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationSetting;>;"
    new-instance v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;-><init>()V

    .line 116
    .local v0, "notifPrefBldr":Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 117
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->enabled:Ljava/lang/Boolean;

    .line 118
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->readAloud:Ljava/lang/Boolean;

    .line 119
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->showContent:Ljava/lang/Boolean;

    .line 120
    iput-object p5, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->settings:Ljava/util/List;

    .line 121
    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v1

    return-object v1
.end method

.method public static getDefaultValueFor(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 146
    invoke-static {p0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isDrivingGlance(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    const/4 v0, 0x1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 148
    :cond_1
    invoke-static {p0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isWhiteListedApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public static isCallGlancesEnabled()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 44
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 45
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 46
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getDialerPackage(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, "dialerPackage":Ljava/lang/String;
    const-string v5, "glances"

    invoke-interface {v2, v5, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 48
    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public static isDrivingGlance(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->DRIVING_GLANCES:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p0, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    return v0
.end method

.method public static isSmsGlancesEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 53
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSmsPackage()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "smsPackage":Ljava/lang/String;
    const-string v4, "glances"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 55
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public static isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z
    .locals 6
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 60
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->getDefaultValueFor(Ljava/lang/String;)Z

    move-result v0

    .line 61
    .local v0, "defaultVal":Z
    sget-object v4, Lcom/navdy/client/app/ui/glances/GlanceUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "glances are globally turned "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v2, "glances"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "on"

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " and are "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 62
    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "on"

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " for "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " default value for this package is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 61
    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 63
    const-string v2, "glances"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 64
    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    return v2

    .line 61
    :cond_0
    const-string v2, "off"

    goto :goto_0

    .line 62
    :cond_1
    const-string v2, "off"

    goto :goto_1

    :cond_2
    move v2, v3

    .line 64
    goto :goto_2
.end method

.method public static isThisGlanceEnabledInItself(Ljava/lang/String;)Z
    .locals 5
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 69
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->getDefaultValueFor(Ljava/lang/String;)Z

    move-result v0

    .line 70
    .local v0, "defaultVal":Z
    sget-object v3, Lcom/navdy/client/app/ui/glances/GlanceUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "glances are "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "on"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " default value for this package is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 71
    invoke-interface {v1, p0, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    return v2

    .line 70
    :cond_0
    const-string v2, "off"

    goto :goto_0
.end method

.method public static isWhiteListedApp(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pkg"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->WHITE_LIST:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p0, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    return v0
.end method

.method public static saveGlancesConfigurationChanges(Ljava/lang/String;Z)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Z

    .prologue
    .line 75
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 76
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 77
    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 78
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 79
    const-string v1, "Glances_Configured"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public static sendGlancesSettingsToTheHudBasedOnSharedPrefValue()Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 129
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 130
    .local v3, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v7, "glances_serial_number"

    const-wide/16 v8, 0x0

    invoke-interface {v3, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 132
    .local v4, "serialNumber":J
    const-string v7, "glances"

    invoke-interface {v3, v7, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 134
    .local v0, "enabled":Z
    const-string v7, "glances_read_aloud"

    const/4 v8, 0x1

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 136
    .local v2, "readAloud":Z
    const-string v7, "glances_show_content"

    invoke-interface {v3, v7, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 139
    .local v6, "showContent":Z
    invoke-static {v4, v5, v0, v2, v6}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->buildGlancesPreferences(JZZZ)Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v1

    .line 142
    .local v1, "notifPrefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendGlancesSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)Z

    move-result v7

    return v7
.end method

.method public static sendTestGlance(Lcom/navdy/client/app/ui/base/BaseActivity;)V
    .locals 1
    .param p0, "activity"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->sendTestGlance(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;)V

    .line 156
    return-void
.end method

.method public static sendTestGlance(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;)V
    .locals 14
    .param p0, "activity"    # Lcom/navdy/client/app/ui/base/BaseActivity;
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 159
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 161
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 162
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 164
    .local v6, "glanceMessageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090003

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 165
    .local v7, "glanceMessages":[Ljava/lang/String;
    const v9, 0x7f0804b1

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 166
    .local v8, "title":Ljava/lang/String;
    const v9, 0x7f0801dc

    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 168
    .local v3, "firstMessage":Ljava/lang/String;
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 172
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 173
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 174
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 175
    if-eqz p1, :cond_0

    .line 176
    const-string v9, "Device is connected. Sending test glance"

    invoke-virtual {p1, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    :cond_0
    new-instance v9, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v10, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v8}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    new-instance v9, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v10, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 184
    .local v5, "glanceMessage":Ljava/lang/String;
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v8}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_NAVDY_MAIN:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->GLANCE_ICON_MESSAGE_SIDE_BLUE:Lcom/navdy/service/library/events/glances/GlanceIconConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/GlanceIconConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    new-instance v10, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v11, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 189
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    const-string v11, "Navdy"

    .line 191
    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 192
    invoke-virtual {v10, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v4

    .line 194
    .local v4, "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v10

    invoke-virtual {v10, v4}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    goto/16 :goto_0

    .line 197
    .end local v4    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v5    # "glanceMessage":Ljava/lang/String;
    :cond_1
    if-eqz p1, :cond_2

    .line 198
    const-string v9, "Device is disconnected. Cannot send test glance"

    invoke-virtual {p1, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isFinishing()Z

    move-result v9

    if-nez v9, :cond_3

    .line 201
    const/4 v9, 0x0

    const v10, 0x7f0802df

    invoke-virtual {p0, v10}, Lcom/navdy/client/app/ui/base/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    const v11, 0x7f0801db

    invoke-virtual {p0, v11}, Lcom/navdy/client/app/ui/base/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v9, v10, v11}, Lcom/navdy/client/app/ui/base/BaseActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_3
    return-void
.end method
