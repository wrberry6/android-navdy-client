.class public Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;
.super Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;
.source "FavoritesFragmentRecyclerAdapter.java"

# interfaces
.implements Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;"
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static maxShortcutCountPerActivity:I


# instance fields
.field private listener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    .line 47
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-lt v2, v3, :cond_0

    .line 48
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    .local v0, "context":Landroid/content/Context;
    const-class v2, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ShortcutManager;

    .line 50
    .local v1, "shortcutManager":Landroid/content/pm/ShortcutManager;
    invoke-virtual {v1}, Landroid/content/pm/ShortcutManager;->getMaxShortcutCountPerActivity()I

    move-result v2

    sput v2, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->maxShortcutCountPerActivity:I

    .line 53
    :cond_0
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;-><init>(Landroid/content/Context;ZZ)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->maxShortcutCountPerActivity:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getItem(I)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    invoke-static {v0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->dataIsValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 70
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getNewCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 7
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 95
    instance-of v4, p1, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;

    if-nez v4, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v2, p1

    .line 98
    check-cast v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;

    .line 101
    .local v2, "favoriteViewHolder":Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;
    iget-boolean v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->dataIsValid:Z

    if-nez v4, :cond_2

    .line 102
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "this should only be called when the cursor is valid"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 104
    :cond_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v4, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 105
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "couldn\'t move cursor to position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cursor = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->cursor:Landroid/database/Cursor;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 110
    :cond_4
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 113
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v3

    .line 114
    .local v3, "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->firstLine:Landroid/widget/TextView;

    if-eqz v4, :cond_5

    .line 115
    iget-object v5, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->firstLine:Landroid/widget/TextView;

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :cond_5
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->secondLine:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    .line 118
    iget-object v5, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_6
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->icon:Landroid/widget/ImageView;

    if-eqz v4, :cond_7

    .line 121
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    :cond_7
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->edit:Landroid/widget/ImageView;

    if-eqz v4, :cond_8

    .line 124
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->edit:Landroid/widget/ImageView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 125
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->edit:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_8
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->row:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 128
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->row:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 129
    iget-object v4, v2, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->row:Landroid/view/View;

    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 131
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Landroid/database/CursorIndexOutOfBoundsException;
    sget-object v4, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Error happened while reading the destination cursor: "

    invoke-virtual {v4, v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onCreateFooterViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->onCreateFooterViewHolder(Landroid/view/ViewGroup;)Lcom/navdy/client/app/ui/favorites/PaddingFooterViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateFooterViewHolder(Landroid/view/ViewGroup;)Lcom/navdy/client/app/ui/favorites/PaddingFooterViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300cc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 88
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/app/ui/favorites/PaddingFooterViewHolder;

    invoke-direct {v1, v0}, Lcom/navdy/client/app/ui/favorites/PaddingFooterViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onCreateNormalViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->onCreateNormalViewHolder(Landroid/view/ViewGroup;)Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateNormalViewHolder(Landroid/view/ViewGroup;)Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030057

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;

    invoke-direct {v1, v0}, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public onItemDismiss(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 212
    return-void
.end method

.method public onItemMove(II)V
    .locals 3
    .param p1, "fromPosition"    # I
    .param p2, "toPosition"    # I

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 151
    .local v0, "fromDestination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {p0, p2}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 152
    .local v1, "toDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 153
    invoke-virtual {v0, p2}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteOrder(I)V

    .line 154
    invoke-virtual {v1, p1}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteOrder(I)V

    .line 155
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb(Z)I

    .line 156
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb()I

    .line 157
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->notifyDataSetChanged()V

    .line 159
    :cond_0
    return-void
.end method

.method setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    .line 138
    return-void
.end method

.method public setupDynamicShortcuts()V
    .locals 3

    .prologue
    .line 162
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 207
    return-void
.end method
