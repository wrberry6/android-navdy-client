.class public Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "FavoriteViewHolder.java"


# instance fields
.field protected edit:Landroid/widget/ImageView;

.field protected firstLine:Landroid/widget/TextView;

.field protected icon:Landroid/widget/ImageView;

.field protected row:Landroid/view/View;

.field protected secondLine:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 24
    const v0, 0x7f100151

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->row:Landroid/view/View;

    .line 25
    const v0, 0x7f10007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->icon:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f100153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->firstLine:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f100154

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->secondLine:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f100152

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoriteViewHolder;->edit:Landroid/widget/ImageView;

    .line 29
    return-void
.end method
