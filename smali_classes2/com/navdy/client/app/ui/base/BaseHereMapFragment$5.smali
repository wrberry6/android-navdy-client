.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$initListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->val$initListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 240
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$17;->$SwitchMap$com$navdy$client$app$ui$base$BaseHereMapFragment$State:[I

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$300(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 250
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$900(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->val$initListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 253
    :goto_0
    return-void

    .line 243
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->val$initListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;->onInit(Lcom/here/android/mpa/mapping/Map;)V

    goto :goto_0

    .line 246
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->val$initListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$5;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$600(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;->onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
