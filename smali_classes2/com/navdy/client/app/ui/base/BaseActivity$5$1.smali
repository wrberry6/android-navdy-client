.class Lcom/navdy/client/app/ui/base/BaseActivity$5$1;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseActivity$5;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseActivity$5;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseActivity$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseActivity$5;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity$5$1;->this$1:Lcom/navdy/client/app/ui/base/BaseActivity$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$5$1;->this$1:Lcom/navdy/client/app/ui/base/BaseActivity$5;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseActivity$5;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "user overrode blacklist"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$5$1;->this$1:Lcom/navdy/client/app/ui/base/BaseActivity$5;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseActivity$5;->val$sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hud_obd_on_enabled"

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_overrided_blacklist"

    const/4 v2, 0x0

    .line 347
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 348
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 349
    return-void
.end method
