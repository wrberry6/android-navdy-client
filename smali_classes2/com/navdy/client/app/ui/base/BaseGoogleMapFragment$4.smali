.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnGoogleMapFragmentAttached;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttached()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "hide, already hidden, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 260
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 261
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 262
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    goto :goto_0
.end method
