.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;
.super Lcom/navdy/client/app/framework/util/CarMdCallBack;
.source "EditCarInfoUsingWebApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObdInfoCallBack"
.end annotation


# instance fields
.field private customerPrefs:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p2, "customerPrefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;-><init>()V

    .line 432
    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->customerPrefs:Landroid/content/SharedPreferences;

    .line 433
    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->goToNextStep()V

    return-void
.end method

.method private goToNextStep()V
    .locals 3

    .prologue
    .line 506
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_next_step"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    .local v0, "nextStep":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;

    invoke-direct {v2, p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 522
    return-void
.end method


# virtual methods
.method public processFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$1700(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Unable to parse car md obd location response."

    invoke-virtual {v0, v1, p1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 496
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 503
    return-void
.end method

.method public processResponse(Lokhttp3/ResponseBody;)V
    .locals 8
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;

    .prologue
    .line 439
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/CarMdClient;->getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/navdy/client/app/framework/util/CarMdClient;->parseCarMdObdLocationResponse(Lokhttp3/ResponseBody;)Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;

    move-result-object v1

    .line 441
    .local v1, "carMdObdLocationResponse":Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->customerPrefs:Landroid/content/SharedPreferences;

    iget-object v5, v1, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->note:Ljava/lang/String;

    iget-object v6, v1, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->accessNote:Ljava/lang/String;

    iget v7, v1, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->saveObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 450
    :try_start_1
    iget-object v4, v1, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->accessImageURL:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 452
    iget-object v4, v1, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->accessImageURL:Ljava/lang/String;

    invoke-static {v4}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v3

    .line 454
    .local v3, "requestUrl":Lokhttp3/HttpUrl;
    invoke-static {}, Lcom/navdy/client/app/framework/util/CarMdClient;->getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;

    move-result-object v4

    const/4 v5, 0x0

    .line 455
    invoke-virtual {v4, v3, v5}, Lcom/navdy/client/app/framework/util/CarMdClient;->buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;

    move-result-object v0

    .line 457
    .local v0, "accessImageRequest":Lokhttp3/Request;
    invoke-static {}, Lcom/navdy/client/app/framework/util/CarMdClient;->getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;

    move-result-object v4

    new-instance v5, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    invoke-virtual {v4, v5, v0}, Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V

    .line 491
    .end local v0    # "accessImageRequest":Lokhttp3/Request;
    .end local v1    # "carMdObdLocationResponse":Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    .end local v3    # "requestUrl":Lokhttp3/HttpUrl;
    :goto_0
    return-void

    .line 481
    .restart local v1    # "carMdObdLocationResponse":Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->goToNextStep()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 483
    :catch_0
    move-exception v2

    .line 485
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$1600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Something went wrong while trying to get the obd location photo."

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 486
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->goToNextStep()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 488
    .end local v1    # "carMdObdLocationResponse":Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 489
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->processFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
