.class public Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "AppSetupBottomCardGenericFragment.java"


# static fields
.field public static final SCREEN:Ljava/lang/String; = "screen"


# instance fields
.field private blueText:Landroid/widget/TextView;

.field private button:Landroid/widget/Button;

.field private description:Landroid/widget/TextView;

.field private screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    .line 23
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getScreen(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    .line 30
    return-void
.end method

.method private showThis(IIII)V
    .locals 4
    .param p1, "titleRes"    # I
    .param p2, "descriptionRes"    # I
    .param p3, "blueTextRes"    # I
    .param p4, "buttonRes"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 61
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->title:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 62
    if-lez p1, :cond_4

    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->description:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 70
    if-lez p2, :cond_5

    .line 71
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->description:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->description:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->blueText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 78
    if-lez p3, :cond_6

    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->blueText:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->blueText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 85
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->button:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 86
    if-lez p4, :cond_7

    .line 87
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->button:Landroid/widget/Button;

    invoke-virtual {v0, p4}, Landroid/widget/Button;->setText(I)V

    .line 88
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->button:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 93
    :cond_3
    :goto_3
    return-void

    .line 66
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 74
    :cond_5
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->description:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 82
    :cond_6
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->blueText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 90
    :cond_7
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->button:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    const v1, 0x7f030065

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f10007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->title:Landroid/widget/TextView;

    .line 44
    const v1, 0x7f10012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->description:Landroid/widget/TextView;

    .line 45
    const v1, 0x7f10016c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->blueText:Landroid/widget/TextView;

    .line 46
    const v1, 0x7f10016d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->button:Landroid/widget/Button;

    .line 48
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showNormal()V

    .line 49
    return-object v0
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->setArguments(Landroid/os/Bundle;)V

    .line 35
    const-string v0, "screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    .line 36
    return-void
.end method

.method public showFail()V
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v0, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleFailRes:I

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v1, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionFailRes:I

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v2, v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->blueTextRes:I

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v3, v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonFailRes:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showThis(IIII)V

    .line 58
    return-void
.end method

.method public showNormal()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v0, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->titleRes:I

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v1, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->descriptionRes:I

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v2, v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->blueTextRes:I

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->screen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v3, v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->buttonRes:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showThis(IIII)V

    .line 54
    return-void
.end method
