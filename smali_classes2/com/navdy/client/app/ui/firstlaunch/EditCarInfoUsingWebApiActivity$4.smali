.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;
.super Ljava/lang/Object;
.source "EditCarInfoUsingWebApiActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

.field final synthetic val$model:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;->val$model:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "selectedModelPosition"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-gtz p3, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;->val$model:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p3, v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 341
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
