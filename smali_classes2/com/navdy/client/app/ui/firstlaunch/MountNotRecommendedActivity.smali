.class public Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "MountNotRecommendedActivity.java"


# instance fields
.field box:Ljava/lang/String;

.field customerPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 31
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "box"

    const-string v2, "Old_Box"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->box:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->onBackPressed()V

    .line 104
    return-void
.end method

.method public onContinueWithShortClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_mount"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "mount":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_step"

    const v3, 0x7f030074

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 87
    const-string v2, "extra_mount"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    :cond_0
    const-string v2, "Short_Mount_Not_Recommended_Continue"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 98
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->startActivity(Landroid/content/Intent;)V

    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->finish()V

    .line 100
    return-void

    .line 92
    :cond_2
    const-string v2, "Medium_Tall_Mount_Not_Recommended_Continue"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 94
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->box:Ljava/lang/String;

    const-string v3, "New_Box"

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 95
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v1, 0x7f03007e

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->setContentView(I)V

    .line 39
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->customerPrefs:Landroid/content/SharedPreferences;

    .line 41
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCarYearMakeModelString()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "yearMakeModelString":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(Ljava/lang/String;)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 43
    return-void
.end method

.method protected onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f1000b1

    .line 47
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 48
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->hideSystemUI()V

    .line 50
    const v4, 0x7f1001f3

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 51
    .local v3, "title":Landroid/widget/TextView;
    const v4, 0x7f1001f4

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 53
    .local v0, "desc":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v5, "vehicle-model"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "modelString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_mount"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "mount":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 58
    :cond_0
    const-string v4, "Installation_Short_Mount_Not_Recommended"

    invoke-static {v4}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 59
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->box:Ljava/lang/String;

    const-string v5, "Old_Box"

    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 60
    const v4, 0x7f020236

    invoke-virtual {p0, v7, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->loadImage(II)V

    .line 64
    :goto_0
    if-eqz v3, :cond_1

    .line 65
    const v4, 0x7f0804a5

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v1, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_1
    if-eqz v0, :cond_2

    .line 68
    const v4, 0x7f0804a6

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 80
    :cond_2
    :goto_1
    return-void

    .line 62
    :cond_3
    const v4, 0x7f020239

    invoke-virtual {p0, v7, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->loadImage(II)V

    goto :goto_0

    .line 71
    :cond_4
    const-string v4, "Installation_Medium_Tall_Mount_Not_Recommended"

    invoke-static {v4}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 72
    const v4, 0x7f02023c

    invoke-virtual {p0, v7, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->loadImage(II)V

    .line 73
    if-eqz v3, :cond_5

    .line 74
    const v4, 0x7f0804a3

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v1, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_5
    if-eqz v0, :cond_2

    .line 77
    const v4, 0x7f0804a4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method
