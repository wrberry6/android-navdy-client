.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;
.super Landroid/os/AsyncTask;
.source "EditCarInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->buildCarList()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$002(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->hideProgressDialog()V

    .line 114
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$100(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showManualEntry(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    const v1, 0x7f1000b1

    const v2, 0x7f0201e0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->loadImage(II)V

    .line 103
    return-void
.end method
