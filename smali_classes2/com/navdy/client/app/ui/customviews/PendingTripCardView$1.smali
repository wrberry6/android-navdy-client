.class Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;
.super Ljava/lang/Object;
.source "PendingTripCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->setUpClickListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

.field final synthetic val$pendingRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    iput-object p2, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->val$pendingRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-static {}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onClick, starting details activity"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-static {v0}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$100(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->val$pendingRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-static {v1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$100(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->startDetailsActivityForResult(Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V

    .line 55
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$102(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;Landroid/content/Context;)Landroid/content/Context;

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->val$pendingRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$1;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-static {v1}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$200(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->startDetailsActivity(Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V

    goto :goto_0
.end method
