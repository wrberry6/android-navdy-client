.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RoutesCalculationErrorEvent"
.end annotation


# instance fields
.field final cancelHandle:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "cancelHandle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 118
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;->cancelHandle:Ljava/lang/String;

    .line 119
    return-void
.end method
