.class public Lcom/navdy/client/app/framework/util/ContactObserver;
.super Landroid/database/ContentObserver;
.source "ContactObserver.java"


# static fields
.field private static final CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL:J

.field private static final REFRESH_DELAY:J

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private contactServiceHandler:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

.field private handler:Landroid/os/Handler;

.field private refreshContacts:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/ContactObserver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactObserver;->logger:Lcom/navdy/service/library/log/Logger;

    .line 23
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/ContactObserver;->REFRESH_DELAY:J

    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/ContactObserver;->CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL:J

    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Landroid/os/Handler;)V
    .locals 5
    .param p1, "contactServiceHandler"    # Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 43
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 29
    new-instance v2, Lcom/navdy/client/app/framework/util/ContactObserver$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/util/ContactObserver$1;-><init>(Lcom/navdy/client/app/framework/util/ContactObserver;)V

    iput-object v2, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->refreshContacts:Ljava/lang/Runnable;

    .line 45
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->handler:Landroid/os/Handler;

    .line 46
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->contactServiceHandler:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    .line 48
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveContactsPermission()Z

    move-result v1

    .line 49
    .local v1, "hasContactPermission":Z
    sget-object v2, Lcom/navdy/client/app/framework/util/ContactObserver;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init hasContactPermission="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 52
    if-nez v1, :cond_0

    .line 53
    new-instance v0, Lcom/navdy/client/app/framework/util/ContactObserver$2;

    invoke-direct {v0, p0, p2}, Lcom/navdy/client/app/framework/util/ContactObserver$2;-><init>(Lcom/navdy/client/app/framework/util/ContactObserver;Landroid/os/Handler;)V

    .line 66
    .local v0, "checkContactsPermissionGained":Ljava/lang/Runnable;
    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 68
    .end local v0    # "checkContactsPermissionGained":Ljava/lang/Runnable;
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactObserver;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/util/ContactObserver;)Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/ContactObserver;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->contactServiceHandler:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/util/ContactObserver;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/ContactObserver;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->refreshContacts:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300()J
    .locals 2

    .prologue
    .line 19
    sget-wide v0, Lcom/navdy/client/app/framework/util/ContactObserver;->CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL:J

    return-wide v0
.end method

.method private refreshContactsWithDebounce()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->refreshContacts:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactObserver;->refreshContacts:Ljava/lang/Runnable;

    sget-wide v2, Lcom/navdy/client/app/framework/util/ContactObserver;->REFRESH_DELAY:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 82
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 73
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactObserver;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onChange to Contacts"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/ContactObserver;->refreshContactsWithDebounce()V

    .line 75
    return-void
.end method
