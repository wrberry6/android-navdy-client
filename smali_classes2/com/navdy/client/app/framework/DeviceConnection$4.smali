.class Lcom/navdy/client/app/framework/DeviceConnection$4;
.super Ljava/lang/Object;
.source "DeviceConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/DeviceConnection;->onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/DeviceConnection;

.field final synthetic val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

.field final synthetic val$device:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/DeviceConnection;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/navdy/client/app/framework/DeviceConnection$4;->this$0:Lcom/navdy/client/app/framework/DeviceConnection;

    iput-object p2, p0, Lcom/navdy/client/app/framework/DeviceConnection$4;->val$device:Lcom/navdy/service/library/device/RemoteDevice;

    iput-object p3, p0, Lcom/navdy/client/app/framework/DeviceConnection$4;->val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 217
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection$4;->val$device:Lcom/navdy/service/library/device/RemoteDevice;

    iget-object v2, p0, Lcom/navdy/client/app/framework/DeviceConnection$4;->val$cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/AppInstance;->onDeviceDisconnectedFirstResponder(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    .line 218
    return-void
.end method
