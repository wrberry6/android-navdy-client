.class Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

.field final synthetic val$latitude:D

.field final synthetic val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

.field final synthetic val$longitude:D


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    iput-wide p2, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$latitude:D

    iput-wide p4, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$longitude:D

    iput-object p6, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->HERE_INTERNAL_ERROR:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;->onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V

    .line 81
    return-void
.end method

.method public onInit()V
    .locals 8

    .prologue
    .line 69
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-static {v3}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$000(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 71
    .local v1, "location":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v1, :cond_0

    .line 72
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 73
    .local v0, "from":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-wide v4, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$latitude:D

    iget-wide v6, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$longitude:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 74
    .local v2, "to":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    iget-object v4, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    invoke-static {v3, v0, v2, v4}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$100(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    .line 76
    .end local v0    # "from":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "to":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    return-void
.end method
