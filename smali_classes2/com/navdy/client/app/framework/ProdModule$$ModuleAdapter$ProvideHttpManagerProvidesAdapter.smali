.class public final Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideHttpManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/service/library/network/http/IHttpManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/service/library/network/http/IHttpManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/client/app/framework/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/framework/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/client/app/framework/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 46
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    const-string v1, "com.navdy.client.app.framework.ProdModule"

    const-string v2, "provideHttpManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    .line 48
    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;->setLibrary(Z)V

    .line 49
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/service/library/network/http/IHttpManager;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/ProdModule;->provideHttpManager()Lcom/navdy/service/library/network/http/IHttpManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;->get()Lcom/navdy/service/library/network/http/IHttpManager;

    move-result-object v0

    return-object v0
.end method
