.class Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/location/NavdyLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 128
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$100(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    monitor-exit v1

    .line 145
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$200(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update new location from locationListener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :try_start_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$402(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Landroid/location/Location;

    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$400(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->updateLastKnownCountryCode(Landroid/location/Location;)V

    .line 139
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 141
    :try_start_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$500(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)V

    .line 142
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$600(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)V

    .line 144
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 139
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 154
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 151
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 148
    return-void
.end method
