.class public Lcom/navdy/client/app/ui/settings/SettingsConstants;
.super Ljava/lang/Object;
.source "SettingsConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;
    }
.end annotation


# static fields
.field public static final AUDIO_ALL_AUDIO:Ljava/lang/String; = "audio_all_audio"

.field public static final AUDIO_ALL_AUDIO_DEFAULT:Z = true

.field public static final AUDIO_BT_HFP_VOLUME:Ljava/lang/String; = "audio_bt_HFP_volume"

.field public static final AUDIO_BT_HFP_VOLUME_DEFAULT:I = 0x5

.field public static final AUDIO_CAMERA_WARNINGS:Ljava/lang/String; = "audio_camera_warnings"

.field public static final AUDIO_CAMERA_WARNINGS_DEFAULT:Z = true

.field public static final AUDIO_HFP_DELAY_LEVEL:Ljava/lang/String; = "audio_hfp_delay_level"

.field public static final AUDIO_HFP_DELAY_LEVEL_DEFAULT:I = 0x1

.field public static final AUDIO_HFP_DELAY_MEASURED:Ljava/lang/String; = "audio_hfp_delay_measured"

.field public static final AUDIO_HUD_VOLUME:Ljava/lang/String; = "audio_hud_volume"

.field public static final AUDIO_HUD_VOLUME_DEFAULT:I = 0x64

.field public static final AUDIO_MENU_FEEDBACK:Ljava/lang/String; = "audio_menu_feedback"

.field public static final AUDIO_MENU_FEEDBACK_DEFAULT:Z = true

.field public static final AUDIO_NOTIFICATIONS:Ljava/lang/String; = "audio_notifications"

.field public static final AUDIO_NOTIFICATIONS_DEFAULT:Z = true

.field public static final AUDIO_OUTPUT_PREFERENCE:Ljava/lang/String; = "audio_output_preference"

.field public static final AUDIO_OUTPUT_PREFERENCE_DEFAULT:I

.field public static final AUDIO_PHONE_VOLUME:Ljava/lang/String; = "audio_phone_volume"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUDIO_PHONE_VOLUME_DEFAULT:I = 0x4b
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUDIO_PLAY_WELCOME_MESSAGE:Ljava/lang/String; = "audio_play_welcome"

.field public static final AUDIO_PLAY_WELCOME_MESSAGE_DEFAULT:Z = false

.field public static final AUDIO_SERIAL_NUM:Ljava/lang/String; = "audio_serial_number"

.field public static final AUDIO_SPEED_WARNINGS:Ljava/lang/String; = "audio_speed_warnings"

.field public static final AUDIO_SPEED_WARNINGS_DEFAULT:Z = false

.field public static final AUDIO_TTS_VOLUME:Ljava/lang/String; = "audio_tts_volume"

.field public static final AUDIO_TTS_VOLUME_DEFAULT:F = 0.6f

.field public static final AUDIO_TTS_VOLUME_HIGH:F = 1.0f

.field public static final AUDIO_TTS_VOLUME_LOW:F = 0.3f

.field public static final AUDIO_TTS_VOLUME_NORMAL:F = 0.6f

.field public static final AUDIO_TURN_BY_TURN_INSTRUCTIONS:Ljava/lang/String; = "audio_turn_by_turn_instructions"

.field public static final AUDIO_TURN_BY_TURN_INSTRUCTIONS_DEFAULT:Z = true

.field public static final AUDIO_VOICE:Ljava/lang/String; = "audio_voice"

.field public static final AUDIO_VOICE_DEFAULT:Ljava/lang/String; = "en-US"

.field public static final AUTO_ATTACH_LOGS_DEFAULT:Z = false

.field public static final AUTO_ATTACH_LOGS_ENABLED:Ljava/lang/String; = "attach_logs"

.field public static final BOX:Ljava/lang/String; = "box"

.field public static final BOX_DEFAULT:Ljava/lang/String; = "Old_Box"

.field public static final CALENDARS_ENABLED:Ljava/lang/String; = "calendars_enabled"

.field public static final CALENDARS_ENABLED_DEFAULT:Z = true

.field public static final CALENDAR_PREFIX:Ljava/lang/String; = "calendar_"

.field public static final CURRENT_SHARED_PREF_VERSION:I = 0x1

.field public static final DIAL_LONG_PRESS_ACTION:Ljava/lang/String; = "dial_long_press_action"

.field public static final DIAL_LONG_PRESS_ACTION_DEFAULT:I

.field public static final EXTRA_WAS_SKIPPED:Ljava/lang/String; = "extra_was_skipped"

.field public static final FEATURE_MODE:Ljava/lang/String; = "feature-mode"

.field public static final FEATURE_MODE_DEFAULT:Ljava/lang/String;

.field public static final FINISHED_APP_SETUP:Ljava/lang/String; = "finished_app_setup"

.field public static final FINISHED_INSTALL:Ljava/lang/String; = "finished_install"

.field public static final FINISHED_MARKETING:Ljava/lang/String; = "finished_marketing"

.field public static final FIRST_TIME_ON_HOMESCREEN:Ljava/lang/String; = "first_time_on_homescreen"

.field public static final FIRST_TIME_ON_HOMESCREEN_DEFAULT:Z = true

.field public static final GLANCES:Ljava/lang/String; = "glances"

.field public static final GLANCES_CALL_ENABLED_DEFAULT:Z = true

.field public static final GLANCES_ENABLED_DEFAULT:Z = false

.field public static final GLANCES_READ_ALOUD:Ljava/lang/String; = "glances_read_aloud"

.field public static final GLANCES_READ_ALOUD_DEFAULT:Z = true

.field public static final GLANCES_SERIAL_NUM:Ljava/lang/String; = "glances_serial_number"

.field public static final GLANCES_SHOW_CONTENT:Ljava/lang/String; = "glances_show_content"

.field public static final GLANCES_SHOW_CONTENT_DEFAULT:Z = false

.field public static final GLANCES_SMS_ENABLED_DEFAULT:Z = true

.field public static final GMS_IS_MISSING:Ljava/lang/String; = "gms_is_missing"

.field public static final GMS_IS_MISSING_DEFAULT:Z = false

.field public static final GMS_VERSION_NUMBER:Ljava/lang/String; = "gms_version_number"

.field public static final GMS_VERSION_NUMBER_DEFAULT:I = -0x1

.field public static final HAS_SKIPPED_INSTALL:Ljava/lang/String; = "has_skipped_install"

.field public static final HUD_AUTO_ON:Ljava/lang/String; = "hud_auto_on_enabled"

.field public static final HUD_AUTO_ON_DEFAULT:Z = true

.field public static final HUD_BLACKLIST_OVERRIDDEN:Ljava/lang/String; = "user_overrided_blacklist"

.field public static final HUD_BLACKLIST_OVERRIDDEN_DEFAULT:Z = false

.field public static final HUD_CANNED_RESPONSE_CAPABLE:Ljava/lang/String; = "hud_canned_response_capable"

.field public static final HUD_CANNED_RESPONSE_CAPABLE_DEFAULT:Z = false

.field public static final HUD_COMPACT_CAPABLE:Ljava/lang/String; = "hud_compact_capable"

.field public static final HUD_COMPACT_CAPABLE_DEFAULT:Z = false

.field public static final HUD_DIAL_LONG_PRESS_CAPABLE:Ljava/lang/String; = "hud_dial_long_press_capable"

.field public static final HUD_DIAL_LONG_PRESS_CAPABLE_DEFAULT:Z = false

.field public static final HUD_GESTURE:Ljava/lang/String; = "hud_gesture"

.field public static final HUD_GESTURE_DEFAULT:Z = true

.field public static final HUD_LOCAL_MUSIC_BROWSER_CAPABLE:Ljava/lang/String; = "hud_music_capable"

.field public static final HUD_LOCAL_MUSIC_BROWSER_CAPABLE_DEFAULT:Z = false

.field public static final HUD_OBD_ON:Ljava/lang/String; = "hud_obd_on_enabled"

.field public static final HUD_OBD_ON_DEFAULT:Ljava/lang/String;

.field public static final HUD_SEARCH_RESULT_LIST_CAPABLE:Ljava/lang/String; = "hud_search_result_list_capable"

.field public static final HUD_SEARCH_RESULT_LIST_CAPABLE_DEFAULT:Z = false

.field public static final HUD_SERIAL_NUM:Ljava/lang/String; = "hud_serial_number"

.field public static final HUD_UI_SCALING_ON:Ljava/lang/String; = "hud_ui_scaling"

.field public static final HUD_UI_SCALING_ON_DEFAULT:Z = false

.field public static final HUD_VOICE_SEARCH_CAPABLE:Ljava/lang/String; = "hud_voice_search_capable"

.field public static final HUD_VOICE_SEARCH_CAPABLE_DEFAULT:Z = false

.field public static final HUD_VOICE_USED_BEFORE:Ljava/lang/String; = "hud_voice_used_before"

.field public static final HUD_VOICE_USED_BEFORE_DEFAULT:Z = false

.field public static final LAST_BUILD_VERSION:Ljava/lang/String; = "last_build_version"

.field public static final LAST_CONFIG:Ljava/lang/String; = "last_config"

.field public static final LAST_CONFIG_DEFAULT:Ljava/lang/String; = ""

.field public static final LAST_HUD_UUID:Ljava/lang/String; = "last_hud_uuid"

.field public static final LAST_HUD_UUID_DEFAULT:Ljava/lang/String; = "UNKNOWN"

.field public static final LAST_MEDIA_APP:Ljava/lang/String; = "last_media_app"

.field public static final LIMIT_CELLULAR_DATA:Ljava/lang/String; = "limit_cellular_data"

.field public static final LIMIT_CELLULAR_DATA_DEFAULT:Z = false

.field public static final MESSAGING_HAS_SEEN_CAPABLE_HUD:Ljava/lang/String; = "messaging_has_seen_capable_hud"

.field public static final MESSAGING_HAS_SEEN_CAPABLE_HUD_DEFAULT:Z = false

.field public static final MESSAGING_REPLIES:Ljava/lang/String; = "messaging_replies"

.field public static final MESSAGING_SERIAL_NUM:Ljava/lang/String; = "nav_serial_number"

.field public static final MOUNT_TYPE:Ljava/lang/String; = "mount_type"

.field public static final NAVIGATION_AUTO_RECALC:Ljava/lang/String; = "nav_auto_recalc"

.field public static final NAVIGATION_AUTO_RECALC_DEFAULT:Z = false

.field public static final NAVIGATION_AUTO_TRAINS:Ljava/lang/String; = "nav_auto_trains"

.field public static final NAVIGATION_AUTO_TRAINS_DEFAULT:Z = true

.field public static final NAVIGATION_FERRIES:Ljava/lang/String; = "nav_ferries"

.field public static final NAVIGATION_FERRIES_DEFAULT:Z = true

.field public static final NAVIGATION_HIGHWAYS:Ljava/lang/String; = "nav_highways"

.field public static final NAVIGATION_HIGHWAYS_DEFAULT:Z = true

.field public static final NAVIGATION_ROUTE_CALCULATION:Ljava/lang/String; = "nav_route_calculation"

.field public static final NAVIGATION_ROUTE_CALCULATION_DEFAULT:Z = false

.field public static final NAVIGATION_SERIAL_NUM:Ljava/lang/String; = "nav_serial_number"

.field public static final NAVIGATION_TOLL_ROADS:Ljava/lang/String; = "nav_toll_roads"

.field public static final NAVIGATION_TOLL_ROADS_DEFAULT:Z = true

.field public static final NAVIGATION_TUNNELS:Ljava/lang/String; = "nav_tunnels"

.field public static final NAVIGATION_TUNNELS_DEFAULT:Z = true

.field public static final NAVIGATION_UNPAVED_ROADS:Ljava/lang/String; = "nav_unpaved_roads"

.field public static final NAVIGATION_UNPAVED_ROADS_DEFAULT:Z = true

.field public static final NB_CONFIG:Ljava/lang/String; = "nb_config"

.field public static final NB_CONFIG_DEFAULT:I = 0x0

.field public static final NEW_BOX:Ljava/lang/String; = "New_Box"

.field public static final NEW_BOX_PLUS_MOUNTS:Ljava/lang/String; = "New_Box_Plus_Mounts"

.field public static final OLD_BOX:Ljava/lang/String; = "Old_Box"

.field public static final OTA_AUTO_DOWNLOAD:Ljava/lang/String; = "ota_auto_download"

.field public static final OTA_BUILD_TYPE_DEFAULT:Ljava/lang/String;

.field public static final OTA_DESCRIPTION:Ljava/lang/String; = "OTA_DESCRIPTION"

.field public static final OTA_DESCRIPTION_DEFAULT:Ljava/lang/String; = ""

.field public static final OTA_FORCE_FULL_UPDATE:Ljava/lang/String; = "FORCE_FULL_UPDATE"

.field public static final OTA_FORCE_FULL_UPDATE_DEFAULT:Z = false

.field public static final OTA_FROM_VERSION:Ljava/lang/String; = "OTA_FROM_VERSION"

.field public static final OTA_FROM_VERSION_DEFAULT:I = 0x0

.field public static final OTA_IS_INCREMENTAL:Ljava/lang/String; = "OTA_IS_INCREMENTAL"

.field public static final OTA_IS_INCREMENTAL_DEFAULT:Z = false

.field public static final OTA_META_DATA:Ljava/lang/String; = "OTA_META_DATA"

.field public static final OTA_SERIAL_NUM:Ljava/lang/String; = "ota_serial_number"

.field public static final OTA_SIZE:Ljava/lang/String; = "OTA_SIZE"

.field public static final OTA_SIZE_DEFAULT:J = 0x0L

.field public static final OTA_STATUS:Ljava/lang/String; = "OTA_STATUS"

.field public static final OTA_STATUS_DEFAULT:Ljava/lang/String;

.field public static final OTA_URL:Ljava/lang/String; = "OTA_URL"

.field public static final OTA_URL_DEFAULT:Ljava/lang/String; = ""

.field public static final OTA_VERSION:Ljava/lang/String; = "OTA_VERSION"

.field public static final OTA_VERSION_DEFAULT:I = -0x1

.field public static final OTA_VERSION_NAME:Ljava/lang/String; = "OTA_VERSION_NAME"

.field public static final OTA_VERSION_NAME_DEFAULT:Ljava/lang/String; = ""

.field public static final PENDING_SENSOR_DATA_EXIST:Ljava/lang/String; = "pending_sensor_data_exist"

.field public static final PENDING_TICKETS_EXIST:Ljava/lang/String; = "pending_tickets_exist"

.field public static final PERMISSIONS:Ljava/lang/String; = "permissions"

.field public static final PERMISSIONS_VERSION:Ljava/lang/String; = "permissions_version"

.field public static final PERMISSIONS_VERSION_DEFAULT:I = 0x1

.field public static final POWER_CABLE_SELECTION:Ljava/lang/String; = "power_cable_selection"

.field public static final POWER_CABLE_SELECTION_CLA:Ljava/lang/String; = "CLA"

.field public static final POWER_CABLE_SELECTION_DEFAULT:Ljava/lang/String; = "OBD"

.field public static final POWER_CABLE_SELECTION_OBD:Ljava/lang/String; = "OBD"

.field public static final RECOMMENDATIONS_ENABLED:Ljava/lang/String; = "recommendations_enabled"

.field public static final RECOMMENDATIONS_ENABLED_DEFAULT:Z = true

.field public static final RELEASE_NOTES:Ljava/lang/String; = "release_notes"

.field public static final SCREENSHOT:Ljava/lang/String; = "screenshot"

.field public static final SERIAL_NUM_DEFAULT:J = 0x0L

.field public static final SETTINGS:Ljava/lang/String; = "settings"

.field public static final SHARED_PREF_VERSION:Ljava/lang/String; = "shared_pref_version"

.field public static final SHARED_PREF_VERSION_DEFAULT:I = 0x0

.field public static final SHOULD_ASK_WHAT_MOUNT_WORKED:Ljava/lang/String; = "should_ask_what_mount_worked"

.field public static final SHOULD_ASK_WHAT_MOUNT_WORKED_DEFAULT:Z = false

.field public static final TICKETS_AWAITING_HUD_LOG_EXIST:Ljava/lang/String; = "tickets_awaiting_hud_log_exist"

.field public static final USER_ALREADY_SAW_ADD_HOME_TIP:Ljava/lang/String; = "user_already_saw_add_home_tip"

.field public static final USER_ALREADY_SAW_ADD_HOME_TIP_DEFAULT:Z = false

.field public static final USER_ALREADY_SAW_ADD_WORK_TIP:Ljava/lang/String; = "user_already_saw_add_work_tip"

.field public static final USER_ALREADY_SAW_ADD_WORK_TIP_DEFAULT:Z = false

.field public static final USER_ALREADY_SAW_GESTURE_TIP:Ljava/lang/String; = "user_already_saw_gesture_tip"

.field public static final USER_ALREADY_SAW_GESTURE_TIP_DEFAULT:Z = false

.field public static final USER_ALREADY_SAW_GOOGLE_NOW_TIP:Ljava/lang/String; = "user_already_saw_google_now_tip"

.field public static final USER_ALREADY_SAW_GOOGLE_NOW_TIP_DEFAULT:Z = false

.field public static final USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP:Ljava/lang/String; = "user_already_saw_music_tip"

.field public static final USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP_DEFAULT:Z = false

.field public static final USER_ALREADY_SAW_MICROPHONE_TIP:Ljava/lang/String; = "user_already_saw_microphone_tip"

.field public static final USER_ALREADY_SAW_MICROPHONE_TIP_DEFAULT:Z = false

.field public static final USER_ENABLED_GLANCES_ONCE_BEFORE:Ljava/lang/String; = "user_enabled_glances_once_before"

.field public static final USER_ENABLED_GLANCES_ONCE_BEFORE_DEFAULT:Z = false

.field public static final USER_TRIED_GESTURES_ONCE_BEFORE:Ljava/lang/String; = "user_tried_gestures_once_before"

.field public static final USER_TRIED_GESTURES_ONCE_BEFORE_DEFAULT:Z = false

.field public static final USER_WATCHED_THE_DEMO:Ljava/lang/String; = "user_watched_the_demo"

.field public static final USER_WATCHED_THE_DEMO_DEFAULT:Z = false

.field public static final USE_EXPERIMENTAL_HUD_VOICE_ENABLED:Ljava/lang/String; = "use_experimental_hud_voice_enabled"

.field public static final USE_EXPERIMENTAL_HUD_VOICE_ENABLED_DEFAULT:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->HUD_OBD_ON_DEFAULT:Ljava/lang/String;

    .line 80
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->FEATURE_MODE_DEFAULT:Ljava/lang/String;

    .line 118
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->ordinal()I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->AUDIO_OUTPUT_PREFERENCE_DEFAULT:I

    .line 159
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->name()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_BUILD_TYPE_DEFAULT:Ljava/lang/String;

    .line 160
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_STATUS_DEFAULT:Ljava/lang/String;

    .line 286
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->getValue()I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/settings/SettingsConstants;->DIAL_LONG_PRESS_ACTION_DEFAULT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
