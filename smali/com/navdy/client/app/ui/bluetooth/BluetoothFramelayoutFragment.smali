.class public Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;
.super Lcom/navdy/client/app/ui/base/BaseDialogFragment;
.source "BluetoothFramelayoutFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    }
.end annotation


# static fields
.field public static final EXTRA_KILL_ACTIVITY_ON_CANCEL:Ljava/lang/String; = "1"

.field public static final EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE:Ljava/lang/String; = "USER_WAS_IN_FIRST_LAUNCH"

.field private static final TIMEOUT:I = 0x3a98


# instance fields
.field public context:Landroid/content/Context;

.field private currentLayout:I

.field private deviceName:Ljava/lang/String;

.field private inflater:Landroid/view/LayoutInflater;

.field private killActivityOnCancel:Z

.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field protected mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field protected mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

.field protected mDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/devicepicker/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field protected mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

.field private pairing:Z

.field private position:I

.field private recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

.field private rootView:Landroid/widget/FrameLayout;

.field private swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private userWasInFirstLaunch:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseDialogFragment;-><init>()V

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->deviceName:Ljava/lang/String;

    .line 64
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->userWasInFirstLaunch:Z

    .line 65
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairing:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->currentLayout:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private pairingComplete(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    .line 437
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairing:Z

    .line 438
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 439
    .local v0, "pairPhoneToDisplayValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "Success"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    const-string v1, "First_Launch"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->userWasInFirstLaunch:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    const-string v1, "PairPhoneToDisplay"

    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 442
    return-void
.end method


# virtual methods
.method protected declared-synchronized addConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    .locals 6
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p2, "updateIfFound"    # Z

    .prologue
    .line 381
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/navdy/client/debug/devicepicker/Device;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/devicepicker/Device;-><init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 382
    .local v1, "newDevice":Lcom/navdy/client/debug/devicepicker/Device;
    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 383
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 384
    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/debug/devicepicker/Device;

    .line 385
    .local v2, "oldDevice":Lcom/navdy/client/debug/devicepicker/Device;
    invoke-virtual {v2, p1, p2}, Lcom/navdy/client/debug/devicepicker/Device;->add(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    .end local v2    # "oldDevice":Lcom/navdy/client/debug/devicepicker/Device;
    :goto_0
    monitor-exit p0

    return-void

    .line 387
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Added a device"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/client/debug/devicepicker/Device;->getPrettyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381
    .end local v0    # "index":I
    .end local v1    # "newDevice":Lcom/navdy/client/debug/devicepicker/Device;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected declared-synchronized addConnectionInfo(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 393
    .local p1, "connectionInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDevices: add "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 394
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 395
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->addConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 393
    .end local v0    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 397
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public appInstanceDeviceConnectionEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 2
    .param p1, "deviceConnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "appInstanceDeviceConnectionEvent"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 422
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairingComplete(Z)V

    .line 424
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->finishBluetoothPairActivity()V

    .line 425
    return-void
.end method

.method public appInstanceDeviceConnectionFailedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;)V
    .locals 3
    .param p1, "deviceConnectionFailedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 429
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appInstanceDeviceConnectionFailedEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 431
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairingComplete(Z)V

    .line 433
    const v0, 0x7f03005a

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    .line 434
    return-void
.end method

.method public declared-synchronized finishBluetoothPairActivity()V
    .locals 4

    .prologue
    .line 452
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "finishBluetoothPairActivity"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 453
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 454
    .local v0, "currentActivity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 455
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentActivity.finish(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 456
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    :cond_0
    monitor-exit p0

    return-void

    .line 452
    .end local v0    # "currentActivity":Landroid/app/Activity;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getBluetoothState()Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    .locals 1

    .prologue
    .line 304
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 305
    sget-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->NO_BLUETOOTH:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    :goto_0
    monitor-exit p0

    return-object v0

    .line 306
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 307
    sget-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->DISABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    goto :goto_0

    .line 309
    :cond_1
    sget-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->ENABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized goToSettings()V
    .locals 3

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "go to bluetooth settings"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 462
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 463
    .local v0, "intentOpenBluetoothSettings":Landroid/content/Intent;
    const-string v1, "android.settings.BLUETOOTH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    monitor-exit p0

    return-void

    .line 461
    .end local v0    # "intentOpenBluetoothSettings":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 445
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v0

    .line 446
    .local v0, "position":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick called. position is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 447
    iput v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->position:I

    .line 448
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->selectOption(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    monitor-exit p0

    return-void

    .line 445
    .end local v0    # "position":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 78
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->context:Landroid/content/Context;

    .line 79
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->context:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->setupBluetooth(Landroid/content/Context;)V

    .line 80
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 81
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 82
    const-string v1, "1"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->killActivityOnCancel:Z

    .line 83
    const-string v1, "USER_WAS_IN_FIRST_LAUNCH"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->userWasInFirstLaunch:Z

    .line 85
    :cond_0
    return-void
.end method

.method public declared-synchronized onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onCreateDialog"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 110
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 111
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    .line 112
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03005c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    .line 113
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 114
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->selectDialogBasedOnState()V

    .line 115
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 109
    .end local v0    # "builder":Landroid/support/v7/app/AlertDialog$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 90
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "onDismiss"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 92
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    if-nez v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->isInForeground()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 99
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->killActivityOnCancel:Z

    if-eqz v2, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 101
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 102
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public declared-synchronized onPause()V
    .locals 2

    .prologue
    .line 329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->stopScanning()V

    .line 331
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "removing mRemoteDeviceRegistry listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 334
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->onPause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    monitor-exit p0

    return-void

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 2

    .prologue
    .line 320
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->onResume()V

    .line 321
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "added listener to mRemoteDeviceRegistry"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 324
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->selectDialogBasedOnState()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    monitor-exit p0

    return-void

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized retryPairing(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 469
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->position:I

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->selectOption(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    monitor-exit p0

    return-void

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized selectDialogBasedOnState()V
    .locals 6

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getBluetoothState()Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    move-result-object v0

    .line 253
    .local v0, "currentState":Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    sget-object v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$9;->$SwitchMap$com$navdy$client$app$ui$bluetooth$BluetoothFramelayoutFragment$BluetoothState:[I

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 301
    :goto_0
    monitor-exit p0

    return-void

    .line 256
    :pswitch_0
    const v1, 0x7f03005d

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    .end local v0    # "currentState":Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 259
    .restart local v0    # "currentState":Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    :pswitch_1
    :try_start_2
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairing:Z

    if-eqz v1, :cond_0

    .line 260
    const v1, 0x7f03005e

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    goto :goto_0

    .line 263
    :cond_0
    const v1, 0x7f03005f

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    .line 264
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    if-eqz v1, :cond_1

    .line 265
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 267
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "selectDialogBasedOnState(): mRemoteDeviceRegistry.startScanning()"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 268
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->stopScanning()V

    .line 269
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->startScanning()V

    .line 271
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    .line 272
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "RecyclerView was not null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 273
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->updateRecyclerView(Ljava/util/ArrayList;)V

    .line 276
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 253
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized selectOption(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 400
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    if-ltz p1, :cond_0

    .line 402
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->stopScanning()V

    .line 403
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "mRemoteDeviceRegistry.stopScanning"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDevices: selectOption "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDevices size during selectOption: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 406
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/debug/devicepicker/Device;

    .line 407
    .local v1, "pickedDevice":Lcom/navdy/client/debug/devicepicker/Device;
    invoke-virtual {v1}, Lcom/navdy/client/debug/devicepicker/Device;->getPrettyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->setDeviceName(Ljava/lang/String;)V

    .line 408
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->pairing:Z

    .line 409
    const v2, 0x7f03005e

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    .line 410
    invoke-virtual {v1}, Lcom/navdy/client/debug/devicepicker/Device;->getPreferredConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    .line 411
    .local v0, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->setDefaultConnectionInfo(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 412
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->initializeDevice()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    .end local v0    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .end local v1    # "pickedDevice":Lcom/navdy/client/debug/devicepicker/Device;
    :goto_0
    monitor-exit p0

    return-void

    .line 414
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t selectOption position is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and mDevice "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "only has "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " entries"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 400
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 414
    :cond_1
    :try_start_2
    const-string v2, "is null"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized setDeviceName(Ljava/lang/String;)V
    .locals 1
    .param p1, "prettyName"    # Ljava/lang/String;

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->deviceName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setUpCancelButton()V
    .locals 3

    .prologue
    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 244
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 223
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v2, 0x7f10013b

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 225
    .local v0, "cancelButton":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 226
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->killActivityOnCancel:Z

    if-eqz v1, :cond_2

    .line 227
    new-instance v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$5;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$5;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 218
    .end local v0    # "cancelButton":Landroid/widget/Button;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 235
    .restart local v0    # "cancelButton":Landroid/widget/Button;
    :cond_2
    :try_start_2
    new-instance v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$6;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$6;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized setupBluetooth(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setupBluetooth called"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    .line 122
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDevices: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 123
    invoke-static {p1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .line 124
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 125
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRemoteDeviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getKnownConnectionInfo()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->addConnectionInfo(Ljava/util/Set;)V

    .line 126
    new-instance v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDeviceListUpdateListener:Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setupRecyclerView()V
    .locals 3

    .prologue
    .line 338
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "setupRecyclerView()"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v2, 0x7f100164

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 342
    :cond_0
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 343
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 344
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1

    .line 345
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 347
    :cond_1
    new-instance v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    .line 348
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    .line 350
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 354
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    .line 355
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v2, 0x7f100163

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 356
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x4

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 360
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v2, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$8;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$8;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :cond_3
    monitor-exit p0

    return-void

    .line 338
    .end local v0    # "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 356
    nop

    :array_0
    .array-data 4
        0x7f0f0012
        0x7f0f0058
        0x7f0f00cd
        0x7f0f00ae
    .end array-data
.end method

.method public declared-synchronized showDialog(I)V
    .locals 8
    .param p1, "res"    # I

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    .line 215
    :cond_0
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 148
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showDialog called on res: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 149
    iget v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->currentLayout:I

    if-eq v4, p1, :cond_0

    .line 152
    iget v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->currentLayout:I

    if-eqz v4, :cond_2

    .line 153
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "savedLayout: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->currentLayout:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 156
    :cond_2
    iput p1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->currentLayout:I

    .line 157
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 159
    .local v2, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 161
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    .line 163
    :cond_3
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    if-eqz v4, :cond_4

    .line 164
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->inflater:Landroid/view/LayoutInflater;

    iget-object v6, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const/4 v7, 0x0

    invoke-virtual {v5, p1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->setUpCancelButton()V

    .line 169
    packed-switch p1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 204
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v5, 0x7f10015d

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 205
    .local v3, "retryButton":Landroid/widget/Button;
    new-instance v4, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$4;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$4;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 144
    .end local v2    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v3    # "retryButton":Landroid/widget/Button;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 171
    .restart local v2    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_3
    :try_start_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v5, 0x7f100141

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    .local v0, "deviceTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 177
    .end local v0    # "deviceTextView":Landroid/widget/TextView;
    :pswitch_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->setupRecyclerView()V

    goto/16 :goto_0

    .line 180
    :pswitch_5
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v5, 0x7f10015f

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 181
    .local v1, "goToSettingsButton":Landroid/widget/Button;
    new-instance v4, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$2;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$2;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 189
    .end local v1    # "goToSettingsButton":Landroid/widget/Button;
    :pswitch_6
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->rootView:Landroid/widget/FrameLayout;

    const v5, 0x7f100163

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 190
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 194
    iget-object v4, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v5, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$3;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$3;-><init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x7f03005a
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 190
    :array_0
    .array-data 4
        0x7f0f0012
        0x7f0f0058
        0x7f0f00cd
        0x7f0f00ae
    .end array-data
.end method

.method public declared-synchronized stopRefresh()V
    .locals 2

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "swipeContainer: stopRefresh"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    monitor-exit p0

    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateRecyclerView(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/devicepicker/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/debug/devicepicker/Device;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateRecyclerView"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/devicepicker/Device;

    .line 373
    .local v0, "device":Lcom/navdy/client/debug/devicepicker/Device;
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/debug/devicepicker/Device;->getPrettyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 374
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->addDevice(Lcom/navdy/client/debug/devicepicker/Device;)V

    .line 375
    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->recyclerAdapter:Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 371
    .end local v0    # "device":Lcom/navdy/client/debug/devicepicker/Device;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 378
    :cond_1
    monitor-exit p0

    return-void
.end method
