.class public Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;
.super Landroid/app/IntentService;
.source "ActivityRecognizedCallbackService.java"


# static fields
.field public static final MINIMUM_CONFIDENCE_THRESHOLD:I = 0x32

.field private static STATE_EXPIRY_INTERVAL:J = 0x0L

.field public static final STATE_IN_VEHICLE:I = 0x1

.field public static final STATE_ON_FOOT:I = 0x2

.field public static final STATE_UNKNOWN:I = 0x0

.field public static final WALKING_ACTIVITY_CONFIDENCE_MINIMUM_THRESHOLD:I = 0x4b

.field private static currentState:I

.field private static lastState:I

.field private static lastStateUpdateTime:J

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field DEBUG_VIBRATE_PATTERN:[J

.field audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 38
    sput v0, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastState:I

    .line 40
    sput v0, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->STATE_EXPIRY_INTERVAL:J

    .line 43
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "ActivityRecognizedCallbackService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x6

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->DEBUG_VIBRATE_PATTERN:[J

    .line 47
    return-void

    .line 36
    :array_0
    .array-data 8
        0x0
        0x1f4
        0x64
        0x1f4
        0x64
        0x1f4
    .end array-data
.end method

.method private handleDetectedActivities(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/DetectedActivity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "probableActivities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/location/DetectedActivity;>;"
    const/16 v13, 0x32

    const/4 v12, 0x1

    .line 68
    const/4 v1, 0x0

    .line 69
    .local v1, "isInVehicle":Z
    const/4 v2, 0x0

    .line 70
    .local v2, "isInVehicleConfidence":I
    const/4 v3, 0x0

    .line 71
    .local v3, "isOnFoot":Z
    const/4 v4, 0x0

    .line 72
    .local v4, "isOnFootConfidence":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    .line 73
    .local v0, "activity":Lcom/google/android/gms/location/DetectedActivity;
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 85
    :goto_1
    :pswitch_0
    if-nez v1, :cond_1

    if-eqz v3, :cond_0

    .line 86
    :cond_1
    sget-object v8, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Activity "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Confidence : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getConfidence()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :pswitch_1
    const/4 v1, 0x1

    .line 76
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getConfidence()I

    move-result v2

    .line 77
    goto :goto_1

    .line 79
    :pswitch_2
    const/4 v3, 0x1

    .line 80
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->getConfidence()I

    move-result v4

    .line 81
    goto :goto_1

    .line 89
    .end local v0    # "activity":Lcom/google/android/gms/location/DetectedActivity;
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 90
    .local v6, "time":J
    sget-wide v8, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastStateUpdateTime:J

    sub-long v8, v6, v8

    sget-wide v10, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->STATE_EXPIRY_INTERVAL:J

    cmp-long v5, v8, v10

    if-lez v5, :cond_4

    .line 91
    sget-object v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Last update is expired"

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 92
    sget v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    sput v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastState:I

    .line 93
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->setCurrentState(I)V

    .line 97
    :goto_2
    if-eqz v3, :cond_6

    if-le v4, v13, :cond_6

    .line 98
    if-eqz v1, :cond_3

    if-le v4, v2, :cond_5

    .line 99
    :cond_3
    const/4 v5, 0x2

    invoke-direct {p0, v5}, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->setCurrentState(I)V

    .line 110
    :goto_3
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->getInstance()Lcom/navdy/client/app/service/DataCollectionService;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Lcom/navdy/client/app/service/DataCollectionService;->handleDetectedActivities(ZI)V

    .line 111
    return-void

    .line 95
    :cond_4
    sget v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    sput v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastState:I

    goto :goto_2

    .line 102
    :cond_5
    invoke-direct {p0, v12}, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->setCurrentState(I)V

    goto :goto_3

    .line 104
    :cond_6
    if-eqz v1, :cond_7

    if-le v2, v13, :cond_7

    .line 105
    invoke-direct {p0, v12}, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->setCurrentState(I)V

    goto :goto_3

    .line 107
    :cond_7
    sget-object v5, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Currently UNKNOWN"

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_3

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setCurrentState(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 114
    sget-object v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCurrentState "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 115
    sput p1, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    .line 116
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sput-wide v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastStateUpdateTime:J

    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "message":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 120
    :pswitch_0
    sget-object v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Currently ON_FOOT"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 121
    const-string v0, "You are walking now"

    .line 130
    :goto_1
    sget v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastState:I

    sget v3, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    if-eq v2, v3, :cond_1

    .line 134
    :cond_1
    sget v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->lastState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    sget v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->currentState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 135
    sget-object v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Transition from in vehicle to on foot, time to send accelerate shutdown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 137
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 138
    new-instance v2, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_WALKING:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->reason(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;)Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->build()Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0

    .line 124
    .end local v1    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :pswitch_1
    sget-object v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Currently IN_VEHICLE"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 125
    const-string v0, "You got into your vehicle"

    .line 126
    goto :goto_1

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 55
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->hasResult(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    sget-object v1, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onHandleIntent, Activity recognized callback has results"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 62
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->extractResult(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    .line 63
    .local v0, "result":Lcom/google/android/gms/location/ActivityRecognitionResult;
    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->getProbableActivities()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;->handleDetectedActivities(Ljava/util/List;)V

    .line 65
    .end local v0    # "result":Lcom/google/android/gms/location/ActivityRecognitionResult;
    :cond_0
    return-void
.end method
