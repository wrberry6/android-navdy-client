.class Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;
.super Ljava/lang/Object;
.source "DestinationSuggestionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handleAutoTripUpload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

.field final synthetic val$bus:Lcom/squareup/otto/Bus;

.field final synthetic val$maxTripId:J

.field final synthetic val$sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Landroid/content/SharedPreferences;JLcom/squareup/otto/Bus;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    .prologue
    .line 412
    iput-object p1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    iput-object p2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$sharedPreferences:Landroid/content/SharedPreferences;

    iput-wide p3, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$maxTripId:J

    iput-object p5, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$bus:Lcom/squareup/otto/Bus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;)V
    .locals 4
    .param p1, "uploadResult"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 415
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->SUCCESS:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    if-ne p1, v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_LAST_UPLOAD_TIME"

    .line 418
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 417
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "PREF_LAST_UPLOAD_ID"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$maxTripId:J

    .line 419
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 420
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 421
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-static {v0}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$400(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$3;->val$bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 424
    return-void
.end method
