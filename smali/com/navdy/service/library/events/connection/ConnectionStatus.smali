.class public final Lcom/navdy/service/library/events/connection/ConnectionStatus;
.super Lcom/squareup/wire/Message;
.source "ConnectionStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;,
        Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REMOTEDEVICEID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final remoteDeviceId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRING:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->DEFAULT_STATUS:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;

    .prologue
    .line 43
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;->remoteDeviceId:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;Lcom/navdy/service/library/events/connection/ConnectionStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/connection/ConnectionStatus$1;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;
    .param p2, "remoteDeviceId"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 39
    iput-object p2, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 51
    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    .line 52
    .local v0, "o":Lcom/navdy/service/library/events/connection/ConnectionStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->hashCode:I

    .line 59
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 60
    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->status:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->hashCode()I

    move-result v0

    .line 61
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->remoteDeviceId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 62
    iput v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus;->hashCode:I

    .line 64
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0
.end method
