.class public final Lcom/navdy/service/library/events/places/AutoCompleteResponse;
.super Lcom/squareup/wire/Message;
.source "AutoCompleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_PARTIALSEARCH:Ljava/lang/String; = ""

.field public static final DEFAULT_RESULTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final partialSearch:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final results:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->DEFAULT_RESULTS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;

    .prologue
    .line 56
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->partialSearch:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->results:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 58
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;Lcom/navdy/service/library/events/places/AutoCompleteResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/AutoCompleteResponse$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;-><init>(Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "partialSearch"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p3, "statusDetail"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p4, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 51
    iput-object p3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    .line 52
    invoke-static {p4}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    .line 53
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 64
    check-cast v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    .line 65
    .local v0, "o":Lcom/navdy/service/library/events/places/AutoCompleteResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 66
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    .line 67
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    .line 68
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 73
    iget v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->hashCode:I

    .line 74
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 75
    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 76
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 77
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 78
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v2, v1

    .line 79
    iput v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->hashCode:I

    .line 81
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 75
    goto :goto_0

    :cond_3
    move v2, v1

    .line 76
    goto :goto_1

    .line 78
    :cond_4
    const/4 v1, 0x1

    goto :goto_2
.end method
