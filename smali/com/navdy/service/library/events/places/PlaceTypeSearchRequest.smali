.class public final Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;
.super Lcom/squareup/wire/Message;
.source "PlaceTypeSearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_PLACE_TYPE:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final DEFAULT_REQUEST_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final place_type:Lcom/navdy/service/library/events/places/PlaceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final request_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->DEFAULT_PLACE_TYPE:Lcom/navdy/service/library/events/places/PlaceType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;

    .prologue
    .line 39
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;->request_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 40
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;-><init>(Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/places/PlaceType;)V
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;
    .param p2, "place_type"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 47
    check-cast v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;

    .line 48
    .local v0, "o":Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 49
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->hashCode:I

    .line 55
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 56
    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 57
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 58
    iput v0, p0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->hashCode:I

    .line 60
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 56
    goto :goto_0
.end method
