.class public final Lcom/navdy/service/library/events/places/PlacesSearchResponse;
.super Lcom/squareup/wire/Message;
.source "PlacesSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field public static final DEFAULT_RESULTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SEARCHQUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final results:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/places/PlacesSearchResult;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field public final searchQuery:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->DEFAULT_RESULTS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    .prologue
    .line 66
    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results:Ljava/util/List;

    iget-object v5, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->requestId:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;Lcom/navdy/service/library/events/places/PlacesSearchResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "searchQuery"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p3, "statusDetail"    # Ljava/lang/String;
    .param p5, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p4, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 60
    iput-object p3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    .line 61
    invoke-static {p4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    .line 62
    iput-object p5, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    .line 63
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 74
    check-cast v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    .line 75
    .local v0, "o":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    iget v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->hashCode:I

    .line 85
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 86
    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 87
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 88
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 91
    iput v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->hashCode:I

    .line 93
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_0

    :cond_3
    move v2, v1

    .line 87
    goto :goto_1

    :cond_4
    move v2, v1

    .line 88
    goto :goto_2

    .line 89
    :cond_5
    const/4 v2, 0x1

    goto :goto_3
.end method
