.class public final Lcom/navdy/service/library/events/audio/MusicCharacterMap;
.super Lcom/squareup/wire/Message;
.source "MusicCharacterMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHARACTER:Ljava/lang/String; = ""

.field public static final DEFAULT_OFFSET:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final character:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final offset:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->DEFAULT_OFFSET:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->character:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->offset:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;Lcom/navdy/service/library/events/audio/MusicCharacterMap$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCharacterMap$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;-><init>(Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "character"    # Ljava/lang/String;
    .param p2, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    .line 44
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCharacterMap;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    .line 45
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->hashCode:I

    .line 51
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 52
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 53
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 54
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->hashCode:I

    .line 56
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 52
    goto :goto_0
.end method
