.class public final Lcom/navdy/service/library/events/location/LatLong;
.super Lcom/squareup/wire/Message;
.source "LatLong.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/location/LatLong$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LATITUDE:Ljava/lang/Double;

.field public static final DEFAULT_LONGITUDE:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final latitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final longitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 17
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/LatLong;->DEFAULT_LATITUDE:Ljava/lang/Double;

    .line 18
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/LatLong;->DEFAULT_LONGITUDE:Ljava/lang/Double;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/location/LatLong$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/location/LatLong$Builder;

    .prologue
    .line 32
    iget-object v0, p1, Lcom/navdy/service/library/events/location/LatLong$Builder;->latitude:Ljava/lang/Double;

    iget-object v1, p1, Lcom/navdy/service/library/events/location/LatLong$Builder;->longitude:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/location/LatLong;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/location/LatLong$Builder;Lcom/navdy/service/library/events/location/LatLong$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/location/LatLong$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/location/LatLong$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Lcom/navdy/service/library/events/location/LatLong$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 0
    .param p1, "latitude"    # Ljava/lang/Double;
    .param p2, "longitude"    # Ljava/lang/Double;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 28
    iput-object p2, p0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-ne p1, p0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v1

    .line 39
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/location/LatLong;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 40
    check-cast v0, Lcom/navdy/service/library/events/location/LatLong;

    .line 41
    .local v0, "o":Lcom/navdy/service/library/events/location/LatLong;
    iget-object v3, p0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/LatLong;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 42
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/LatLong;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    iget v0, p0, Lcom/navdy/service/library/events/location/LatLong;->hashCode:I

    .line 48
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 49
    iget-object v2, p0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v0

    .line 50
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 51
    iput v0, p0, Lcom/navdy/service/library/events/location/LatLong;->hashCode:I

    .line 53
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 49
    goto :goto_0
.end method
