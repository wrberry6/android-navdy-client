.class public final Lcom/navdy/service/library/events/notification/NotificationAction;
.super Lcom/squareup/wire/Message;
.source "NotificationAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTIONID:Ljava/lang/Integer;

.field public static final DEFAULT_HANDLER:Ljava/lang/String; = ""

.field public static final DEFAULT_ICONRESOURCE:Ljava/lang/Integer;

.field public static final DEFAULT_ICONRESOURCEFOCUSED:Ljava/lang/Integer;

.field public static final DEFAULT_ID:Ljava/lang/Integer;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_LABELID:Ljava/lang/Integer;

.field public static final DEFAULT_NOTIFICATIONID:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final actionId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final handler:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final iconResource:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final iconResourceFocused:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final labelId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final notificationId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_ID:Ljava/lang/Integer;

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_NOTIFICATIONID:Ljava/lang/Integer;

    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_ACTIONID:Ljava/lang/Integer;

    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_LABELID:Ljava/lang/Integer;

    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_ICONRESOURCE:Ljava/lang/Integer;

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationAction;->DEFAULT_ICONRESOURCEFOCUSED:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/notification/NotificationAction$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/notification/NotificationAction$Builder;

    .prologue
    .line 94
    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->handler:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->notificationId:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->actionId:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->labelId:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->label:Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResource:Ljava/lang/Integer;

    iget-object v8, p1, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResourceFocused:Ljava/lang/Integer;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/notification/NotificationAction;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 95
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationAction;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 96
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/notification/NotificationAction$Builder;Lcom/navdy/service/library/events/notification/NotificationAction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/notification/NotificationAction$1;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationAction;-><init>(Lcom/navdy/service/library/events/notification/NotificationAction$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "handler"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/Integer;
    .param p3, "notificationId"    # Ljava/lang/Integer;
    .param p4, "actionId"    # Ljava/lang/Integer;
    .param p5, "labelId"    # Ljava/lang/Integer;
    .param p6, "label"    # Ljava/lang/String;
    .param p7, "iconResource"    # Ljava/lang/Integer;
    .param p8, "iconResourceFocused"    # Ljava/lang/Integer;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    .line 85
    iput-object p3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    .line 86
    iput-object p4, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    .line 87
    iput-object p5, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    .line 88
    iput-object p6, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    .line 89
    iput-object p7, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    .line 90
    iput-object p8, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    .line 91
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    if-ne p1, p0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v1

    .line 101
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/notification/NotificationAction;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 102
    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationAction;

    .line 103
    .local v0, "o":Lcom/navdy/service/library/events/notification/NotificationAction;
    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    .line 104
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    .line 105
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    .line 106
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    .line 107
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    .line 108
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    .line 109
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    .line 110
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationAction;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 115
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->hashCode:I

    .line 116
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 117
    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 118
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 119
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 120
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 121
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 122
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 123
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 124
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 125
    iput v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction;->hashCode:I

    .line 127
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 117
    goto :goto_0

    :cond_3
    move v2, v1

    .line 118
    goto :goto_1

    :cond_4
    move v2, v1

    .line 119
    goto :goto_2

    :cond_5
    move v2, v1

    .line 120
    goto :goto_3

    :cond_6
    move v2, v1

    .line 121
    goto :goto_4

    :cond_7
    move v2, v1

    .line 122
    goto :goto_5

    :cond_8
    move v2, v1

    .line 123
    goto :goto_6
.end method
