.class public final Lcom/navdy/service/library/events/file/FileListResponse;
.super Lcom/squareup/wire/Message;
.source "FileListResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FILES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FILE_TYPE:Lcom/navdy/service/library/events/file/FileType;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUS_DETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final file_type:Lcom/navdy/service/library/events/file/FileType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final files:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status_detail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/file/FileListResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 21
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    sput-object v0, Lcom/navdy/service/library/events/file/FileListResponse;->DEFAULT_FILE_TYPE:Lcom/navdy/service/library/events/file/FileType;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileListResponse;->DEFAULT_FILES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/file/FileType;Ljava/util/List;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "status_detail"    # Ljava/lang/String;
    .param p3, "file_type"    # Lcom/navdy/service/library/events/file/FileType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/file/FileType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p4, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 47
    iput-object p2, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    .line 49
    invoke-static {p4}, Lcom/navdy/service/library/events/file/FileListResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    .line 50
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileListResponse$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileListResponse$Builder;

    .prologue
    .line 53
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->status_detail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->file_type:Lcom/navdy/service/library/events/file/FileType;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileListResponse$Builder;->files:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/file/FileListResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/file/FileType;Ljava/util/List;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileListResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileListResponse$Builder;Lcom/navdy/service/library/events/file/FileListResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileListResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileListResponse$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileListResponse;-><init>(Lcom/navdy/service/library/events/file/FileListResponse$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/navdy/service/library/events/file/FileListResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/FileListResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 61
    check-cast v0, Lcom/navdy/service/library/events/file/FileListResponse;

    .line 62
    .local v0, "o":Lcom/navdy/service/library/events/file/FileListResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileListResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileListResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileListResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileListResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    iget v0, p0, Lcom/navdy/service/library/events/file/FileListResponse;->hashCode:I

    .line 71
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 72
    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 73
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileListResponse;->status_detail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 74
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileListResponse;->file_type:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 75
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileListResponse;->files:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v2, v1

    .line 76
    iput v0, p0, Lcom/navdy/service/library/events/file/FileListResponse;->hashCode:I

    .line 78
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 72
    goto :goto_0

    :cond_3
    move v2, v1

    .line 73
    goto :goto_1

    .line 75
    :cond_4
    const/4 v1, 0x1

    goto :goto_2
.end method
