.class public final Lcom/navdy/service/library/events/input/GestureEvent;
.super Lcom/squareup/wire/Message;
.source "GestureEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/input/GestureEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_GESTURE:Lcom/navdy/service/library/events/input/Gesture;

.field public static final DEFAULT_X:Ljava/lang/Integer;

.field public static final DEFAULT_Y:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final gesture:Lcom/navdy/service/library/events/input/Gesture;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final x:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final y:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    sget-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    sput-object v0, Lcom/navdy/service/library/events/input/GestureEvent;->DEFAULT_GESTURE:Lcom/navdy/service/library/events/input/Gesture;

    .line 16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/input/GestureEvent;->DEFAULT_X:Ljava/lang/Integer;

    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/input/GestureEvent;->DEFAULT_Y:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "gesture"    # Lcom/navdy/service/library/events/input/Gesture;
    .param p2, "x"    # Ljava/lang/Integer;
    .param p3, "y"    # Ljava/lang/Integer;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 30
    iput-object p2, p0, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    .line 31
    iput-object p3, p0, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    .line 32
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/input/GestureEvent$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/input/GestureEvent$Builder;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    iget-object v1, p1, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->x:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->y:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/input/GestureEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/input/GestureEvent$Builder;Lcom/navdy/service/library/events/input/GestureEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/input/GestureEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/input/GestureEvent$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/GestureEvent$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/navdy/service/library/events/input/GestureEvent;

    .line 44
    .local v0, "o":Lcom/navdy/service/library/events/input/GestureEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    iget-object v4, v0, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/input/GestureEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    .line 45
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/input/GestureEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    .line 46
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/input/GestureEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51
    iget v0, p0, Lcom/navdy/service/library/events/input/GestureEvent;->hashCode:I

    .line 52
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 53
    iget-object v2, p0, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/input/Gesture;->hashCode()I

    move-result v0

    .line 54
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 55
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 56
    iput v0, p0, Lcom/navdy/service/library/events/input/GestureEvent;->hashCode:I

    .line 58
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 53
    goto :goto_0

    :cond_3
    move v2, v1

    .line 54
    goto :goto_1
.end method
