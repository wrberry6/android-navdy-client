.class public Lcom/navdy/service/library/log/FastPrintWriter;
.super Ljava/io/PrintWriter;
.source "FastPrintWriter.java"


# static fields
.field private static sDummyWriter:Ljava/io/Writer;


# instance fields
.field private final mAutoFlush:Z

.field private final mBufferLen:I

.field private final mBytes:Ljava/nio/ByteBuffer;

.field private mCharset:Ljava/nio/charset/CharsetEncoder;

.field private mIoError:Z

.field private final mOutputStream:Ljava/io/OutputStream;

.field private mPos:I

.field private final mPrinter:Landroid/util/Printer;

.field private final mSeparator:Ljava/lang/String;

.field private final mText:[C

.field private final mWriter:Ljava/io/Writer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/FastPrintWriter$1;

    invoke-direct {v0}, Lcom/navdy/service/library/log/FastPrintWriter$1;-><init>()V

    sput-object v0, Lcom/navdy/service/library/log/FastPrintWriter;->sDummyWriter:Ljava/io/Writer;

    return-void
.end method

.method public constructor <init>(Landroid/util/Printer;)V
    .locals 1
    .param p1, "pr"    # Landroid/util/Printer;

    .prologue
    .line 207
    const/16 v0, 0x200

    invoke-direct {p0, p1, v0}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Landroid/util/Printer;I)V

    .line 208
    return-void
.end method

.method public constructor <init>(Landroid/util/Printer;I)V
    .locals 3
    .param p1, "pr"    # Landroid/util/Printer;
    .param p2, "bufferLen"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 224
    sget-object v0, Lcom/navdy/service/library/log/FastPrintWriter;->sDummyWriter:Ljava/io/Writer;

    invoke-direct {p0, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    .line 225
    if-nez p1, :cond_0

    .line 226
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "pr is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_0
    iput p2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    .line 229
    new-array v0, p2, [C

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    .line 230
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    .line 231
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    .line 232
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    .line 233
    iput-object p1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPrinter:Landroid/util/Printer;

    .line 234
    iput-boolean v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mAutoFlush:Z

    .line 235
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 236
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    .line 240
    :goto_0
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->initDefaultEncoder()V

    .line 241
    return-void

    .line 238
    :cond_1
    const-string v0, "\n"

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 65
    const/4 v0, 0x0

    const/16 v1, 0x2000

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/OutputStream;ZI)V

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Z)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "autoFlush"    # Z

    .prologue
    .line 83
    const/16 v0, 0x2000

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/OutputStream;ZI)V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;ZI)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "autoFlush"    # Z
    .param p3, "bufferLen"    # I

    .prologue
    const/4 v1, 0x0

    .line 104
    sget-object v0, Lcom/navdy/service/library/log/FastPrintWriter;->sDummyWriter:Ljava/io/Writer;

    invoke-direct {p0, v0, p2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    .line 105
    if-nez p1, :cond_0

    .line 106
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "out is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    iput p3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    .line 109
    new-array v0, p3, [C

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    .line 110
    iget v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    .line 112
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    .line 113
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPrinter:Landroid/util/Printer;

    .line 114
    iput-boolean p2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mAutoFlush:Z

    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 116
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    .line 120
    :goto_0
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->initDefaultEncoder()V

    .line 121
    return-void

    .line 118
    :cond_1
    const-string v0, "\n"

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2
    .param p1, "wr"    # Ljava/io/Writer;

    .prologue
    .line 138
    const/4 v0, 0x0

    const/16 v1, 0x2000

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    .line 139
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Z)V
    .locals 1
    .param p1, "wr"    # Ljava/io/Writer;
    .param p2, "autoFlush"    # Z

    .prologue
    .line 156
    const/16 v0, 0x2000

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    .line 157
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;ZI)V
    .locals 2
    .param p1, "wr"    # Ljava/io/Writer;
    .param p2, "autoFlush"    # Z
    .param p3, "bufferLen"    # I

    .prologue
    const/4 v1, 0x0

    .line 177
    sget-object v0, Lcom/navdy/service/library/log/FastPrintWriter;->sDummyWriter:Ljava/io/Writer;

    invoke-direct {p0, v0, p2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    .line 178
    if-nez p1, :cond_0

    .line 179
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "wr is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    iput p3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    .line 182
    new-array v0, p3, [C

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    .line 183
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    .line 184
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    .line 185
    iput-object p1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    .line 186
    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPrinter:Landroid/util/Printer;

    .line 187
    iput-boolean p2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mAutoFlush:Z

    .line 188
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 189
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    .line 193
    :goto_0
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->initDefaultEncoder()V

    .line 194
    return-void

    .line 191
    :cond_1
    const-string v0, "\n"

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    goto :goto_0
.end method

.method private appendLocked(C)V
    .locals 2
    .param p1, "c"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    iget v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 295
    .local v0, "pos":I
    iget v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 296
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V

    .line 297
    iget v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 299
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    aput-char p1, v1, v0

    .line 300
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 301
    return-void
.end method

.method private appendLocked(Ljava/lang/String;II)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "i"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 304
    iget v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    .line 305
    .local v0, "BUFFER_LEN":I
    if-le p3, v0, :cond_1

    .line 306
    add-int v1, p2, p3

    .line 307
    .local v1, "end":I
    :goto_0
    if-ge p2, v1, :cond_3

    .line 308
    add-int v2, p2, v0

    .line 309
    .local v2, "next":I
    if-ge v2, v1, :cond_0

    move v4, v0

    :goto_1
    invoke-direct {p0, p1, p2, v4}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(Ljava/lang/String;II)V

    .line 310
    move p2, v2

    .line 311
    goto :goto_0

    .line 309
    :cond_0
    sub-int v4, v1, p2

    goto :goto_1

    .line 314
    .end local v1    # "end":I
    .end local v2    # "next":I
    :cond_1
    iget v3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 315
    .local v3, "pos":I
    add-int v4, v3, p3

    if-le v4, v0, :cond_2

    .line 316
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V

    .line 317
    iget v3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 319
    :cond_2
    add-int v4, p2, p3

    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    invoke-virtual {p1, p2, v4, v5, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 320
    add-int v4, v3, p3

    iput v4, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 321
    .end local v3    # "pos":I
    :cond_3
    return-void
.end method

.method private appendLocked([CII)V
    .locals 5
    .param p1, "buf"    # [C
    .param p2, "i"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    iget v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBufferLen:I

    .line 325
    .local v0, "BUFFER_LEN":I
    if-le p3, v0, :cond_1

    .line 326
    add-int v1, p2, p3

    .line 327
    .local v1, "end":I
    :goto_0
    if-ge p2, v1, :cond_3

    .line 328
    add-int v2, p2, v0

    .line 329
    .local v2, "next":I
    if-ge v2, v1, :cond_0

    move v4, v0

    :goto_1
    invoke-direct {p0, p1, p2, v4}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked([CII)V

    .line 330
    move p2, v2

    .line 331
    goto :goto_0

    .line 329
    :cond_0
    sub-int v4, v1, p2

    goto :goto_1

    .line 334
    .end local v1    # "end":I
    .end local v2    # "next":I
    :cond_1
    iget v3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 335
    .local v3, "pos":I
    add-int v4, v3, p3

    if-le v4, v0, :cond_2

    .line 336
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V

    .line 337
    iget v3, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 339
    :cond_2
    iget-object v4, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    invoke-static {p1, p2, v4, v3, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 340
    add-int v4, v3, p3

    iput v4, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 341
    .end local v3    # "pos":I
    :cond_3
    return-void
.end method

.method private flushBytesLocked()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .local v0, "position":I
    if-lez v0, :cond_0

    .line 346
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 347
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 348
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 350
    :cond_0
    return-void
.end method

.method private flushLocked()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 354
    iget v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    if-lez v5, :cond_2

    .line 355
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v5, :cond_3

    .line 356
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    iget v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    invoke-static {v5, v9, v6}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 357
    .local v0, "charBuffer":Ljava/nio/CharBuffer;
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    iget-object v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0, v6, v7}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v3

    .line 359
    .local v3, "result":Ljava/nio/charset/CoderResult;
    :goto_0
    invoke-virtual {v3}, Ljava/nio/charset/CoderResult;->isError()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 360
    new-instance v5, Ljava/io/IOException;

    invoke-virtual {v3}, Ljava/nio/charset/CoderResult;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 361
    :cond_0
    invoke-virtual {v3}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 362
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushBytesLocked()V

    .line 363
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    iget-object v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0, v6, v7}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v3

    .line 364
    goto :goto_0

    .line 368
    :cond_1
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushBytesLocked()V

    .line 369
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    .line 387
    .end local v0    # "charBuffer":Ljava/nio/CharBuffer;
    .end local v3    # "result":Ljava/nio/charset/CoderResult;
    :goto_1
    iput v9, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    .line 389
    :cond_2
    return-void

    .line 370
    :cond_3
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    if-eqz v5, :cond_4

    .line 371
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    iget-object v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    iget v7, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    invoke-virtual {v5, v6, v9, v7}, Ljava/io/Writer;->write([CII)V

    .line 372
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    invoke-virtual {v5}, Ljava/io/Writer;->flush()V

    goto :goto_1

    .line 374
    :cond_4
    const/4 v2, 0x0

    .line 375
    .local v2, "nonEolOff":I
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 376
    .local v4, "sepLen":I
    iget v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    if-ge v4, v5, :cond_5

    move v1, v4

    .line 377
    .local v1, "len":I
    :goto_2
    if-ge v2, v1, :cond_6

    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    iget v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    add-int/lit8 v6, v6, -0x1

    sub-int/2addr v6, v2

    aget-char v5, v5, v6

    iget-object v6, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    .line 378
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    sub-int/2addr v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_6

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 376
    .end local v1    # "len":I
    :cond_5
    iget v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    goto :goto_2

    .line 381
    .restart local v1    # "len":I
    :cond_6
    iget v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    if-lt v2, v5, :cond_7

    .line 382
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPrinter:Landroid/util/Printer;

    const-string v6, ""

    invoke-interface {v5, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 384
    :cond_7
    iget-object v5, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPrinter:Landroid/util/Printer;

    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mText:[C

    iget v8, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mPos:I

    sub-int/2addr v8, v2

    invoke-direct {v6, v7, v9, v8}, Ljava/lang/String;-><init>([CII)V

    invoke-interface {v5, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private final initDefaultEncoder()V
    .locals 2

    .prologue
    .line 288
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    .line 289
    iget-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 290
    iget-object v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 291
    return-void
.end method

.method private final initEncoder(Ljava/lang/String;)V
    .locals 3
    .param p1, "csn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 245
    :try_start_0
    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetEncoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 250
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mCharset:Ljava/nio/charset/CharsetEncoder;

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetEncoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 251
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    invoke-direct {v1, p1}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public append(Ljava/lang/CharSequence;II)Ljava/io/PrintWriter;
    .locals 3
    .param p1, "csq"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 667
    if-nez p1, :cond_0

    .line 668
    const-string p1, "null"

    .line 670
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 671
    .local v0, "output":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/service/library/log/FastPrintWriter;->write(Ljava/lang/String;II)V

    .line 672
    return-object p0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2, p3}, Lcom/navdy/service/library/log/FastPrintWriter;->append(Ljava/lang/CharSequence;II)Ljava/io/PrintWriter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2, p3}, Lcom/navdy/service/library/log/FastPrintWriter;->append(Ljava/lang/CharSequence;II)Ljava/io/PrintWriter;

    move-result-object v0

    return-object v0
.end method

.method public checkError()Z
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flush()V

    .line 263
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mIoError:Z

    monitor-exit v1

    return v0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected clearError()V
    .locals 2

    .prologue
    .line 273
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 274
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mIoError:Z

    .line 275
    monitor-exit v1

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 414
    iget-object v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 416
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V

    .line 417
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v1, :cond_1

    .line 418
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    return-void

    .line 419
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    if-eqz v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->setError()V

    goto :goto_0

    .line 425
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public flush()V
    .locals 3

    .prologue
    .line 398
    iget-object v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 400
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V

    .line 401
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v1, :cond_1

    .line 402
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    return-void

    .line 403
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mWriter:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->setError()V

    goto :goto_0

    .line 409
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public print(C)V
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 454
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 456
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(C)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 460
    return-void

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 457
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public print(I)V
    .locals 1
    .param p1, "inum"    # I

    .prologue
    .line 489
    if-nez p1, :cond_0

    .line 490
    const-string v0, "0"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/log/FastPrintWriter;->print(Ljava/lang/String;)V

    .line 494
    :goto_0
    return-void

    .line 492
    :cond_0
    invoke-super {p0, p1}, Ljava/io/PrintWriter;->print(I)V

    goto :goto_0
.end method

.method public print(J)V
    .locals 3
    .param p1, "lnum"    # J

    .prologue
    .line 498
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 499
    const-string v0, "0"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/log/FastPrintWriter;->print(Ljava/lang/String;)V

    .line 503
    :goto_0
    return-void

    .line 501
    :cond_0
    invoke-super {p0, p1, p2}, Ljava/io/PrintWriter;->print(J)V

    goto :goto_0
.end method

.method public print(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 474
    if-nez p1, :cond_0

    .line 475
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 477
    :cond_0
    iget-object v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 479
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {p0, p1, v1, v3}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 484
    return-void

    .line 480
    :catch_0
    move-exception v0

    .line 481
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->setError()V

    goto :goto_0

    .line 483
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public print([C)V
    .locals 3
    .param p1, "charArray"    # [C

    .prologue
    .line 437
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 439
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p1

    invoke-direct {p0, p1, v0, v2}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 443
    return-void

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 440
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public println()V
    .locals 5

    .prologue
    .line 509
    iget-object v2, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 511
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mSeparator:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {p0, v1, v3, v4}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(Ljava/lang/String;II)V

    .line 512
    iget-boolean v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mAutoFlush:Z

    if-eqz v1, :cond_0

    .line 513
    invoke-direct {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->flushLocked()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 519
    return-void

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->setError()V

    goto :goto_0

    .line 518
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public println(C)V
    .locals 0
    .param p1, "c"    # C

    .prologue
    .line 553
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/log/FastPrintWriter;->print(C)V

    .line 554
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->println()V

    .line 555
    return-void
.end method

.method public println(I)V
    .locals 1
    .param p1, "inum"    # I

    .prologue
    .line 523
    if-nez p1, :cond_0

    .line 524
    const-string v0, "0"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/log/FastPrintWriter;->println(Ljava/lang/String;)V

    .line 528
    :goto_0
    return-void

    .line 526
    :cond_0
    invoke-super {p0, p1}, Ljava/io/PrintWriter;->println(I)V

    goto :goto_0
.end method

.method public println(J)V
    .locals 3
    .param p1, "lnum"    # J

    .prologue
    .line 532
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 533
    const-string v0, "0"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/log/FastPrintWriter;->println(Ljava/lang/String;)V

    .line 537
    :goto_0
    return-void

    .line 535
    :cond_0
    invoke-super {p0, p1, p2}, Ljava/io/PrintWriter;->println(J)V

    goto :goto_0
.end method

.method public println([C)V
    .locals 0
    .param p1, "chars"    # [C

    .prologue
    .line 544
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/log/FastPrintWriter;->print([C)V

    .line 545
    invoke-virtual {p0}, Lcom/navdy/service/library/log/FastPrintWriter;->println()V

    .line 546
    return-void
.end method

.method protected setError()V
    .locals 2

    .prologue
    .line 282
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 283
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/service/library/log/FastPrintWriter;->mIoError:Z

    .line 284
    monitor-exit v1

    .line 285
    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public write(I)V
    .locals 2
    .param p1, "oneChar"    # I

    .prologue
    .line 596
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 598
    int-to-char v0, p1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(C)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 602
    return-void

    .line 601
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 599
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public write(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 612
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 614
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, p1, v0, v2}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 618
    return-void

    .line 617
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 615
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public write(Ljava/lang/String;II)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "count"    # I

    .prologue
    .line 636
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 638
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 642
    return-void

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 639
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public write([CII)V
    .locals 2
    .param p1, "buf"    # [C
    .param p2, "offset"    # I
    .param p3, "count"    # I

    .prologue
    .line 576
    iget-object v1, p0, Lcom/navdy/service/library/log/FastPrintWriter;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 578
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/service/library/log/FastPrintWriter;->appendLocked([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 582
    return-void

    .line 581
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 579
    :catch_0
    move-exception v0

    goto :goto_0
.end method
